<?php
/**
 * FCO_Form_Registration:
 * Subclass of DF_Form for special handling.
 */

namespace FCO\Form;

use \Entity\User;
use \Entity\Session;

class Registration extends \DF\Form
{
	protected $app_type;
	protected $config;
	protected $module_config;
	
	protected $_admin_view;
	protected $_limited_view;

	protected $_user;
	protected $_user_info;
	
	public function __construct($app_type = NULL, $user = NULL, $admin_view = FALSE, $limited_view = FALSE)
    {
		parent::__construct();
		
		$this->_admin_view = $admin_view;
		$this->_limited_view = $limited_view;

		$this->config = \Zend_Registry::get('config');
		
		$module_config = \Zend_Registry::get('module_config');
		$this->module_config = $module_config['registration'];
		
		if (!($user instanceof User))
			$user = \DF\Auth::getLoggedInUser();
		
		$this->_user = $user;
		$this->_user_info = $user->toArray(TRUE, TRUE);
		$this->_user_info['fc_session_preference'] = str_split($this->_user_info['fc_session_preference']);
		
		if ($app_type === NULL)
			$app_type = $user->fc_app_type;
		
		if ($app_type === NULL)
			$app_type = FC_TYPE_GUEST;
		
		$this->app_type = $app_type;
		
		$this->addSubForm($this->_getProfileForm(), 'profile');
		$this->addSubForm($this->_getRegistrationForm(), 'registration');
		
		if ($this->_admin_view && !$this->_limited_view)
			$this->addSubForm($this->_getAdminForm(), 'admin');
		
		$this->addElement('submit', 'submit', array(
            'type'	=> 'submit',
            'label'	=> 'Submit Registration',
            'class' => 'ui-button',
        ));
	}

	/**
	 * Profile Form
	 */
	protected function _getProfileForm()
	{
		$profile_form_config = $this->module_config->forms->profile->form->toArray();
		unset($profile_form_config['groups']['submit']);

		if (!in_array($this->app_type, array(FC_TYPE_COUNSELOR, FC_TYPE_COUNSELOR2)))
		{
			unset($profile_form_config['groups']['personal']['elements']['fc_org_affiliation']);
		}

		if ($this->_limited_view)
		{
			// Leave only the "Gender" field.
			$gender_el = $profile_form_config['groups']['personal']['elements']['gender'];
			$profile_form_config['groups']['personal']['elements'] = array('gender' => $gender_el);
		}

		if ($this->app_type == FC_TYPE_GUEST || $this->_limited_view)
        {
            unset($profile_form_config['groups']['medical']);
            unset($profile_form_config['groups']['parent']);
            unset($profile_form_config['groups']['alt1']);
            unset($profile_form_config['groups']['alt2']);
            unset($profile_form_config['groups']['personal']['elements']['birthdate']);
            unset($profile_form_config['groups']['personal']['elements']['college']);
        }
		
		$profile_form = new \DF\Form\SubForm($profile_form_config);
		$profile_form->setDefaults($this->_user_info);
		return $profile_form;
	}

	/**
	 * Registration Form
	 */
	protected function _getRegistrationForm()
	{
		$registration_form_config = $this->module_config->forms->registration->form->toArray();
		unset($registration_form_config['groups']['submit']);

		if ($this->_admin_view)
		{
			unset($registration_form_config['groups']['agreements']);
			unset($registration_form_config['groups']['payment']);
		}

		if ($this->app_type != FC_TYPE_COUNSELOR)
		{
			unset($registration_form_config['groups']['counselor_application']);
		}

		if ($this->app_type != FC_TYPE_FRESHMAN)
		{
			unset($registration_form_config['groups']['sessions']['elements']['specialprograms']);
		}
        
        if ($this->app_type == FC_TYPE_GUEST)
        {
            unset($registration_form_config['groups']['sessions']['elements']['fc_session_preference']);
        }
        else
        {
            unset($registration_form_config['groups']['sessions']['elements']['fc_assigned_session']);
        }
		
		$payment_config = $this->config->fishcamp->payment_config->toArray();
		if (!isset($payment_config[$this->app_type]) || $payment_config[$this->app_type]['type'] != "touchnet")
		{
			unset($registration_form_config['groups']['payment']);
		}

		$registration_form = new \DF\Form\SubForm($registration_form_config);
		$registration_form->setDefaults($this->_user_info);
		return $registration_form;
	}

	/**
	 * Administrator View Form
	 */
	protected function _getAdminForm()
	{
		$admin_form_config = $this->module_config->forms->registration_admin->form->toArray();
		unset($admin_form_config['groups']['submit']);

		$admin_form = new \DF\Form\SubForm($admin_form_config);
		$admin_form->setDefaults($this->_user_info);
		return $admin_form;
	}
	
	/**
	 * Submission Handling
	 */
	public function save()
	{
		$data = $this->getValues();
		$em = \Zend_Registry::get('em');
		
		// Updating a user from the admin panel.
		if ($this->_admin_view)
		{
			$other_data = $data['admin'];
			
			// Reset registration if applicant type changes.
			if ($this->_user['fc_app_type'] != $other_data['fc_app_type'])
			{
				$other_data['fc_app_completed'] = 0;
			}
		}
		// A user registering from the main system.
		else
		{
			$other_data = array(
				'fc_app_type'		=> $this->app_type,
				'fc_initial_timestamp' => time(),
				'fc_app_completed'	=> time(),
                'fc_sessions_selected' => time(),
				'fc_app_last_modified' => time(),
			);
            
            if ($this->app_type != FC_TYPE_FRESHMAN)
            {
                $other_data['fc_received_papers'] = 1;
            }
            else
            {
                $session_a = Session::getRepository()->findOneByName('A');
    			$other_data['fc_received_papers'] = ($data['profile']['birthdate'] <= strtotime($this->config->fishcamp->birthdate_time, $session_a->startdate)) ? 1 : 0;
            }
            
			$payment_config = $this->config->fishcamp->payment_config->toArray();
			if (isset($payment_config[$this->app_type]))
				$other_data['fc_received_payment'] = 0;
			else
				$other_data['fc_received_payment'] = 1;
		}
		
		$user_data = $data['profile'] + $data['registration'] + $other_data;
		$user_data['fc_session_preference'] = implode('', $user_data['fc_session_preference']);
		
		$this->_user->fromArray($user_data);
		$em->persist($this->_user);

		// Audit logging.
		if ($this->_admin_view)
		{
			$uow = $em->getUnitOfWork();
			$uow->computeChangeSets();

			$changes = (array)$uow->getEntityChangeSet($this->_user);
			$assignment_changes = array();

			$em->flush();

			foreach($changes as $field => $changeset)
			{
				if (strpos($field, 'assigned') !== FALSE)
				{ 
					$old = $changeset[0];
					$new = $changeset[1];

					if (strcmp($old, $new) != 0)
					{
						$field_name = ucfirst(substr($field, strrpos($field, '_')+1));
						$assignment_changes[] = $field_name.': "'.$old.'" to "'.$new.'"';
					}
				}
			}

			if ($assignment_changes)
			{
				$current_user = \DF\Auth::getLoggedInUser();

				$audit_log = (array)$this->_user->fc_audit_log;
				$audit_log[] = array(
					'user'			=> $current_user->firstname.' '.$current_user->lastname,
					'timestamp'		=> time(),
					'changes'		=> implode('<br>', $assignment_changes),
				);

				$this->_user->fc_audit_log = $audit_log;
				$em->persist($this->_user);
			}
		}

		$em->flush();
	}
}