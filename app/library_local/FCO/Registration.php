<?php
/**
 * FCO_Registration:
 * Fish Camp registration manager class.
 */

namespace FCO;

use \Entity\Settings;
use \Entity\User;

class Registration
{
	public static function getStatus($registration_type)
	{
        list($open_time, $close_time) = self::getOpenAndCloseTime($registration_type);
        
        $override = Settings::getSetting('registration_'.$registration_type.'_override', '');
		
		if ($override)
			return $override;
		else if ($open_time <= time() && $close_time >= time())
			return FC_STATUS_OPEN;
		else
			return FC_STATUS_CLOSED;
	}
    
    public static function getOverrideAccessCode($registration_type)
    {
        $hash_base = 'fc_registration_override_'.date('Y').'_'.$registration_type;
        return substr(strtoupper(sha1($hash_base)), 0, 5);
    }
	
	public static function getDateRangeText($registration_type)
	{
        list($open_time, $close_time) = self::getOpenAndCloseTime($registration_type);
		
		if ($open_time == 0 && $close_time == 0)
			return 'TBD';
		else if ($close_time == 0)
			return 'Opens '.date('M j, Y g:ia', $open_time);
		else
			return date('M j, Y g:ia', $open_time).' to '.date('M j, Y g:ia', $close_time);
	}
    
    public static function getOpenAndCloseTime($registration_type)
    {
        $config = \Zend_Registry::get('config');
        $fc_config = $config->fishcamp->toArray();
        
        $registration_open_time = ($fc_config['registration_open_time']) ? $fc_config['registration_open_time'] : '09:00:00';
        $registration_close_time = ($fc_config['registration_close_time']) ? $fc_config['registration_close_time'] : '17:00:00';
        
        $open_time = Settings::getSetting('registration_'.$registration_type.'_open', 0);
        $close_time = Settings::getSetting('registration_'.$registration_type.'_close', 0);
        
        $open_time = strtotime('Today '.$registration_open_time, $open_time);
		$close_time = strtotime('Today '.$registration_close_time, $close_time);
        
        return array($open_time, $close_time);
    }
	
	public static function updateUserPaidStatus($user)
	{
		if (!($user instanceof User))
			return NULL;
		
		$app_type = $user->fc_app_type;

		if ($app_type != FC_TYPE_FRESHMAN)
			return;
		
		$amount_due = Settings::getSetting('registration_'.$app_type.'_due', 0);
		
		if ($amount_due == 0)
		{
			$user->fc_received_payment = 1;
		}
		else
		{
			$total = 0;
			
			$em = \Zend_Registry::get('em');
			$ledger_items = $em->createQuery('SELECT l.amount FROM Entity\Ledger l WHERE l.user_id = :user_id')
				->setParameter('user_id', $user->id)->getArrayResult();
			
			if (count($ledger_items) > 0)
			{
				foreach($ledger_items as $item)
				{
					$total += $item['amount'];
				}
			}
			
			$user->fc_received_payment = ($total >= $amount_due) ? 1 : 0;
		}
		
		$user->save();
	}
}