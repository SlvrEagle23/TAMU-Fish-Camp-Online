<?php
namespace FCO;

use \Entity\User;
use \Entity\Checkin;

class CheckInManager
{
    public static function checkInUser($user_text, $origin = 'desktop')
    {
        $user_text = trim($user_text);
        
        if (strtoupper(substr($user_text, 0, 2)) == "FC")
        {
            $user_id = (int)substr($user_text, 2);
            $user = User::find($user_id);
        }
        else
        {
            $user = User::getRepository()->findOneByUin($user_text);
        }
        
        if ($user instanceof User)
        {
            $new_checkin_record = new Checkin();
            $new_checkin_record->user = $user;
            $new_checkin_record->origin = $origin;
            $new_checkin_record->save();
            
            return array(
                'success'       => TRUE,
                'user'          => $user->toArray(),
            );
        }
        else
        {
            return array(
                'success'       => FALSE,
            );
        }
    }
}