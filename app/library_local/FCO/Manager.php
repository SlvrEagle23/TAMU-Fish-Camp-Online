<?php
/**
 * Fish Camp registration coordinating class.
 */

namespace FCO;

use \Entity\User;
use \Entity\Session;
use \Entity\Settings;

class Manager
{
	protected $_settings;
	
	public function __construct()
	{
		$this->processSettings();
	}
	
	/*
	 * Settings
	 */
	public function getSettings()
	{
		return $this->_settings;
	}
	public function getSetting($setting_name)
	{
		return $this->_settings[$setting_name];
	}
	
	public function areAnySessionsWaitlisted()
	{
		foreach($this->_settings['sessions'] as $session_name => $session_info)
		{
			if ($session_info['status'] == FC_STATUS_WAITLIST)
				return true;
		}
		return false;
	}
	public function areAnySessionsFull()
	{
		foreach($this->_settings['sessions'] as $session_name => $session_info)
		{
			if ($session_info['status'] == FC_STATUS_FULL)
				return true;
		}

		return false;
	}

	protected function processSettings()
	{
		$config = \Zend_Registry::get('config');
		$datestamp_format = 'F j, Y g:ia';
		
		$this->_settings = $config->fishcamp->toArray();
		$this->_settings['freshman_status'] = \FCO\Registration::getStatus(FC_TYPE_FRESHMAN);

		// Prevent the CLI from freaking out about any potential DB model incongruency
		try
		{
			$this->_settings['sessions'] = \Entity\Session::fetchAll();
		}
		catch(\Exception $e)
		{
			return NULL;
		}
	}
	
	// Retrieve all freshmen who are currently eligible to attend camp.
	public function getEligibleCampers($order_by = 'u.fc_assignment_override DESC, u.fc_sessions_selected ASC')
	{
        $em = \Zend_Registry::get('em');
        return $em->createQuery('SELECT u FROM \Entity\User u WHERE u.fc_app_type = :app_type AND u.fc_received_papers = 1 AND u.fc_received_payment = 1 AND u.fc_app_completed != 0 ORDER BY '.$order_by)
            ->setParameter('app_type', FC_TYPE_FRESHMAN)
            ->getArrayResult();
	}
	
	// Retrieve all freshmen who are registered in the system, regardless of status.
	public function getAllCampers($type = FC_TYPE_FRESHMAN)
	{
        $em = \Zend_Registry::get('em');
        return $em->createQuery('SELECT u FROM \Entity\User u WHERE u.fc_app_type = :app_type AND u.fc_app_completed != 0 ORDER BY u.fc_sessions_selected ASC')
            ->setParameter('app_type', $type)
            ->getArrayResult();
	}
	
	public function runCampAssignment($process_all = false)
	{
		$em = \Zend_Registry::get('em');

		$run_assignment = Settings::getSetting('auto_assignment', 1);
		if (!$run_assignment)
			return NULL;

        // Clear all freshmen who are not override-assigned.
        $clear_users_query = $em->createQuery('UPDATE Entity\User u SET u.fc_assigned_session=NULL, u.fc_assigned_camp=NULL, u.fc_assigned_cabin=NULL, u.fc_assigned_bus=NULL WHERE u.fc_app_type IN (:types) AND (u.fc_assignment_override = 0 OR u.fc_is_cancelled = 1)')
        	->setParameter('types', array(FC_TYPE_NONE, FC_TYPE_FRESHMAN))
        	->execute();
        
		// Retrieve the appropriate set of campers for the query type.
		$all_campers = ($process_all) ? $this->getAllCampers() : $this->getEligibleCampers();
        
        set_time_limit(300);
        
        $db_updates = array();
        
		// Assemble arrays used for camp assignment.
		$session_populations = $session_populations_male = $session_populations_female = array();
		$session_totals = $session_totals_male = $session_totals_female = array();
		$session_ratios = array();
		
		$cabin_populations_male = $cabin_populations_female = array();
		$cabin_totals_male = $cabin_totals_female = array();
		
		$cabin_names_male = $cabin_names_female = array();
		
		$cabin_camps = array();
		$camp_totals = array();
		
		// Compose session information.
		$session_db_populations = array();
		foreach($all_campers as $camper)
		{
			$gender = ($camper['gender'] == 'M') ? 'M' : 'F';
			$selected_sessions = str_split($camper['fc_session_preference']);
			
			foreach($selected_sessions as $session_name)
			{
				if ($session_name)
				{
					$session_db_populations[$session_name][$gender]++;
					$session_db_populations['total'][$gender]++;
				}
			}
		}
		
		foreach($this->_settings['sessions'] as $session_name => $session_info)
		{
			$session_db_populations_male[$session_name] = $session_db_populations[$session_name]['M'] / $session_db_populations['total']['M'];
			$session_db_populations_female[$session_name] = $session_db_populations[$session_name]['F'] / $session_db_populations['total']['F'];
			
			$session_populations_male[$session_name] = 0;
			$session_populations_female[$session_name] = 0;
			
			$session_totals_male[$session_name] = 0;
			$session_totals_female[$session_name] = 0;
			
			$cabin_populations_male[$session_name] = array();
			$cabin_populations_female[$session_name] = array();
		}
		
		// Compose cabin information.
        $em = \Zend_Registry::get('em');
        $raw_cabin_data = $em->createQueryBuilder()
            ->select('c1, c2')
            ->from('\Entity\Cabin', 'c1')
            ->join('c1.camp', 'c2')
            ->getQuery()->getArrayResult();
        
		foreach($raw_cabin_data as $cabin_info)
		{
            $cabin_info['camp_name'] = $cabin_info['camp']['name'];
            
			foreach($this->_settings['sessions'] as $session_name => $session_info)
			{
				if ($cabin_info['cabin_gender_'.$session_name] == 'X')
					continue;

                if ($cabin_info['cabin_gender_'.$session_name] == 'M')
				{
					$cabin_populations_male[$session_name][$cabin_info['name']] = 0;
					$cabin_totals_male[$session_name][$cabin_info['name']] = $cabin_info['population'];
					$session_totals_male[$session_name] += $cabin_info['population'];
					$cabin_names_male[$session_name][] = $cabin_info['name'];
				}
				else
				{
					$cabin_populations_female[$session_name][$cabin_info['name']] = 0;
					$cabin_totals_female[$session_name][$cabin_info['name']] = $cabin_info['population'];
					$session_totals_female[$session_name] += $cabin_info['population'];
					$cabin_names_female[$session_name][] = $cabin_info['name'];
				}
			
				$cabin_populations[$session_name][$cabin_info['name']] = 0;
				$camp_populations[$session_name][$cabin_info['camp_name']] = 0;
			}
			
			$camp_totals[$cabin_info['camp_name']] += $cabin_info['population'];
			$cabin_camps[$cabin_info['name']] = $cabin_info['camp_name'];
		}
        
		// Assemble Bus Array
		$bus_pointer = 0;
		$camp_buses = array();
		
		foreach($this->_settings['sessions'] as $session_name => $session_info)
		{
			foreach($camp_totals as $camp_name => $camp_total)
			{
				$camp_buses[$session_name][$camp_name] = $bus_pointer+1;
				$bus_pointer += $camp_total;
			}
			$bus_pointer = 1;
		}

        $update_user_query = $em->createQuery('UPDATE Entity\User u SET u.fc_assigned_session = :fc_assigned_session, u.fc_assigned_camp = :fc_assigned_camp, u.fc_assigned_cabin = :fc_assigned_cabin WHERE u.id = :user_id');

        // Process all "forced assignment" people first, to avoid cabin over-assignment.
        $assignable_campers = array();
        $lock_only_sessions = false;

        if ($lock_only_sessions)
        {
        	$assignable_campers = $all_campers;
        }
        else
        {
	        foreach($all_campers as $camper)
	        {
	        	$gender = ($camper['gender'] == 'M') ? 'M' : 'F';

	        	// Handle session override - don't assign, but still use in totals.
				$skip_assignment = ((int)$camper['fc_assignment_override'] == 1);

				if ($skip_assignment)
				{
					$assigned_session = $camper['fc_assigned_session'];
					$assigned_cabin = $camper['fc_assigned_cabin'];
					$assigned_camp = $camper['fc_assigned_camp'];

					// Update totals.
					if ($assigned_session != 'WL' && $assigned_cabin != 'WL')
					{
						$camp_populations[$assigned_session][$assigned_camp]++;
						$cabin_populations[$assigned_session][$assigned_cabin]++;

						if ($camper['gender'] == 'M')
						{
							$session_populations_male[$assigned_session]++;
							
							if (in_array($assigned_cabin, $cabin_names_male[$assigned_session]))
								$cabin_populations_male[$assigned_session][$assigned_cabin]++;
						}
						else
						{
							$session_populations_female[$assigned_session]++;
							if (in_array($assigned_cabin, $cabin_names_female[$assigned_session]))
								$cabin_populations_female[$assigned_session][$assigned_cabin]++;
						}
						$camp_buses[$assigned_session][$assigned_camp]++;
					}
				}
				else
				{
					$assignable_campers[] = $camper;
				}
	        }
	    }

        // Process all auto-assignable campers.
		foreach($assignable_campers as $camper)
		{
			$gender = ($camper['gender'] == 'M') ? 'M' : 'F';
			
			// Re-calculate session population ratios (current population to total population, per gender).
			$sessions = $this->_settings['sessions'];
							
			foreach ($sessions as $session_name => $session_info)
			{
				if ($camper['gender'] == 'M')
				{
					$session_db_populations[$session_name] = $session_db_populations_male[$session_name];
					$session_populations[$session_name] = $session_populations_male[$session_name];
					$session_totals[$session_name] = $session_totals_male[$session_name];							
				}
				else
				{
					$session_db_populations[$session_name] = $session_db_populations_female[$session_name];
					$session_populations[$session_name] = $session_populations_female[$session_name];
					$session_totals[$session_name] = $session_totals_female[$session_name];
				}
				
				$session_ratio = round($session_populations[$session_name] / $session_totals[$session_name], 3);
				$session_ratios[$session_name] = $session_ratio;
				
				// Adjust the tendency to assign a user to a given session based on how popular it usually is.
				$session_fraction = $session_db_populations[$session_name];
				$session_ranks[$session_name] = $session_ratio * $session_fraction;
			}
			
			if ($lock_only_sessions && (int)$camper['fc_assignment_override'] == 1)
			{
				$session_name = $camper['fc_assigned_session'];
			}
			else
			{
				// If the camper only chose one session, choose this session.
				if (strlen($camper['fc_session_preference']) == 1)
				{
					$session_name = substr($camper['fc_session_preference'], 0, 1);
				}
				// If the camper included session G at all, assign them to it.
				else if (strpos($camper['fc_session_preference'], 'G') !== FALSE)
				{
					$session_name = 'G';
				}
				else
				{
					$session_preferences_raw = str_split($camper['fc_session_preference']);

					// Default to the first listed session, in case no opening is found.
					$lowest_session_name = $session_preferences_raw[0];
					$lowest_session_ratio = 10.0;
					
					// Loop through all sessions to find an opening in the lowest-populated session.
					foreach($session_preferences_raw as $session_pref)
					{
						if (isset($sessions[$session_pref]))
						{
							$current_session_ratio = $session_ranks[$session_pref];
							
							if ($current_session_ratio < $lowest_session_ratio)
							{
								$lowest_session_name = $session_pref;
								$lowest_session_ratio = $current_session_ratio;
							}
						}
					}
					
					$session_name = $lowest_session_name;
				}
			}

			$session_pop = $session_populations[$session_name];
			$session_total = $session_totals[$session_name];				
			
			// If the camper cannot go to the selected session, they cannot go to any of them, as all were looped through.
			if ($session_pop >= $session_total)
			{
				$assigned_session = 'WL';
				$assigned_camp = 'WL';
				$assigned_cabin = 'WL';
				$assigned_bus = 'WL';
			}
			else
			{
				$assigned_session = $session_name;
				$assign_attempts = 0;
				
				while(TRUE)
				{
					// Assign a cabin, and (by association) a camp and bus.
					$possible_cabins = ($camper['gender'] == 'M') ? $cabin_populations_male[$assigned_session] : $cabin_populations_female[$assigned_session];

					if (count($possible_cabins) > 0)
					{
						$cabins_sort_array = array();
						foreach($possible_cabins as $cabin_name => $cabin_population)
						{
							$cabin_camp = $cabin_camps[$cabin_name];
							
							$camp_population = $camp_populations[$assigned_session][$cabin_camp];
							$camp_fraction = $camp_population / $camp_totals[$cabin_camp];
							
							$cabins_sort_array[$cabin_name] = $cabin_population * $camp_fraction;
						}

						asort($cabins_sort_array);
						reset($cabins_sort_array);

						$possible_cabin_name = key($cabins_sort_array);	
						$possible_cabin_population = $possible_cabins[$possible_cabin_name];
	                    
						$cabin_total = ($camper['gender'] == 'M') ? $cabin_totals_male[$assigned_session][$possible_cabin_name] : $cabin_totals_female[$assigned_session][$possible_cabin_name];
	                    
						if ($possible_cabin_population >= $cabin_total)
						{
							if ($camper['gender'] == 'M')
								unset($cabin_populations_male[$assigned_session][$possible_cabin_name]);
							else
								unset($cabin_populations_female[$assigned_session][$possible_cabin_name]);
						}
						else
						{
							$assigned_cabin = $possible_cabin_name;
							$assigned_camp = $cabin_camps[$assigned_cabin];
							break;
						}
					}
					
					$assign_attempts++;
					if ($assign_attempts > 10 || count($possible_cabins) == 0)
					{
						$assigned_session = 'WL';
						$assigned_cabin = 'WL';
						$assigned_camp = 'WL';
						$assigned_bus = 'WL';
						break;
					}
				}
			}
			
			// Update database with their new information.
			if (!$process_all)
			{
                $update_user_query->setParameters(array(
                    'user_id'       => $camper['id'],
                    'fc_assigned_session' => $assigned_session,
                    'fc_assigned_camp' => $assigned_camp,
                    'fc_assigned_cabin' => $assigned_cabin,
                ))->execute();
			}
			
			// Update totals.
			if ($assigned_session != 'WL' && $assigned_cabin != 'WL')
			{
				$camp_populations[$assigned_session][$assigned_camp]++;
				$cabin_populations[$assigned_session][$assigned_cabin]++;

				if ($camper['gender'] == 'M')
				{
					$session_populations_male[$assigned_session]++;
					
					if (in_array($assigned_cabin, $cabin_names_male[$assigned_session]))
						$cabin_populations_male[$assigned_session][$assigned_cabin]++;
				}
				else
				{
					$session_populations_female[$assigned_session]++;
					if (in_array($assigned_cabin, $cabin_names_female[$assigned_session]))
						$cabin_populations_female[$assigned_session][$assigned_cabin]++;
				}
				$camp_buses[$assigned_session][$assigned_camp]++;
			}
		}
		
		// Update sessions database to reflect the new data.
		foreach($this->_settings['sessions'] as $session_name => $session_info)
		{
			$sess_num_current_m = $session_populations_male[$session_name];
			$sess_num_current_f = $session_populations_female[$session_name];
			$sess_num_current = $sess_num_current_m + $sess_num_current_f;
			
			$sess_num_total_m = $session_totals_male[$session_name];
			$sess_num_total_f = $session_totals_female[$session_name];
			$sess_num_total = $sess_num_total_m + $sess_num_total_f;
			
			$sess_num_waitlist_m = floor($sess_num_total_m * $this->_settings['waitlist_percentage']);
			$sess_num_waitlist_f = floor($sess_num_total_f * $this->_settings['waitlist_percentage']);
			$sess_num_waitlist = $sess_num_waitlist_m + $sess_num_waitlist_f;
			
			$sess_num_full_m = floor($sess_num_total_m * $this->_settings['full_percentage']);
			$sess_num_full_f = floor($sess_num_total_f * $this->_settings['full_percentage']);
			$sess_num_full = $sess_num_full_m + $sess_num_full_f;

			$updated_session_info = array(
				'num_current'		=> $sess_num_current,
				'num_current_m'	    => $sess_num_current_m,
				'num_current_f'	    => $sess_num_current_f,
				'num_total'		    => $sess_num_total,
				'num_total_m'		=> $sess_num_total_m,
				'num_total_f'		=> $sess_num_total_f,
				'num_full'			=> $sess_num_full,
				'num_full_m'		=> $sess_num_full_m,
				'num_full_f'		=> $sess_num_full_f,
				'num_waitlist'		=> $sess_num_waitlist,
				'num_waitlist_m'	=> $sess_num_waitlist_m,
				'num_waitlist_f'	=> $sess_num_waitlist_f,
			);
            
            $session = Session::getRepository()->findOneBy(array('name' => $session_name));
            $session->fromArray($updated_session_info);
            $session->save();
		}
		
		$this->assignBuses();
		$this->processSettings();
	}
	
	/**
	 * Camp information
	 */
	 
	public function getCampColor($camp_name)
	{
		if (isset($this->_settings['camp_colors'][$camp_name]))
			return $this->_settings['camp_colors'][$camp_name];
		else
			return NULL;
	}
	
	/**
	 * Buses
	 */
	
	public function assignBuses()
	{
		$run_assignment = Settings::getSetting('auto_assignment_buses', 1);
		if (!$run_assignment)
			return NULL;

		$all_campers = $this->getEligibleCampers('u.fc_assigned_session ASC, u.fc_assigned_camp ASC, u.lastname ASC');
        
		$campers_by_session = array();
		foreach($all_campers as $camper)
		{
			$session = $camper['fc_assigned_session'];
			
			if (isset($this->_settings['sessions'][$session]))
				$campers_by_session[$session][] = $camper;
		}
		
		$buses_to_assign = array();
		foreach($campers_by_session as $session_name => $campers)
		{
			$i = 1;
			foreach($campers as $camper)
			{
				$bus_name = $this->determineBusName($i);
				$buses_to_assign[$bus_name][] = $camper['uin'];
				$i++;
			}
		}
        
        $em = \Zend_Registry::get('em');
        $assign_bus_query = $em->createQueryBuilder()
            ->update('\Entity\User', 'u')
            ->set('u.fc_assigned_bus', ':bus_name')
            ->where('u.uin IN (:uins)');
        
        foreach($buses_to_assign as $bus_name => $uins)
		{
            $assign_bus_query->setParameters(array('bus_name' => $bus_name, 'uins' => $uins))->getQuery()->execute();
		}
	}
	
	public function determineBusName($raw_number)
	{
		$bus_number = ceil(($raw_number) / $this->_settings['freshmen_per_bus']);
		return $this->getbusNameByNumber($bus_number);
	}
	
	public function getBusNameByNumber($bus_number)
	{
		$bus_cluster = ceil($bus_number / 5);
		$bus_name = chr(64+$bus_cluster).$bus_number;
		return $bus_name;
	}
	
	public function getBusData($session = 'A')
	{
		global $db;
        
        $users_on_buses = $this->getEligibleCampers('u.fc_assigned_bus ASC');
		$bus_data = array();
		
		foreach($users_on_buses as $user_on_bus)
		{
			if ($user_on_bus['fc_assigned_bus'] && $user_on_bus['fc_assigned_camp'] != "WL" && $user_on_bus['fc_assigned_session'] == $session)
            {
                $bus_number = str_pad(substr($user_on_bus['fc_assigned_bus'], 1), 2, '0', STR_PAD_LEFT).substr($user_on_bus['fc_assigned_bus'], 0, 1);
                $camp_name = ucfirst($user_on_bus['fc_assigned_camp']);
                $bus_data[$bus_number]['Total Filled']++;
                $bus_data[$bus_number]['Camp '.$camp_name]++;
            }
		}
		
		ksort($bus_data);
		
		$new_bus_data = array();
		
		foreach($bus_data as $bus_number => $bus_totals)
		{
			$current_passengers = $bus_totals['Total Filled'];
			if ($current_passengers < $this->_settings['freshmen_per_bus'])
				$bus_totals['Open Seats'] = ($this->_settings['freshmen_per_bus'] - $current_passengers);
			
			$modified_bus_number = substr($bus_number, -1).intval(substr($bus_number, 0, -1));
			
			$grand_total_num = $bus_totals['Total Filled'];
			unset($bus_totals['Total Filled']);
			ksort($bus_totals);
			
			$new_bus_totals = array();
			foreach($bus_totals as $total_type => $total_num)
			{
				$new_bus_totals[$total_type] = array(
					'num'		=> $total_num,
				);
				
				if (substr($total_type, 0, 4) == "Camp")
					$new_bus_totals[$total_type]['color'] = $this->getCampColor(substr($total_type, 5));
				else
					$new_bus_totals[$total_type]['color'] = '#CCC';
			}
			
			$new_bus_data[$modified_bus_number] = array(
				'total'		=> $grand_total_num,
				'subtotals'	=> $new_bus_totals,
			);
		}
		
		return $new_bus_data;
	}
	
	/**
	 * Charting
	 */
	
	public function getSessionChart()
	{
        $session_data = array();
		foreach($this->_settings['sessions'] as $session_name => $session_info)
		{
            $session_data['totals'][] = $session_info['num_total'];

            $session_info['name'] = $session_name;
            $session_info['total'] = $session_info['num_current'];
            $session_info['male'] = $session_info['num_current_m'];
            $session_info['female'] = $session_info['num_current_f'];

            $session_data['sessions'][$session_name] = $session_info;
		}
        
        $session_data['max_total'] = max($session_data['totals']);

        return $session_data;
	}

	/**
	 * Direct manipulation of setting values
	 */
    public function __set($key, $val)
    {
        $this->_settings[$key] = $val;
    }
    public function __get($key)
    {
		return $this->_settings[$key];
    }
    public function __isset($key)
    {
        return (isset($this->_settings[$key]));
    }
    public function __unset($key)
    {
        unset($this->_settings[$key]);
    }
}