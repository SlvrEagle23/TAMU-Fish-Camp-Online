<?php
/**
 * COMPASS Grade management functions
 */

namespace FCO;

use \Entity\User;

class Grades
{	
	// Name of the cumulative GPR field returned by Compass.
	const CUMULATIVE_GPR = 'CUMUL';
	
	// Internal use variables.
	protected static $compass;
	protected static $is_compass_available = NULL;
	
	public static function getStudentRecord($uin, $current_term = NULL)
	{
		$grades = self::getBulkStudentRecords(array($uin), $current_term);
		return $grades[$uin];
	}
	
	public static function getBulkStudentRecords($uins, $current_term = NULL)
	{
		$current_term = (is_null($current_term)) ? self::getCurrentActiveTerm() : $current_term;

		// Check for Compass availability before continuing.
		if (self::isCompassAvailable())
		{
			// Extend the script's running time.
			set_time_limit(200);
			
			$uins_filtered = array();
			foreach($uins as $uin_raw)
			{
				$uins_filtered[] = self::getUIN($uin_raw);
			}
			$all_grades = self::fetchBulkGrades($uins_filtered);
			
			if (count($uins) > (count($all_grades) - 50))
			{
				$grades = array();
				foreach($uins_filtered as $uin)
				{
					$uin = intval(trim(str_replace('-', '', $uin)));
					
					$grade_record = $all_grades[$uin];
					
					$student_record = array();
					$student_record['gpa'] = (isset($grade_record['grades'][$current_term])) ? $grade_record['grades'][$current_term] : 0;
					$student_record['cumul'] = (isset($grade_record['grades'][self::CUMULATIVE_GPR])) ? $grade_record['grades'][self::CUMULATIVE_GPR] : 0;
					$student_record['classification'] = (isset($grade_record['classification'])) ? $grade_record['classification'] : 'NA';
					
					$grades[$uin] = $student_record;
				}
			}
			
			return $grades;
		}
		else
		{
			return NULL;
		}
	}
	
	public static function getTermName($term_code = NULL)
	{
		$term_code = (is_null($term_code)) ? self::getCurrentActiveTerm() : $term_code;
	
		$semesters = array(
			1		=> 'Spring',
			2		=> 'Summer',
			3		=> 'Fall',
		);
        $campus_codes = array(
            1       => 'College Station',
            2       => 'Galveston',
            3       => 'Qatar',
        );
		
		if ($term_code == self::CUMULATIVE_GPR)
		{
			return 'Cumulative';
		}
		else if (!is_null($term_code))
		{
			$term_year = substr($term_code, 0, 4);
			$term_semester = substr($term_code, 4, 1);
            $term_campus_code = substr($term_code, 5, 1);
            
            $term_name = $semesters[$term_semester].' '.$term_year;
            
            if ($term_campus_code != 1)
				$term_name .= ' ('.$campus_codes[$term_campus_code].')';
			
			return $term_name;
		}
		else
		{
			return '';
		}
	}
	
	/**
	 * COMPASS API Functions
	 */
	
	protected static function getCompass()
	{
		if (!is_object(self::$compass))
		{
			self::$compass = new \DF\Service\DoitApi();
		}
		
		return self::$compass;
	}
	
	public static function isCompassAvailable()
	{
		if (self::$is_compass_available === NULL)
		{
			$compass = self::getCompass();
			self::$is_compass_available = $compass->checkAvailability();
		}
		
		return self::$is_compass_available;
	}
	
	public static function fetchGradesForAllTerms($uin)
	{
		$grades = self::fetchBulkGrades(array($uin));
		return $grades[$uin];
	}
	
	public static function fetchBulkGrades($uins)
	{
		$compass = self::getCompass();
		
		try
		{
			$grades_raw = $compass->getBulkGPAByStudentId($uins);
			
			if ($grades_raw)
			{
				$grades = array();
				foreach($grades_raw as $uin => $grade_array)
				{
					$grades[$uin] = self::processCompassGradeRow($grade_array);
				}
				return $grades;
			}
			else
			{
				return NULL;
			}
		}
		catch (Exception $e)
		{
			return NULL;
		}
	}
	
	public static function processCompassGradeRow($grade_row)
	{	
		$grades = array();
		
		$grades['classification'] = $grade_row['current_classification'];
		$class_code = (substr($grades['classification'], 0, 1) == 'U') ? 'UG' : 'GR';
		
		foreach($grade_row['grades'] as $grade_data)
		{
			$term = $grade_data['term'];
			
			$term_campus_code = substr($term, -1);
			if ($term == self::CUMULATIVE_GPR || $term_campus_code == 1)
			{
				if ($grade_data['classification'] == $class_code)
				{
					$grades['grades'][$term] = $grade_data['gpa'];
				}
			}
		}
		
		return $grades;
	}
	
	public static function getAvailableTerms()
	{
		$compass = self::getCompass();
		
		try
		{
			$terms_raw = $compass->getAvailableGPATerms();
			
			if ($terms_raw)
			{
				rsort($terms_raw);
				
				$terms = array();
				foreach($terms_raw as $term_num)
				{
					if ($term_num != self::CUMULATIVE_GPR && substr($term_num, -1) == 1)
						$terms[$term_num] = self::getTermName($term_num);
				}
				
				return $terms;
			}
			else
			{
				return NULL;
			}
		}
		catch (Exception $e)
		{
			return NULL;
		}
	}
	
	public static function getStudentStatus($uin)
	{
		$student_details = self::getStudentDetails($uin);
		
		return (isset($student['classification'])) ? $student['classification'] : NULL;
	}
	
	public static function getStudentDetails($uin)
	{
		$compass = self::getCompass();
		
		try
		{
			$student = $compass->getStudentById($uin);
			return ($student) ? $student : NULL;
		}
		catch (Exception $e)
		{
			return NULL;
		}
	}
	
	public static function getUIN($uin_raw)
	{
		return intval(substr(str_replace('-', '', trim($uin_raw)), 0, 9));
	}
}