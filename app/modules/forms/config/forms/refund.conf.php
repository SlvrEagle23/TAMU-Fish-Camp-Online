<?php
/**
 * Refund Request Form
 */

use \Entity\RefundRequest;

$refund_deadline = \Entity\Settings::getSetting('refund_close_date', 0);
 
return array(
	
	'notify' => array(
		RefundRequest::STATUS_PENDING_DIRECTOR => array(
			'fcamp-ir@dsa.tamu.edu',
		),
		RefundRequest::STATUS_PENDING_ADVISOR => array(
			'ahowell@stuact.tamu.edu',
		),
		RefundRequest::STATUS_PENDING_ACCOUNTING => array(
			'nbath@stuact.tamu.edu',
			'mfeatherston@stuact.tamu.edu',
		),
	),
	
	'upload_folder' => 'refunds',
	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		
		'groups'		=> array(
			
			'contact'		=> array(
				'legend'		=> 'Request Details',
				'elements'		=> array(
				
					'circumstances'	=> array('textarea', array(
						'label' => 'Extenuating Circumstances',
						'class' => 'full-width full-height',
						'description' => 'Describe the reason you are requesting a refund. Please include all relevant details, as this information will be used when considering your refund request.',
						'required' => true,
					)),
				
				),
			),
			
			'travelers'		=> array(
				'legend'		=> 'Supporting Documents',
				'description' 	=> 'If you wish to provide additional documents to support your request, attach them below.',
				'elements'		=> array(
				
					'file_supporting' => array('file', array(
						'label' => 'Supporting Documents',
						'multiFile' => 3,
					)),
				
				),
			),
			
			'student_travel' => array(
				'legend'		=> 'Statement of Understanding',
				'elements'		=> array(
					
					'policy' => array('markup', array(
						'label' => 'Cancellation Policy',
						'markup' => '
							<p>I understand Fish Camp\'s Cancellation Policy is the following:</p>
							<ul>
							<li>Cancellations must be received in writing before '.date('F j, Y', $refund_deadline).' at 5:00pm. Those canceling on or
before this date will receive a refund less a $25.00 cancellation fee.</li>
							<li>Those canceling after '.date('F j, Y', $refund_deadline).' at 5:00pm will forfeit their entire payment.</li>
							<li>Registration is not transferable to another individual.</li>
							<li>Online payments will be refunded to the credit card provided for payment.</li>
							</ul>
						',
					)),
					
					'initials' => array('text', array(
						'label' => 'Your Initials',
						'required' => true,
					)),
				
				),
			),
			
			'submit'		=> array(
				'elements'		=> array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save and Submit',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);