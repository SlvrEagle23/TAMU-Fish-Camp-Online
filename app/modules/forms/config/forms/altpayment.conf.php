<?php
/**
 * Alternate Payment form
 */

use \Entity\AltPaymentRequest;

return array(	

	// E-mail addresses to notify with submissions.
	'notify' => array(
		AltPaymentRequest::STATUS_PENDING_DIRECTOR => array(
			'fcamp-ir@dsa.tamu.edu',
		),
		AltPaymentRequest::STATUS_PENDING_ADVISOR => array(
			'ahowell@stuact.tamu.edu',
		),
		AltPaymentRequest::STATUS_PENDING_ACCOUNTING => array(
			'jbeen@stuact.tamu.edu',
			'mfeatherston@stuact.tamu.edu',
		),
	),

	// Approval types
	'approval_types' => array(
		2	=> 'Approved - Full',
		1	=> 'Approved - Partial',
		0	=> 'Declined',
	),

	// Scholarship types.
	'scholarship_types' => array(
		'N/A',
		'Gina and Bill \'76 Flores Endowment',
		'Kristen \'05 and Michael \'08 Nance Endowment',
		'12th Man Foundation',
		'International Student Scholarship',
		'PSC Scholarship',
		'Fish Camp Scholarship Endowment',
		'Fiscal Scholarship',
	),

	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'groups'		=> array(
			
			'about' => array(
				'legend' => 'Alternate Payment Options',
				'elements' => array(
					
					'about_info' => array('markup', array(
						'label' => 'About Alternate Payment',
						'markup' => '
							<p>Please complete the form below. The information received will help determine if you will receive a scholarship for Fish Camp. Even though the information below is not mandatory please provide information as you feel comfortable because our decision will be based on the information we receive.  Once your application has been reviewed and processed, you will be notified of any remaining balance. Please note the remaining balance will be paid by check and a fee of $35 will be applied for any checks returned for non-sufficient funds.  All communication about this process will be through your Texas A&M University e-mail address, so please be sure to check this e-mail account regularly.  If you have any questions please contact us at <a href="mailto:fcamp-ir@dsa.tamu.edu">fcamp-ir@dsa.tamu.edu</a>.</p>

							<p>Please note: If you have not filled out your FASFA and submitted it to the University, we recommend that you do so after you complete this form.  We ask this so that we can best process your Fish Camp  Scholarship Application.  Fish Camp scholarships will be awarded based on need and availability of funds at the discretion of the Fish Camp Director Staff. All applicants will be notified via e-mail.</p>

							<p>Fish Camp would like to acknowledge the 12th Man Foundation, the Gina and Bill Flores \'76 Scholarship Endowment, the Michael \'08 and Kristen \'05 Nance Scholarship Endowment, the Fish Camp Scholarship Endowment, and Aggie Moms\' Clubs for helping to make these scholarships possible.</p>
						',
					)),
				),
			),
			
			'scholarship_questions' => array(
				'legend' => 'Scholarship Questions',
				'description' => 'Only answer these questions if you have selected the "Scholarship" option above.',
				'elements' => array(
				
					'amount' => array('radio', array(
						'label' => 'What amount are you requesting from Fish Camp?',
						'multiOptions' => array(
							'Full'		=> 'Full Scholarship',
							'Half'		=> 'Half Scholarship',
						),
					)),

					'first_gen' => array('radio', array(
						'label' => 'Are you a first-generation college student?',
						'multiOptions' => array(
							0			=> 'No',
							1			=> 'Yes',
						),
					)),

					'single_parent' => array('radio', array(
						'label' => 'Do you currently live in a single parent/guardian household?',
						'multiOptions' => array(
							0			=> 'No',
							1			=> 'Yes',
						),
					)),

					'dependents' => array('text', array(
						'label' => 'How many people currently reside in your household, including yourself, that are being supported financially? (Dependents)',
					)),

					'students_in_4yr_college' => array('text', array(
						'label' => 'How many students in your family are attending four-year colleges/universities this academic year of Fall '.date('Y').' & Spring '.(date('Y')+1).'?',
					)),

					'students_in_2yr_college' => array('text', array(
						'label' => 'How many students in your family are attending two-year colleges this academic year of Fall '.date('Y').' & Spring '.(date('Y')+1).'?',
					)),

					'gross_income' => array('radio', array(
						'label' => 'What is your family\'s gross annual income for the previous calendar year?',
						'multiOptions' => \DF\Utilities::pairs(array(
							'Below $25,000',
							'$25,000-$50,000',
							'$50,000-$75,000',
							'$75,000-$100,000',
							'$100,000-$125,000',
							'More than $125,000',
							'Prefer not to say',
						)),
					)),

					'fafsa_efc' => array('radio', array(
						'label' => 'What is your Estimated Family Contribution (EFC) on your FASFA form?',
						'multiOptions' => \DF\Utilities::pairs(array(
							'Below $1,000',
							'$1,000-$5,000',
							'$5,000-$10,000',
							'$10,000-$15,000',
							'$15,000-$20,000',
							'More than $25,000',
							'Prefer not to say',
						)),
					)),

					'scholarships_offered' => array('radio', array(
						'label' => 'Have you been offered any scholarships for the academic year of Fall '.date('Y').' & Spring '.(date('Y')+1).'?',
						'multiOptions' => array(
							0			=> 'No',
							1			=> 'Yes',
						),
					)),

					'scholarship_amount' => array('text', array(
						'label' => 'If yes, please list the amount of scholarship dollars you have been awarded for the academic year.',
					)),

					'grants_offered' => array('radio', array(
						'label' => 'Have you been offered any grants for the academic year of Fall '.date('Y').' & Spring '.(date('Y')+1).'?',
						'multiOptions' => array(
							0			=> 'No',
							1			=> 'Yes',
						),
					)),

					'grant_amount' => array('text', array(
						'label' => 'If yes, please list the amount of grant dollars you have been awarded for the academic year.',
					)),

					'college_savings_plan' => array('radio', array(
						'label' => 'Do your parents or someone you know have a Texas Tomorrow Fund, a 529 Plan, or other college savings plan for you?',
						'multiOptions' => array(
							0			=> 'No',
							1			=> 'Yes',
						),
					)),

					'college_savings_amount' => array('text', array(
						'label' => 'If yes, please list the amount of the Texas Tomorrow Fund, 529 Plan, or other college savings plan.',
					)),

					'working_currently' => array('radio', array(
						'label' => 'Are you working currently?',
						'multiOptions' => array(
							0			=> 'No',
							1			=> 'Yes',
						),
					)),

					'working_hours' => array('text', array(
						'label' => 'If yes, how many hours per week are you working?',
					)),
					
					'circumstances' => array('textarea', array(
						'label' => 'Please describe in detail any additional information/ circumstances we should know that affect your financial situation currently.',
						'class' => 'full-width full-height',
					)),
				
				),
			),
			
			'submit'	=> array(
				'elements'	=> array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Submit Request',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);