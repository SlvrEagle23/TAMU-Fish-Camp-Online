<?php
/**
 * Roadtrip Form
 */

$camp_name_options = array(
	'Select a Camp',
	'Session A'		=> array(
		'A Red Camp Uptain',
		'A Blue Camp Toback',
		'A Green Camp Davis',
		'A Yellow Camp Ramirez',
		'A Purple Camp Sieggreen',
		'A Aqua Camp Youngkin',
		'A Crew',
	),
	'Session B'		=> array(
		'B Red Camp Engle',
		'B Blue Camp Vaculin',
		'B Green Camp Jibby',
		'B Yellow Camp Knoop',
		'B Purple Camp Reid',
		'B Aqua Camp Smith',
	),
	'Session C'		=> array(
		'C Red Camp Lewis',
		'C Blue Camp Kinison',
		'C Green Camp Klett',
		'C Yellow Camp Cutshall',
		'C Purple Camp Ivey',
		'C Aqua Camp Pryor',
	),
	'Session D'		=> array(
		'D Red Camp Murray',
		'D Blue Camp Gandy',
		'D Green Camp Rackley',
		'D Yellow Camp Wilson',
		'D Purple Camp Shields',
		'D Aqua Camp Schmid',
	),
	'Session E'		=> array(
		'E Red Camp Curtis',
		'E Blue Camp Warren',
		'E Green Camp Fudge',
		'E Yellow Camp Lopez',
		'E Purple Camp Kipp',
		'E Aqua Camp Wright',
	),
	'Session F'		=> array(
		'F Red Camp Klemm',
		'F Blue Camp Deer',
		'F Green Camp Woosnam',
		'F Yellow Camp Donaldson',
		'F Purple Camp Phillips',
		'F Aqua Camp Walker',
	),
	'Session G'		=> array(
		'G Blue Camp Whitfield',
		'G Yellow Camp Coolidge',
		'G Purple Camp Kirksey',
		'G Aqua Camp Pavliska',
	),
	'Other'			=> array(
		'X Black: Director Staff',
		'X Black: Crew',
		'A Black: General',
		'B Black: General',
		'C Black: General',
		'D Black: General',
		'E Black: General',
		'F Black: General',
		'G Black: General',
	),
);

$new_camp_name_options = array();
foreach($camp_name_options as $camp_name_key => $camp_name_option)
{
	if (is_array($camp_name_option))
		$new_camp_name_options[$camp_name_key] = array_combine($camp_name_option, $camp_name_option);
	else
		$new_camp_name_options[$camp_name_option] = $camp_name_option;
}
$camp_name_options = $new_camp_name_options;

$trip_type_options = array(
	'roadtrip'	=> 'Road Trip',
	'daytrip'	=> 'Day Trip',
);

$itinerary = array();
for($i = 1; $i <= 40; $i++)
{
	$itinerary['itinerary_'.$i.'_datetime'] = array('text', array(
		'rel' => $i,
		'label' => 'Stop #'.$i.' - Date/Time',
		'class' => 'half-width itinerary_field itinerary_field_'.$i,
		'belongsTo' => 'itinerary',
	));
	$itinerary['itinerary_'.$i.'_activity'] = array('text', array(
		'rel' => $i,
		'label' => 'Stop #'.$i.' - Activity',
		'class' => 'full-width itinerary_field itinerary_field_'.$i,
		'belongsTo' => 'itinerary',
	));
}

return array(	

	'notify' => array(
		'fcamp-ops@stuact.tamu.edu',
		'ahowell@stuact.tamu.edu',
		'cirtmail@dsa.tamu.edu',
	),
    
    'notify_message' => array(
        'fcamp-ops@stuact.tamu.edu',
    ),
	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		
		'groups'		=> array(
			
			'camp_info'		=> array(
				'legend'		=> 'Camp Information',
				'elements'		=> array(
				
					'camp_name'		=> array('select', array(
						'label' => 'Camp Session/Color/Name',
						'multiOptions' => $camp_name_options,
					)),

				),
			),
			
			'contact'		=> array(
				'legend'		=> 'Contact Information',
				'elements'		=> array(
				
					'email'			=> array('text', array(
						'label' => 'Your E-mail Address',
						'validators' => array('EmailAddress'),
						'class' => 'half-width',
						'required' => true,
					)),
					'partner_email' => array('text', array(
						'label' => 'Your Partner\'s E-mail Address (if applicable)',
						'class' => 'half-width',
						'validators' => array('EmailAddress'),
					)),

					'chairperson'	=> array('text', array(
						'label' => 'Name of Chairperson(s) attending',
                        'required' => true,
					)),
					'chairperson_cell' => array('text', array(
						'label' => 'Chairperson(s) cell phone number',
                        'required' => true,
					)),

					'advisor'		=> array('text', array(
						'label' => 'Advisor(s) attending',
                        'required' => true,
					)),
					'advisor_contact' => array('text', array(
						'label' => 'Advisor Contact Information',
                        'required' => true,
					)),

					'codemaroon_contact' => array('text', array(
						'label' => 'On-Site Code Maroon Contact',
                        'required' => true,
					)),

					'destination_contact' => array('textarea', array(
						'label' => 'Destination Contact Information',
						'class' => 'full-width half-height',
                        'required' => true,
					)),
					'alt_destination_contact' => array('textarea', array(
						'label' => 'Alternate Destination Contact Information',
						'class' => 'full-width half-height',
                        'required' => true,
					)),
				
				),
			),
			
			'trip'			=> array(
				'legend' 		=> 'Trip Information',
				'elements'		=> array(
				
					'trip_type'		=> array('radio', array(
						'label' => 'Trip Type',
						'multiOptions' => $trip_type_options,
					)),

					'multiple_cars_note' => array('markup', array(
						'label' => 'Note Regarding Multiple Vehicles',
						'markup' => '<p>If more than 4 cars are leaving from the same location at the same time, it is best to stagger departure times so a maximum of 4 cars are leaving at a time.</p>',
					)),

					'departure_date' => array('unixdate', array(
						'label' => 'Departure Date',
                        'required' => true,
					)),
					'departure_start_time' => array('text', array(
						'label' => 'Departure Time from Original Location',
                        'required' => true,
					)),
					'departure_multiple_locations' => array('textarea', array(
						'label' => 'Additional Departure Date(s) and Time(s)',
						'description' => 'If you are departing from multiple locations, please indicate the additional dates, times, and locations of departure here.',
						'class' => 'full-width half-height',
					)),
					'departure_end_time' => array('text', array(
						'label' => 'Arrival Time at Destination',
                        'required' => true,
					)),

					'destination'	=> array('textarea', array(
						'label' => 'Destination Address',
                        'description' => 'Include the name of the location, i.e. "Joe Smith\'s House".',
						'class' => 'full-width half-height',
                        'required' => true,
					)),

					'cars'			=> array('text', array(
						'label' => 'Number of Cars',
                        'required' => true,
					)),

					'return_date'	=> array('unixdate', array(
						'label' => 'Return Date',
                        'required' => true,
					)),
					'return_start_time' => array('text', array(
						'label' => 'Departure Time from Destination',
                        'required' => true,
					)),
					'return_end_time' 	=> array('text', array(
						'label' => 'Arrival Time at Original Location',
                        'required' => true,
					)),
					'return_multiple_locations' => array('textarea', array(
						'label' => 'Additional Arrival Date(s) and Time(s)',
						'description' => 'If you are returning to multiple locations, please indicate the additional dates, times, and locations of here.',
						'class' => 'full-width half-height',
					)),

					'activities'		=> array('text', array(
						'label' => 'List of Activities',
						'description' => 'For example: Swimming, Volleyball, Board Games, Watching TV, etc.',
						'class' => 'full-width',
                        'required' => true,
					)),
					'itinerary'			=> array('textarea', array(
						'label' => 'Detailed Itinerary',
						'description' => 'Include each individual\'s location/time departure to road trip, individual time of arrival, daily activities, and date/time/location of final destination after trip is complete.',
						'class' => 'full-width half-height',
                        'required' => true,
					)),
                    
                    'origin_directions' => array('file', array(
                        'label' => 'Counselor Driving Directions',
                        'description' => 'Please include the directions that each of your counselors will be following as they travel from their origin location to the roadtrip.'
                    )),
				),
			),

			'itinerary' => array(
				'legend'		=> 'Detailed Itinerary',
				'id'			=> 'itinerary',
				'elements'		=> $itinerary,
			),
			
			'general_info'	=> array(
				'legend'		=> 'General Information',
				'elements'		=> array(
				
					'purpose'		=> array('textarea', array(
						'label' => 'What is the purpose of the road trip?',
						'class' => 'full-width half-height',
                        'required' => true,
					)),
					'alternate'		=> array('textarea', array(
						'label' => 'What alternate plan do you have if this trip cannot take place due to circumstances such as weather or cancellation of housing?',
						'class' => 'full-width half-height',
                        'required' => true,
					)),
				
				),
			),
			
			'risks'			=> array(
				'legend'		=> 'Risks Involved and Mitigation Strategies',
                'description'   => 'Please ensure that you are mitigating the risks for both your primary plan and alternate plan.',
                
				'elements'		=> array(
				
					'risk_physical'		=> array('textarea', array(
						'label' => 'Physical Risk: Activity and Environment Risks',
						'class' => 'full-width half-height',
						'description' => 'When listing physical risks, think about risks involved with EVERY activity listed in the itinerary (i.e. indoor and outdoor activities, driving, cooking, eating, swimming, etc. Please also include risks associated with the environments you will be in).',
                        'required' => true,
					)),
					'risk_physical_directions' => array('textarea', array(
						'label' => 'Physical Risk: Directions to Nearest Hospital',
						'class' => 'full-width half-height',
						'description' => 'Include the directions to the nearest hospital from your destination and the address of the hospital.',
                        'required' => true,
					)),

					'risk_reputational'	=> array('textarea', array(
						'label' => 'Reputational Risk',
						'class' => 'full-width half-height',
						'description' => 'How will chairs train and/or communicate to counselors on guidelines and expectations set for this road trip/day trip?',
                        'required' => true,
					)),
					'risk_emotional' 	=> array('textarea', array(
						'label' => 'Emotional Risk',
						'class' => 'full-width half-height',
						'description' => 'Please include information on how you will address situations where members of your camp are left out, uncomfortable, or unable to attend the trip (for scheduling or cost reasons).',
                        'required' => true,
					)),
					'risk_financial'	=> array('textarea', array(
						'label' => 'Financial Risk',
						'class' => 'full-width half-height',
						'description' => '
							<div>Please describe the possible financial burdens associated with this roadtrip. Then, portray how you are mitigating these risks.</div>
							<div>What if someone can\'t afford the trip?</div>
						',
                        'required' => true,
					)),
                    
                    'risk_financial_budget' => array('file', array(
						'label' => 'Attach Budget File',
                        'description' => 'Download the <a href="'.\DF\Url::content('documents/2013_RoadtripBudget.xls').'" target="_blank">Roadtrip Budget Spreadsheet</a>, complete it, and attach it to this form using the field below.',
					)),
                    
					'risk_facilities'	=> array('textarea', array(
						'label' => 'Facilities Risk',
						'class' => 'full-width half-height',
						'description' => '
							<div>Does this facility support your purpose of being on this roadtrip?</div>
							<div>How is it conducive to providing an inclusive and accepting atmosphere for all of your camp?</div>
							<div>Is this facility capable of providing lodging or space for activities for your entire camp comfortably?</div>
							<div>What is your exit strategy in the event of an emergency?</div>
							<div>Please describe sleeping arrangements. What is someone is uncomfortable with them (i.e. sleeping on the floor, co-ed arrangements, etc.)?</div>
						',
                        'required' => true,
					)),
					'questions_for_director' => array('textarea', array(
						'label' => 'Questions for Risk Management Officer (Optional)',
						'class' => 'full-width half-height',
					)),
				
				),
			),
			
			'travelers'		=> array(
				'legend'		=> 'Travelers\' Information',
				'elements'		=> array(
				
					'travelers_desc' => array('markup', array(
						'label' => 'About the Traveler Information File',
						'markup' => '
							<p><a href="'.\DF\Url::content('documents/2013_TravelerInformationForm.xls').'" target="_blank">Download an Example File</a></p>
							<p>The excel file needs to include:</p>
							<ul>
								<li>Traveler\'s full name</li>
								<li>Student ID (UIN)</li>
								<li>Emergency contact name and number</li>
								<li>Will this person be driving?</li>
								<li>Special circumstances</li>
								<li>Traveler\'s starting location</li>
                            </ul>
                            
                            <p>For persons who will be driving vehicles, also include:</p>
                            <ul>
                                <li>Insurance expiration date</li>
                                <li>Registration expiration date</li>
                                <li>Inspection expiration date</li>
                                <li>Miles until next oil change</li>
                            </ul>
						',
					)),
					'excel_file' => array('file', array(
						'label' => 'Attach Excel File',
					)),
				
				),
			),
			'student_travel' => array(
				'legend'		=> 'Statement of Understanding',
				'elements'		=> array(
					
					'travel_guidelines' => array('markup', array(
						'label' => 'Student Travel Guidelines',
						'markup' => '
							<p>The guidelines that apply to student travel are available at this location:</p>
							<ul>
								<li><a href="http://studentactivities.tamu.edu/risk/travel" target="_blank">http://studentactivities.tamu.edu/risk/travel</a></li>
							</ul>
						',
					)),
				
					'travel_rule' => array('markup', array(
						'label' => 'Student Travel Information',
						'markup' => '
							<p>The Standard Administrative Procedure regarding student travel is available at this location:</p>
							<ul>
								<li><a href="http://rules-saps.tamu.edu/PDFs/13.04.99.M1.pdf" target="_blank">http://rules-saps.tamu.edu/PDFs/13.04.99.M1.pdf</a></li>
							</ul>

							<p>By saving this form using the button below, you are indicating that you have read and understand the Student Travel Rule, Travel Safety Guidelines, and Enhanced Travel Expectations set by Fish Camp as they apply to your trip.</p>
						',
					)),
                    
                    'related_documents' => array('markup', array(
                        'label' => 'Related Documents',
                        'markup' => '
                            <p>Please ensure that you have downloaded the forms below and distributed them to the appropriate parties.</p>
                            <div><a target="_blank" href="'.\DF\Url::content('documents/2011_ChairRoadtripAndDaytripChecklist.pdf').'">Chair Roadtrip & Daytrip Checklist (PDF)</a></div>
                            <div><a target="_blank" href="'.\DF\Url::content('documents/2013_DriverInformationAndExpectations.pdf').'">Driver Information and Expectations (PDF)</a></div>
                        ',
                    )),

                    'initials_1' => array('text', array(
						'label' => 'Initials of First Co-Chair',
						'class' => 'span1',
						'required' => true,
					)),
					'initials_2' => array('text', array(
						'label' => 'Initials of Second Co-Chair',
						'class' => 'span1',
						'required' => true,
					)),
				
				),
			),
			
			'submit'		=> array(
				'elements'		=> array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save and Submit',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);