<?php
/**
 * Appeal Request Form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'groups' => array(

			'notes' => array(
				'legend' => 'Important Notes',
				'elements' => array(

					'deadline' => array('markup', array(
						'label' => 'Deadline for Submission',
						'markup' => '<ul>
							<li>All event appeals are due by 5:00pm on the 15th day before the event date.</li>
						</ul>',
					)),

				),
			),

			'general' => array(
				'legend' => 'General Information',
				'elements' => array(

					'name' => array('text', array(
						'label' => 'Your Name',
						'class' => 'full-width',
						'required' => true,
					)),

					'uin' => array('text', array(
						'label' => 'Your UIN',
						'class' => 'half-width',
						'required' => true,
					)),

					'position' => array('radio', array(
						'label'		=> 'Your Position in the Organization',
						'multiOptions' => array(
							'dir'		=> 'Director',
							'chair'		=> 'Co-Chair',
							'counselor'	=> 'Counselor',
						),
						'required'	=> true,
					)),

					'appeal_reason' => array('radio', array(
						'label'		=> 'Reason for Appeal',
						'multiOptions' => array(
							'gpr'		=> 'GPR Deficiency',
							'attendance' => 'Event Attendance',
						),
						'required'	=> true,
					)),
					
					'appeal_description' => array('textarea', array(
						'label'		=> 'Specific Event Being Appealed (if applicable)',
						'description' => 'Please list the specific event (and date) that you are requesting an appeal for.',
						'class' 	=> 'full-width half-height',
					)),

				),
			),

			'appeal_questions' => array(
				'legend' => 'Appeal Request',
				'description' => 'Please provide a response to the following questions. If you prefer, responses can be typed in an external program and pasted into the form. There is no limit to the length of each response.',

				'elements' => array(

					'extenuating_circumstances' => array('textarea', array(
						'label' => 'Extenuating Circumstances',
						'description' => 'What extenuating circumstance(s) led to your current grade deficiency or your absence from a required event? (An extenuating circumstance is defined as a specific event or events that hindered your ability to meet your requirements – examples include prior commitments, scheduling conflicts or unforeseen circumstances like a death in the family, illness, etc.)',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'mitigating_strategies' => array('textarea', array(
						'label' => 'Mitigation Strategies',
						'description' => 'What specific strategies will you implement to ensure that the same situation does not occur again?',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'importance_of_position' => array('textarea', array(
						'label' => 'Importance of Position',
						'description' => 'Why is it important for you to continue serving in your current position?',
						'class' => 'full-width full-height',
						'required' => true,
					)),

				),
			),

			'records_disclosure' => array(
				'legend' => 'Student Records Disclosure',
				'elements' => array(

					'description' => array('markup', array(
						'markup' => '
							<p>CONSENT TO DISCLOSE STUDENT RECORDS MAINTAINED BY FISH CAMP</p>

							<p>This release represents your written consent to disclose educational records maintained by Fish Camp to specific individuals identified below. This consent will expire at the end of the appeal request process. Please read this document carefully.</p>
	
						    <p>I hereby give my voluntary consent to the <u>Fish Camp</u> to disclose the following records:</p>
						    
						    <ul>
						        <li>Contents of Individual Appeal Form File (copies of files are not provided and files may not leave Fish Camp)</li>
						    </ul>
						    
						    <p>Disclosure may be made to the following Person(s):</p>

						    <ul>
						    	<li>Fish Camp Primary, Secondary, and Additional Advisors</li>
						    </ul>
						        
						    <p>These records are being released for the purpose of:</p>
						    <ol type="1">
						        <li>Authorizing Fish Camp staff to e-mail and orally discuss, as needed, information that is contained in the file with the student organization leadership as indicated above.</li>
						        <li>Authorizing the Appeal Review Committee, comprised of Fish Camp student directors, to review and orally discuss information on file.</li>
						    </ol>
						    
						    <p><b>I understand that under the Family Educational Rights and Privacy Act of 1974 (20 USCA 1232g and 34 CFR Part 99), no disclosure of my records can be made without my written consent unless otherwise provided for in legal statutes and judicial decisions. I also understand that I may revoke this consent at any time (via written request to the Primary Advisor of Fish Camp) except to the extent that action has already been taken upon this release.</b></p>
						    
						    <p><b>I understand and agree that a committee composed of student leaders from the Fish Camp organization will review this information, along with a generalized summary of my grade history, in making a determination of my exemption. I also hereby acknowledge that the information I have provided is true and accurate.</b></p>
						',
					)),

					'initials' => array('text', array(
						'label' => 'Your Initials',
						'maxlength' => 5,
						'required' => true,
					)),
				
				),
			),

			'submit' => array(
				'elements' => array(

					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Submit Appeal Request',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
					
				),
			),
		),
	),
);