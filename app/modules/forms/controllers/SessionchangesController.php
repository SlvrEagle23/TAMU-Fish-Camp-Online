<?php
use \Entity\SessionChangeForm;
use \Entity\User;

class Forms_SessionchangesController extends \DF\Controller\Action
{
	public function permissions()
    {
		return $this->acl->isAllowed('manage registration');
    }

    public function indexAction()
    {
        $query = $this->em->createQuery('SELECT scf FROM Entity\SessionChangeForm scf ORDER BY scf.is_archived ASC, scf.timestamp DESC');
            
        $pager = new \DF\Paginator\Doctrine($query, $this->_getParam('page', 1));
        $this->view->pager = $pager;
    }

    public function exportAction()
    {
        $forms = $this->em->createQuery('SELECT scf, u FROM Entity\SessionChangeForm scf JOIN scf.user u ORDER BY scf.is_archived ASC, scf.timestamp DESC')
            ->getArrayResult();

        $export_data = array(array(
            'Archived',
            'UIN',
            'First Name',
            'Last Name',
            'Timestamp',
            'Old Sessions',
            'New Sessions',
            'Reason',
        ));

        foreach($forms as $form)
        {
            $export_data[] = array(
                ($form['is_archived']) ? 'Y' : 'N',
                $form['user']['uin'],
                $form['user']['firstname'],
                $form['user']['lastname'],
                date('m/d/Y g:ia', $form['timestamp']),
                $form['old_sessions'],
                $form['new_sessions'],
                $form['reason'],
            );
        }

        \DF\Export::csv($export_data);
    }

    public function toggleAction()
    {
    	$id = (int)$this->_getParam('id');
    	$record = SessionChangeForm::find($id);

    	$record->is_archived = !$record->is_archived;
    	$record->save();

    	$this->alert('<b>Form archived!</b>', 'green');
    	$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
    	return;
    }
}