<?php
use \Entity\Settings;
use \Entity\RefundRequest;
use \Entity\User;

class Forms_RefundsController extends \DF\Controller\Action
{
	public function permissions()
    {
		return $this->acl->isAllowed('is logged in');
    }

	protected $_config;
	public function preDispatch()
	{
		parent::preDispatch();
		$this->_config = $this->current_module_config->forms->refund->toArray();
	}

	/**
	 * User Submission
	 */

    public function indexAction()
    {
    	$this->redirectFromHere(array('action' => 'request'));
    }

	public function requestAction()
	{
        if (!RefundRequest::isOpen())
            throw new \DF\Exception\DisplayOnly('Refund requests are currently closed.');
        
		$user = \DF\Auth::getLoggedInUser();
		$form = new \DF\Form($this->_config['form']);

		if ($user->fc_received_payment != 1)
			throw new \DF\Exception\DisplayOnly('Your account is not currently recorded as having paid for Fish Camp.');
		
		if( !empty($_POST) && $form->isValid($_POST) )
        {	
			$data = $email_data = $form->getValues();
			
			$record = new RefundRequest();
			$record->user = $user;
			$record->save();
			
			$access_code = $record->access_code;
			$uploaded_files = $form->processFiles($this->_config['upload_folder'], $access_code);
			
			foreach($uploaded_files as $element_name => $element_files)
			{
				$data[$element_name] = $element_files;
				$email_data[$element_name] = array();
				foreach($element_files as $file_offset => $file_path)
				{
					$email_data[$element_name][$file_offset] = \DF\Url::content($file_path);
				}
			}
			
			$record->request_data = $data;
			$record->save();

			// Trigger the appropriate e-mail(s).
			$record->notify();
			
			$this->alert('Your request has been submitted. You will be notified when your request is processed.');
			$this->redirectHome();
			return;
		}

		$this->view->headTitle('Fish Camp Refund/Cancellation Request Form');
		$this->renderForm($form);
	}

	/**
	 * Administration
	 */

	// Administrative panel.
	public function manageAction()
	{
        $this->acl->checkPermission('view accounting','refund request director','refund request advisor');
        
        $phase = $this->_getParam('phase', RefundRequest::STATUS_PENDING_DIRECTOR);
        $this->view->phase = $phase;
        $this->view->phases = RefundRequest::getStatusNames();
        
        $query = $this->em->createQueryBuilder()
            ->select('rr, u')
            ->from('\Entity\RefundRequest', 'rr')
            ->leftJoin('rr.user', 'u')
            ->where('rr.status = :phase')
           	->setParameter('phase', $phase)
            ->orderBy('rr.created_at', 'DESC');
		
		$page_num = ($this->_hasParam('page')) ? $this->_getParam('page') : 1;
		$paginator = new \DF\Paginator\Doctrine($query, $page_num);
		
		$this->view->form_config = $this->_config;
		$this->view->pager = $paginator;
	}
	
	// Review request and send for further review.
	public function reviewAction()
	{
		$this->acl->checkPermission('view accounting','refund request director','refund request advisor');

		$access_code = $this->_getParam('code');
		$record = RefundRequest::getRepository()->findOneBy(array('access_code' => $access_code));
		
		if (!($record instanceof RefundRequest))
			throw new \DF\Exception\DisplayOnly('The form entered could not be found.');

		$form = new \DF\Form($this->_config['form']);
		
		$request_data = $record->request_data;
		$form->populate($request_data);
		
		$this->view->record = $record;
		$this->view->form = $form;
		$this->view->can_modify = $this->_canModifyForm($record);
	}

	public function processAction()
	{
		$access_code = $this->_getParam('code');
		$record = RefundRequest::getRepository()->findOneBy(array('access_code' => $access_code));
		
		if (!($record instanceof RefundRequest))
			throw new \DF\Exception\DisplayOnly('The form entered could not be found.');

		$this->_checkFormPermission($record);

		if ($record->status == RefundRequest::STATUS_PENDING_DIRECTOR)
		{
			$record->decision = $this->_getParam('decision');

			$amount = $this->_getParam('decision_amount');
			$amount = preg_replace('#[^0-9\.\-]#', '', $amount);
			$record->decision_amount = '$'.number_format((float)$amount, 2);

			$record->status = RefundRequest::STATUS_PENDING_ADVISOR;
			$record->save();
		}
		elseif ($record->status == RefundRequest::STATUS_PENDING_ADVISOR)
		{
			// Always flag the user as cancelled regardless of outcome.
			$submitter = $record->user;
			$submitter->fc_app_type = FC_TYPE_NONE;
			$submitter->fc_is_cancelled = 1;
			$submitter->save();

			if ($this->_getParam('advisor_decision') == "approve")
			{
				if ($record->decision != "Decline")
					$record->status = RefundRequest::STATUS_PENDING_ACCOUNTING;
				else
					$record->status = RefundRequest::STATUS_ARCHIVED;
				
				$record->notifySubmitter();
			}
			else
			{
				$record->status = RefundRequest::STATUS_PENDING_DIRECTOR;
			}

			$record->save();
		}

		$this->alert('<b>Refund request processed.</b>', 'green');
		$this->redirectFromHere(array('action' => 'manage', 'code' => NULL, 'decision' => NULL, 'advisor_decision' => NULL));
		return;
	}

	public function archiveAction()
	{
		$access_code = $this->_getParam('code');
		$record = RefundRequest::getRepository()->findOneBy(array('access_code' => $access_code));
		
		if (!($record instanceof RefundRequest))
			throw new \DF\Exception\DisplayOnly('The form entered could not be found.');

		$this->_checkFormPermission($record);

		$record->status = RefundRequest::STATUS_ARCHIVED;
		$record->save();

		$this->alert('<b>Refund request archived!</b>', 'green');
		$this->redirectFromHere(array('action' => 'manage', 'code' => NULL));
		return;
	}

	protected function _checkFormPermission($record)
	{
		if (!$this->_canModifyForm($record))
			throw new \DF\Exception\PermissionDenied;
	}
	protected function _canModifyForm($record)
	{
		if ($record->status == RefundRequest::STATUS_PENDING_DIRECTOR)
			return $this->acl->isAllowed('refund request director');
		elseif ($record->status == RefundRequest::STATUS_PENDING_ADVISOR)
			return $this->acl->isAllowed('refund request advisor');
		else
			return $this->acl->isAllowed('manage accounting');
	}
}