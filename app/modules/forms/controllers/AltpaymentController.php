<?php
use \Entity\Settings;
use \Entity\AltPaymentRequest;
use \Entity\User;

class Forms_AltpaymentController extends \DF\Controller\Action
{
	public function permissions()
    {
		return $this->acl->isAllowed('is logged in');
    }

	protected $_config;
	public function preDispatch()
	{
		parent::preDispatch();
		$this->_config = AltPaymentRequest::loadSettings();
	}

	/**
	 * User Submission
	 */

    public function indexAction()
    {
    	$user = $this->auth->getLoggedInUser();
		if ($user->fc_app_completed == 0 || $user->fc_received_payment == 1)
	    {
	        $this->alert('<b>You have already paid for Fish Camp!</b><br>No additional action is required at this time. You may log out from the top right corner of this page.', 'yellow');
			$this->redirectToRoute(array('module' => 'registration'));
	        return;
	    }

	    if (count($user->alt_payment_requests) > 0)
	    {
	    	$this->alert('<b>You have already submitted an alternate payment request.</b><br>No additional action is required at this time. You will be contacted by the Fish Camp team with further information regarding your request.', 'yellow');
	    	$this->redirectToRoute(array('module' => 'registration'));
	        return;
	    }

	    $close_date = Settings::getSetting('altpayment_close_date', time()+86400);
        $close_date = strtotime('Today 17:00:00', $close_date);

        if ($close_date <= time())
            throw new \DF\Exception\DisplayOnly('Alternate payments are currently unavailable.');

    	if (!$this->_hasParam('type'))
    	{
    		$this->render('index_select');
    		return;
    	}

		$type = $this->_getParam('type');

		if ($type == "scholarship")
		{
			$form_config = AltPaymentRequest::loadSettings();
			$form = new \DF\Form($form_config['form']);
		
			if (!empty($_POST) && $form->isValid($_POST))
			{
				$data = $form->getValues();
				
				$record = new AltPaymentRequest;
		        $record->user = $user;
		        $record->request_type = "scholarship";
				$record->request_data = $data;
				$record->save();

				$record->notify();
				
				$this->alert('<b>Alternate payment request submitted!</b><br />You will be contacted via e-mail with any updates that relate to your request. No additional action is required at this time. You may log out from the top right corner of this page.');
				$this->redirectToRoute(array('module' => 'registration'));
				return;
			}
			
			$this->view->headTitle('Alternate Payment Request');
			$this->renderForm($form);
			return;
		}
		else
		{
			$record = new AltPaymentRequest;
	        $record->user = $user;
	        $record->request_type = "installment";
			$record->request_data = array();
			$record->status = AltPaymentRequest::STATUS_PENDING_ACCOUNTING;
			$record->approval_type = 2;

			$record->save();

			$record->notify();
			$record->notifySubmitter();
			
			$this->alert('<b>Alternate payment request submitted!</b><br />You will be contacted via e-mail with any updates that relate to your request. No additional action is required at this time. You may log out from the top right corner of this page.');
			$this->redirectToRoute(array('module' => 'registration'));
			return;
		}
    }

	/**
	 * Administration
	 */

	// Administrative panel.
	public function manageAction()
	{
		$this->acl->checkPermission('view accounting','alt payment request director','alt payment request advisor');

        $phase = $this->_getParam('phase', 'all');

        $this->view->phase = $phase;
        $this->view->phases = AltPaymentRequest::getStatusNames();

        $query = $this->em->createQueryBuilder()
            ->select('apr, u')
            ->from('\Entity\AltPaymentRequest', 'apr')
            ->leftJoin('apr.user', 'u')
            ->orderBy('apr.created_at', 'DESC');

        if ($phase != 'all')
        {
        	$query->where('apr.status = :phase')->setParameter('phase', $phase);
        }

       	if ($this->_getParam('format') == "csv")
       	{
       		$form_config = AltPaymentRequest::loadSettings();
       		$fields = $form_config['form']['groups']['scholarship_questions']['elements'];

       		$export_headers = array(
       			'UIN',
       			'First Name',
       			'Last Name',
       			'ID',
       			'Status',
       			'Type',
       			'Decision',
       			'Scholarship Type',
       		);
       		foreach($fields as $field)
       			$export_headers[] = $field[1]['label'];

       		$export_data = array($export_headers);

       		$records = $query->getQuery()->getResult();
       		foreach($records as $record)
       		{
       			$export_row = array(
       				$record->user->uin,
       				$record->user->firstname,
       				$record->user->lastname,
       				$record->access_code,
       				$record->status_name,
       				ucfirst($record->request_type),
       				$record->approval_type_name,
       				$record->scholarship,
       			);

       			foreach($fields as $field_key => $field)
       			{
       				$field_value = $record->request_data[$field_key];

       				if ($field[0] == 'radio')
       					$field_value = ($field_value == 1) ? 'Yes' : 'No';

       				$export_row[] = $field_value;
       			}

       			$export_data[] = $export_row;
       		}

       		\DF\Export::csv($export_data);
       		return;
       	}
       	else
       	{
			$page_num = ($this->_hasParam('page')) ? $this->_getParam('page') : 1;
			$paginator = new \DF\Paginator\Doctrine($query, $page_num);
			
			$this->view->form_config = $this->_config;
			$this->view->pager = $paginator;
		}
	}
	
	// Review request and send for further review.
	public function reviewAction()
	{
		$this->acl->checkPermission('view accounting','alt payment request director','alt payment request advisor');

		$access_code = $this->_getParam('code');
		$record = AltPaymentRequest::getRepository()->findOneBy(array('access_code' => $access_code));
		
		if (!($record instanceof AltPaymentRequest))
			throw new \DF\Exception\DisplayOnly('The form entered could not be found.');

		$form = new \DF\Form($this->_config['form']);
		
		$request_data = $record->request_data;
		$form->populate($request_data);

		$this->view->scholarship_types = $this->_config['scholarship_types'];
		$this->view->record = $record;
		$this->view->form = $form;

		$this->view->can_modify = $this->_canModifyForm($record);
	}

	public function processAction()
	{
		$access_code = $this->_getParam('code');
		$record = AltPaymentRequest::getRepository()->findOneBy(array('access_code' => $access_code));
		
		if (!($record instanceof AltPaymentRequest))
			throw new \DF\Exception\DisplayOnly('The form entered could not be found.');

		$this->_checkFormPermission($record);

		if ($record->status == AltPaymentRequest::STATUS_PENDING_DIRECTOR)
		{
			$record->approval_type = $this->_getParam('approval_type');
			$record->scholarship = $this->_getParam('scholarship');

			if ($record->request_type == 'installment')
				$record->status = AltPaymentRequest::STATUS_ARCHIVED;
			else
				$record->status = AltPaymentRequest::STATUS_PENDING_ADVISOR;
			
			$record->save();
		}
		elseif ($record->status == AltPaymentRequest::STATUS_PENDING_ADVISOR)
		{
			if ($this->_getParam('decision') == "approve")
			{
				if ($record->approval_type != 0)
					$record->status = AltPaymentRequest::STATUS_PENDING_ACCOUNTING;
				else
					$record->status = AltPaymentRequest::STATUS_ARCHIVED;

				$record->notifySubmitter();
			}
			else
			{
				$record->status = AltPaymentRequest::STATUS_PENDING_DIRECTOR;
			}

			$record->save();
		}

		$this->alert('<b>Refund request processed.</b>', 'green');
		$this->redirectFromHere(array('action' => 'manage', 'code' => NULL));
		return;
	}

	public function archiveAction()
	{
		$access_code = $this->_getParam('code');
		$record = AltPaymentRequest::getRepository()->findOneBy(array('access_code' => $access_code));
		
		if (!($record instanceof AltPaymentRequest))
			throw new \DF\Exception\DisplayOnly('The form entered could not be found.');

		$this->_checkFormPermission($record);

		$record->status = AltPaymentRequest::STATUS_ARCHIVED;
		$record->save();

		$this->alert('<b>Refund request archived!</b>', 'green');
		$this->redirectFromHere(array('action' => 'manage', 'code' => NULL));
		return;
	}

	protected function _checkFormPermission($record)
	{
		if (!$this->_canModifyForm($record))
			throw new \DF\Exception\PermissionDenied;
	}
	protected function _canModifyForm($record)
	{
		if ($record->status == AltPaymentRequest::STATUS_PENDING_DIRECTOR)
			return $this->acl->isAllowed('alt payment request director');
		elseif ($record->status == AltPaymentRequest::STATUS_PENDING_ADVISOR)
			return $this->acl->isAllowed('alt payment request advisor');
		else
			return $this->acl->isAllowed('manage accounting');
	}
}