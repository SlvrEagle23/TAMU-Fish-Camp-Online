<?php
return array(
    'default' => array(
    
		'forms' => array(
			'label' => 'Forms',
			'module' => 'forms',
			'permission' => 'is logged in',
			'show_only_with_subpages' => true,
			
			'pages' => array(

				/**
				 * Cancellation/Refund Request
				 */

				'refunds_submit' => array(
					'label' => 'Cancellation/Refund Request',
					'order'	=> 0,
					'module' => 'forms',
					'controller' => 'refunds',
					'action' => 'index',
					'permission' => 'is logged in',
				),

				'refunds_manage' => array(
					'label' => 'Cancellation/Refund Request (Admin)',
					'order' => 1,
					'module' => 'forms',
					'controller' => 'refunds',
					'action' => 'manage',
					'permission' => array('refund request director', 'refund request advisor', 'view accounting'),

					'pages' => array(
						'refunds_review' => array(
							'module' => 'forms',
							'controller' => 'refunds',
							'action' => 'review',
						),
					),
				),

				/**
				 * Alternate Payment Request
				 */

				'altpayment_submit' => array(
					'label' => 'Alternate Payment Request',
					'order' => 10,
					'module' => 'forms',
					'controller' => 'altpayment',
					'action' => 'index',
					'permission' => 'is logged in',
				),

				'altpayment_manage' => array(
					'label' => 'Alternate Payment Request (Admin)',
					'order' => 11,
					'module' => 'forms',
					'controller' => 'altpayment',
					'action' => 'manage',
					'permission' => array('alt payment request director', 'alt payment request advisor', 'view accounting'),

					'pages' => array(
						'altpayment_review' => array(
							'module' => 'forms',
							'controller' => 'altpayment',
							'action' => 'review',
						),
					),
				),

				/**
				 * Appeals
				 */

				'appeals_submit' => array(
					'label' => 'Appeals',
					'order'	=> 20,
					'module' => 'forms',
					'controller' => 'appeals',
					'action' => 'index',
					'permission' => array('appeal form administrator', 'is director', 'is chair', 'is counselor'),
				),

				'appeals_manage' => array(
					'label' 	=> 'Appeals (Admin)',
					'order'	=> 21,
					'module' 	=> 'forms',
					'controller' => 'appeals',
					'action' => 'admin',
					'permission' => array('appeal form administrator', 'appeal form manager', 'appeal form reviewer', 'appeal form final'),
					
					'pages' => array(
						'appeals_view' => array(
							'module' => 'forms',
							'controller' => 'appeals',
							'action' => 'view',
						),
						
						'appeals_committee' => array(
							'module' => 'forms',
							'controller' => 'appeals',
							'action' => 'committee',
						),
						
						'appeals_final' => array(
							'module' => 'forms',
							'controller' => 'appeals',
							'action' => 'final',
						),
					),
				),

				/**
				 * Road Trip Form
				 */

				'roadtrip_form' => array(
					'label'	=> 'Road Trip Form',
					'order' => 30,
					'module' => 'forms',
					'controller' => 'roadtrip',
					'action' => 'index',
					'permission' => array('is chair', 'is director', 'is counselor'),
				),

				'roadtrips' => array(
					'label' => 'Road Trip Form (Admin)',
					'order' => 31,
					'module' => 'forms',
					'controller' => 'roadtrip',
		            'action' => 'manage',
		            'permission' => 'manage road trips',
		            
		            'pages' => array(
		                
		                'roadtrip_detail' => array(
		                    'module' => 'forms',
		                    'controller' => 'roadtrip',
		                    'action' => 'view',
		                ),
		                
		            ),
				),

				/**
				 * Session Change Forms
				 */

				'sessionchanges' => array(
					'label' 	=> 'Session Changes (Admin)',
					'order'		=> 41,
					'module' 	=> 'forms',
					'controller' => 'sessionchanges',
					'action'	=> 'index',
					'permission' => 'manage registration',
				),
                
            ),
		),
		
    ),
);