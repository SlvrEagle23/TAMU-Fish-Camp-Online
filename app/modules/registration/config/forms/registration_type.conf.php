<?php
/**
 * Personal profile form
 */

$config = \Zend_Registry::get('config');

return array(
	'method' => 'post',
	'elements' => array(

		'fc_app_type' => array('radio', array(
			'label' => 'Set Applicant Type',
			'multiOptions' => $config->fishcamp->applicant_types->toArray(),
		)),

		'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Save Changes',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),

	),
);