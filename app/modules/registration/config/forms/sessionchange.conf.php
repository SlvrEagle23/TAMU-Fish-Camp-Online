<?php
/**
 * Personal profile form
 */

$config = Zend_Registry::get('config');

// List of sessions.
$sessions = \Entity\Session::fetchAll();
$session_select = array();
foreach($sessions as $session)
{
	$session_select[$session['name']] = 'Session '.$session['name'].': '.$session['date'];
}

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'legend'		=> 'Session Change Request',
		'method'		=> 'post',
		'elements'		=> array(
            
            'fc_session_preference' => array('multicheckbox', array(
                'label' => 'Choose Sessions',
                'required' => true,
                'multiOptions' => $session_select,
            )),
            
            'note' => array('markup', array(
                'label' => 'Important Note About Session Changes',
                'markup' => '<b>Reducing your session availability will cause your space in line to move down. Adding more sessions will not affect your space in line.</b> If your space in line moves down, your ability to attend your preferred sessions may be affected. Before changing your sessions, make sure that you absolutely cannot attend the sessions you originally specified.',
            )),
            
            'reason' => array('textarea', array(
                'label' => 'Reason for Change',
                'description' => 'If there is a specific reason why you are requesting to change sessions, please specify it here.',
                'class' => 'full-width full-height',
            )),
            
            'submit'		=> array('submit', array(
                'type'	=> 'submit',
                'label'	=> 'Save Changes',
                'helper' => 'formButton',
                'class' => 'ui-button',
            )),
		),
	),
);