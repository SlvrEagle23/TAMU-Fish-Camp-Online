<?php
/**
 * Personal profile form
 */

use \Entity\Session;
use \Entity\Settings;

$config = Zend_Registry::get('config');

// List of sessions.
$sessions = Session::fetchAll();
$session_select = array();
foreach($sessions as $session)
{
	$session_select[$session['name']] = 'Session '.$session['name'].': '.$session['date'];
}

// List of special programs.
$view_renderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
$view = $view_renderer->view;

$special_programs_header = array('Program Name');
foreach($sessions as $session)
	$special_programs_header[] = $session['name'];

$special_programs_raw = $config->fishcamp->special_programs->toArray();
foreach($special_programs_raw as $program_name => $program_sessions)
{
	$special_programs_row = array($program_name);
	
	foreach($sessions as $session)
	{
		if (stristr($program_sessions, $session['name']) !== FALSE)
			$special_programs_row[] = $view->icon('cross');
		else
			$special_programs_row[] = $view->icon('tick');
	}
	
	$special_programs_body[] = $special_programs_row;
}

$special_programs = '<p>If you will be attending or involved in any of the following activities, check the table below for conflicts with Fish Camp sessions.</p>';
$special_programs .= $view->table()
	->setOptions(array(
		'style' => 'width: 100%',
		'class' => 'datatable zebra',
	))
	->setHeader(array($special_programs_header))
	->setBody($special_programs_body);

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'legend'		=> 'Registration Information',
		'method'		=> 'post',
		'groups'		=> array(
		
			'sessions'	=> array(
				'legend'	=> 'Sessions',
				'elements'	=> array(
				
					'specialprograms' => array('markup', array(
						'label' => 'Special Programs',
						'markup' => $special_programs,
					)),
                    
					'fc_session_preference' => array('multicheckbox', array(
						'label' => 'Choose Sessions',
						'required' => true,
						'multiOptions' => $session_select,
					)),
                    
                    'fc_assigned_session' => array('select', array(
                        'label' => 'Session You Are Attending',
						'required' => true,
						'multiOptions' => $session_select,
                    )),
					
				),
			),
			
			'agreements' => array(
				'legend'	=> 'Policy, Behavior, and Liability Agreement',
				'elements'	=> array(
				
					'policy_agreement'		=> array('markup', array(
						'label'	=> 'Policy Agreement',
						'markup' => Settings::getSetting('policy_agreement', ''),
					)),
					'behavior_agreement'	=> array('markup', array(
						'label' => 'Behavior Agreement',
						'markup' => Settings::getSetting('behavior_agreement', ''),
					)),
					'liability_waiver'		=> array('markup', array(
						'label' => 'Liability Waiver',
						'markup' => Settings::getSetting('liability_waiver', ''),
					)),
					'photo_release'			=> array('markup', array(
						'label' => 'Photo Release',
						'markup' => Settings::getSetting('photo_release', ''),
					)),
					'fc_initials'			=> array('text', array(
						'label' => 'Initials',
						'required' => true,
                        'maxLength' => 5,
						'description' => 'SIGNING THIS DOCUMENT INVOLVES THE WAIVER OF VALUABLE LEGAL RIGHTS. CONSULT YOUR ATTORNEY BEFORE SIGNING THIS DOCUMENT.',
					)),
					
				),
			),

			'counselor_application' => array(
				'legend'	=> 'Counselor Application Questions',
				'elements' 	=> array(

					'application_type' => array('radio', array(
						'label' => 'Are you applying to be a counselor or member of Crew?',
						'multiOptions' => array(
							'counselor' => 'Camp Counselor',
							'crew' => 'Crew Counselor',
							'both' => 'Both',
						),
						'belongsTo' => 'fc_counselor_application',
						'required' => true,
					)),

					'camp_history' => array('textarea', array(
						'label' => 'Enter Your Camp History (Year, Camp Name, Position)',
						'description' => 'Please note that experience is NOT necessary! **NOTE**: Failure to list all previous counselor or staff experience may result in the removal of your application from consideration. List previous camps even if you were unable to attend camp.',
						'belongsTo' => 'fc_counselor_application',
						'class' => 'full-width full-height',
					)),

					'experience' => array('textarea', array(
						'label' => 'In what ways do you think the Fish Camp experience will help better the Class of '.(date('Y')+4).' and Texas A&M University?',
						'description' => 'Please limit submission to 600 words.',
						'belongsTo' => 'fc_counselor_application',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'inexperienced_motivates' => array('textarea', array(
						'label' => 'Inexperienced Counselors Only: What motivates you to apply for Fish Camp '.date('Y').' and what qualities would make you a good counselor?',
						'description' => 'Please limit submission to 600 words.',
						'belongsTo' => 'fc_counselor_application',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'experienced_responsibilities' => array('textarea', array(
						'label' => 'Experienced Counselors Only: What are your responsibilities as a counselor and what goals do you have for this year?',
						'description' => 'Please limit submission to 600 words.',
						'belongsTo' => 'fc_counselor_application',
						'class' => 'full-width full-height',
						'required' => true,
					)),

				),
			),
			
			'submit'	=> array(
				'elements'	=> array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save Changes',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);