<?php
$config = \Zend_Registry::get('config');

return array(	
'method'		=> 'post',
	'elements' => array(

		'type' => array('select', array(
			'label' => 'Registrant Type',
			'multiOptions' => $config->fishcamp->registration_types->toArray(),
			'required' => true,
		)),

		'active_start' => array('unixdatetime', array(
			'label' => 'Access Begins',
			'description' => 'The date/time when the access code will begin working. Defaults to today.',
			'required' => true,
		)),

		'active_end' => array('unixdatetime', array(
			'label' => 'Access Expires',
			'description' => 'The date/time when the access code will expire. After this time, the code will no longer be usable.',
			'required' => true,
		)),

		'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Save Changes',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);