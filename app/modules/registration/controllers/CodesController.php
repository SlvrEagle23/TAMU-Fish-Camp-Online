<?php
use \Entity\User;
use \Entity\AccessCode;

class Registration_CodesController extends \DF\Controller\Action
{
    public function permissions()
    {
		return $this->acl->isAllowed('manage registration');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {
        $this->view->codes = AccessCode::fetchAll();
    }

    public function editAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->access_code);

        $id = (int)$this->_getParam('id');
        if ($id != 0)
        {
            $record = AccessCode::find($id);
            $form->setDefaults($record->toArray());
        }

        if ($_POST && $form->isValid($_POST))
        {
            $data = $form->getValues();

            if (!($record instanceof AccessCode))
                $record = new AccessCode;

            $record->fromArray($data);
            $record->save();

            $this->alert('<b>Access code updated!</b>', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Access Code');
        $this->renderForm($form);
    }

    public function deleteAction()
    {
        $id = (int)$this->_getParam('id');
        $record = AccessCode::find($id);

        if ($record instanceof AccessCode)
            $record->delete();

        $this->alert('<b>Access code deleted!</b>', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
    }
}