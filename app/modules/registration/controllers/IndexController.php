<?php
use \Entity\User;
use \Entity\Settings;
use \Entity\AccessCode;
use \Entity\RefundRequest;

class Registration_IndexController extends \DF\Controller\Action
{
	// Registration type.
	protected $type;
	
    public function permissions()
    {
        return $this->acl->isAllowed('is logged in');
    }
	
	public function preDispatch()
	{
        $exempt_actions = array('inactive', 'select', 'complete', 'sessionchange', 'pay');

		$user = $this->auth->getLoggedInUser();
		if ($this->_hasParam('type'))
			$this->type = (int)$this->_getParam('type');
		else if ($user->fc_app_type)
			$this->type = $registration_type = $user->fc_app_type;

		if (!in_array($this->_getParam('action'), $exempt_actions))
		{
			if ($this->type)
			{
                // !$this->acl->isAllowed('manage registration')
				if (\FCO\Registration::getStatus($this->type) != FC_STATUS_OPEN)
				{
                    $reg_session = new \Zend_Session_Namespace('fc_registration');
                    
                    if ($this->_hasParam('type_access'))
                        $reg_session->access_code = $this->_getParam('type_access', '');
                    else
                        $reg_session->access_code = '';

                    if (!AccessCode::check($this->type, $reg_session->access_code))
                    {
                        if ($user->fc_app_completed != 0)
                        {
                            $this->redirectToRoute(array('module' => 'registration', 'controller' => 'index', 'action' => 'complete'));
                            return;
                        }
                        else
                        {
                            $this->alert('The registration type you selected is not currently active!', 'red');
                            $this->redirectToRoute(array('module' => 'registration', 'controller' => 'index', 'action' => 'select'));
                        }
                    }
				}
			}
			else
			{
				$this->redirectToRoute(array('module' => 'registration', 'controller' => 'index', 'action' => 'select'));
			}
		}
	}
	
    /**
     * Main display.
     */
    public function indexAction()
    {
		$user = $this->auth->getLoggedInUser();
		
		// Display registration form if registration is not completed.
		if ($user->fc_app_completed == 0)
		{
			$form = new \FCO\Form\Registration($this->type);				   
			if (!empty($_POST) && $form->isValid($_POST))
			{
				$form->save();
                
                $form_values = $form->getValues();
                
                \DF\Messenger::send(array(
                    'to'        => array($form_values['email'], $form_values['email2']),
                    'subject'   => 'Online Registration Submitted',
                    'template'  => 'confirmation',
                ));
                
				$this->alert('<b>Registration details submitted!</b><br />To complete your registration, follow the instructions below.');
				
				$this->redirectHere();
			}
			
			$this->view->form = $form;
		}
		// Display registration completion update.
		else
		{
            $this->redirectFromHere(array('action' => 'complete'));
		}
	}
	
	/**
	 * Type selection page.
	 */
	public function selectAction()
	{
		$this->view->registration_types = $this->config->fishcamp->registration_types->toArray();
	}
    
    /**
     * Registration complete page.
     */
    public function completeAction()
    {
        $user = $this->auth->getLoggedInUser();
        
        // Direct back to registration if user hasn't completed it yet.
        if ($user->fc_app_completed == 0)
        {
            $this->redirectFromHere(array('action' => 'index'));
            return;
        }

        if ($user['fc_app_type'] == FC_TYPE_FRESHMAN && $user['fc_assigned_session'] && $user['fc_assignment_override'] == 1)
        {
            $this->view->session = $user['fc_assigned_session'];
            $this->view->user = $user;
            
            $this->render('complete_assignment');
            return;
        }
        else
        {
            $sessions_raw = str_split($user->fc_session_preference);
            $this->view->sessions = $sessions_raw;
            $this->view->session_names = \Entity\Session::fetchSelect();

            $this->view->user = $user;
            $this->view->app_type = $this->type;
        
            $registration_fee = Settings::getSetting('registration_'.$this->type.'_due', 0);
            $this->view->registration_fee = $registration_fee;
            
            $payment_config = $this->config->fishcamp->payment_config->toArray();
            $this->view->show_payment = (isset($payment_config[$this->type]));

            $this->render('complete');
            return;
        }
    }
	
	/**
	 * Online payment page.
	 */
	public function payAction()
	{
		$user = $this->auth->getLoggedInUser();

        $payment_enabled = Settings::getSetting('payment_enabled', 1);
        if (!$payment_enabled)
            throw new \DF\Exception\DisplayOnly('Payment is not currently enabled. Please contact the Fish Camp staff for more information.');
        
		if ($user->fc_app_completed == 0 || $user->fc_received_payment == 1)
        {
            $this->alert('<b>Payment complete!</b><br />If all of the items on the list below are complete, no additional action is required at this time. You may log out from the top right corner of this page.');
			$this->redirectFromHere(array('action' => 'index'));
            return;
        }
		
		$this->view->user = $user;
		
		$payment_config = $this->config->fishcamp->payment_config->toArray();
		
		if (isset($payment_config[$this->type]))
		{
			$this_config = $payment_config[$this->type];
			
			switch($this_config['type'])
			{
				case "touchnet":
					$registration_fee = Settings::getSetting('registration_'.$this->type.'_due', 0);
					$registration_service_charge = Settings::getSetting('upay_service_charge', 0);
                    
					$this->view->registration_fee = $registration_fee;
					$this->view->registration_service_charge = $registration_service_charge;
                    
					$total_amount = $registration_fee + $registration_service_charge;
					$this->view->total_amount = $total_amount;
                    
					$touchnet_data = \DF\Service\TouchNet::createTransaction($total_amount, $user);
					$this->view->upay = $touchnet_data;
					$this->render();
					return;
				break;
				
				case "marketplace":
					$url = $this_config['url'];
					$this->redirect($url);
					return;
				break;
			}
		}
		else
		{
			$this->alert('This type of registration does not require payment.');
			$this->redirectFromHere(array('action' => 'index'));
            return;
		}
	}
    
    // Session change process (automated and e-mail)
    public function sessionchangeAction()
    {
        $user = $this->auth->getLoggedInUser();
		if ($user->fc_app_completed == 0)
        {
            $this->alert('<b>You have not registered yet!</b><br />You can only change your session once you have completed the initial registration process.');
			$this->redirectFromHere(array('action' => 'index'));
            return;
        }
        
        $deadline = Settings::getSetting('session_change_deadline', 0);
        if (time() >= $deadline)
            throw new \DF\Exception\DisplayOnly('The deadline to change sessions has passed.');
        
        $form = new \DF\Form($this->current_module_config->forms->sessionchange->form);
        $form->setDefaults(array('fc_session_preference' => str_split($user['fc_session_preference'])));

        $registration_status = \FCO\Registration::getStatus(FC_TYPE_FRESHMAN);
        $registration_open = ($registration_status != FC_STATUS_CLOSED);

        if (!$registration_open)
            $this->alert('<b>Registration is currently closed; session changes will not take effect automatically.</b><br>Your session change request will be sent to the Fish Camp director staff for review and approval.', 'blue');
        
        if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
            $old_preference = $user->fc_session_preference;
            $new_preference = implode('', $data['fc_session_preference']);

            if ($registration_open)
            {
                $user->fc_session_preference = $new_preference;

                if (strlen($new_preference) < strlen($old_preference))
                	$user->fc_sessions_selected = time();

                $user->save();

                $this->alert('<b>Session change processed!</b><br>Your session preference has been processed successfully.', 'green');
            }
            else
            {
                $record = new \Entity\SessionChangeForm;
                $record->user = $user;
                $record->old_sessions = $old_preference;
                $record->new_sessions = $new_preference;
                $record->reason = $data['reason'];
                $record->save();
                
                \DF\Messenger::send(array(
                    'to'        => array('fcamp-ir@dsa.tamu.edu'),
                    'subject'   => 'Session Change Request',
                    'template'  => 'session_change',
                    'module'    => 'registration',
                    'vars'      => $record->toArray(TRUE, TRUE),
                ));

                $this->alert('<b>Session change submitted!</b><br>Your request for a session change has been sent to the Fish Camp director team. A member of the Fish Camp staff will contact you regarding the status of your change.', 'green');
            }
            
            
            $this->redirectFromHere(array('action' => 'index'));
            return;
        }

        $this->view->headTitle('Session Change Request Form');
        $this->renderForm($form);
    }

    // Cancellation Page
    public function cancelAction()
    {
        $user = $this->auth->getLoggedInUser();
        if ($user->fc_app_completed == 0)
        {
            $this->alert('<b>You have not registered yet!</b><br>You can only change your session once you have completed the initial registration process.');
            $this->redirectFromHere(array('action' => 'index'));
            return;
        }

        if ($user->fc_received_payment != 0 && RefundRequest::isOpen())
        {
            $this->alert('<b>You have already paid for Fish Camp.</b><br>Please complete the refund request form below to cancel your registration.');
            $this->redirectToRoute(array('module' => 'forms', 'controller' => 'refunds'));
            return;
        }

        $user->fc_app_type = FC_TYPE_NONE;
        $user->save();

        $this->alert('<b>Registration successfully cancelled.</b><br>If registration is still open, you can resubmit your registration information by clicking the "Register Now" link on the menu.', 'green');
        $this->redirectHome();
    }
}