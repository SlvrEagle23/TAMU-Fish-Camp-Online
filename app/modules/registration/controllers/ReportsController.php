<?php
use \Entity\User;
use \Entity\Checkin;
use \Entity\Touchnet;

class Registration_ReportsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('manage registration');
    }
    
    /* List of reports. */
    public function indexAction()
    {
        $fc = \Zend_Registry::get('fc');
        $fc_settings = $fc->getSettings();
        
        $this->view->sessions = $fc_settings['sessions'];
    }

    /* No-Show Report */
    public function checkinAction()
    {
        $session = $this->_getParam('session');

        $fc = \Zend_Registry::get('fc');
        $all_campers = $fc->getEligibleCampers('u.fc_assigned_session ASC, u.fc_assigned_camp ASC, u.fc_assigned_cabin ASC, u.lastname ASC');

        $all_checkin = Checkin::fetchArray();
        $checked_in = array();
        foreach($all_checkin as $checkin_row)
            $checked_in[$checkin_row['user_id']] = $checkin_row['user_id'];

        $stats = array();
        $export_campers = array();

        foreach($all_campers as $camper)
        {
            if ($camper['fc_assigned_session'] == $session)
            {
                $is_checked_in = (isset($checked_in[$camper['id']]));
                $camper['is_checked_in'] = ($is_checked_in) ? 'Y' : 'N';

                if ($is_checked_in)
                {
                    $stats['checked_in']++;
                    $export_campers['checked_in'][] = $camper;
                }
                else
                {
                    $stats['not_checked_in']++;
                    $export_campers['not_checked_in'][] = $camper;
                }
            }
        }

        $export_data = array(
            array('Session '.$session.' Check-In Report'),
            array('Checked In:', $stats['checked_in']),
            array('Not Checked In: '.$stats['not_checked_in']),
        );

        $groups = array('not_checked_in' => 'No-Show (Not Checked In) Freshmen', 'checked_in' => 'Checked In Freshmen');
        foreach($groups as $group_id => $group_name)
        {
            $export_data[] = array(' ');
            $export_data[] = array($group_name.' ('.count($export_campers[$group_id]).' Total)');
            $export_data[] = array(
                'UIN', 
                'First Name', 
                'Last Name', 
                'Session', 
                'Camp', 
                'Cabin', 
                'Bus',
                'Primary E-mail', 
                'Secondary E-mail',
                'Parent Name',
                'Parent Phone',
                'Parent E-mail',
                'Alt 1 Name',
                'Alt 1 Phone',
                'Alt 1 E-mail',
                'Alt 2 Name',
                'Alt 2 Phone',
                'Alt 2 E-mail',
            );

            foreach($export_campers[$group_id] as $camper)
            {
                $export_data[] = array(
                    $camper['uin'],
                    $camper['firstname'],
                    $camper['lastname'],
                    $camper['fc_assigned_session'],
                    $camper['fc_assigned_camp'],
                    $camper['fc_assigned_cabin'],
                    $camper['fc_assigned_bus'],
                    $camper['email'],
                    $camper['email2'],
                    $camper['parentname'],
                    $camper['parentphone'],
                    $camper['parentemail'],
                    $camper['alt1name'],
                    $camper['alt1phone'],
                    $camper['alt1email'],
                    $camper['alt2name'],
                    $camper['alt2phone'],
                    $camper['alt2email'],
                );
            }
        }

        \DF\Export::csv($export_data);
        return;
    }
    
    /* Special accommodations report. */
    public function specialAction()
    {
        $blank_strings = array('', ' ', '-', 'None', 'None.', 'none', 'N/A', 'n/a', 'No Known', 'no known');
        
        $type = $this->_getParam('type', FC_TYPE_FRESHMAN);
        
        $special_users = $this->em->createQueryBuilder()
            ->select('u')
            ->from('\Entity\User', 'u')
            ->where('u.fc_app_type = '.$type)
            ->andWhere('u.fc_app_completed != 0')
            ->andWhere('(u.allergies NOT IN (:blank) OR u.medications NOT IN (:blank) OR u.othermedical NOT IN (:blank) OR u.specialaccommodations NOT IN (:blank))')
            ->setParameter('blank', $blank_strings)
            ->orderBy('u.fc_app_completed', 'ASC');
        
        $special_users = $special_users->getQuery()->getArrayResult();

        $fields_to_show = array(
            'allergies' => 'Allergies',
            'medications' => 'Medications',
            'othermedical' => 'Other Medical',
            'specialaccommodations' => 'Special Accommodations'
        );

        foreach((array)$special_users as $user_offset => $user)
        {
            $user_special = array();

            foreach($fields_to_show as $field_key => $field_name)
            {
                $field_val = $user[$field_key];
                $field_val_lower = strtolower($field_val);

                if (!empty($field_val) && substr($field_val_lower, 0, 3) != 'n/a' && substr($field_val_lower, 0, 4) != 'none')
                    $user_special[$field_name] = $field_val;
            }

            if (!$user_special)
                unset($special_users[$user_offset]);
            else
                $special_users[$user_offset]['special'] = $user_special;
        }
        
        if ($this->_getParam('format') == "csv")
        {
            $this->doNotRender();
            
            $export_data = array(array(
                'UIN',
                'Name',
                'Registration Date',
                'Session',
                'Camp',
                'Cabin',
                'Bus',
                'Allergies',
                'Medications',
                'Other Medical',
                'Special Accommodations',
                'Notes',
            ));
            
            foreach($special_users as $special)
            {
                $export_data[] = array(
                    $special['uin'],
                    $special['firstname'].' '.$special['lastname'],
                    date('m/d/Y g:ia', $special['fc_app_completed']),
                    $special['fc_assigned_session'],
                    $special['fc_assigned_camp'],
                    $special['fc_assigned_cabin'],
                    $special['fc_assigned_bus'],
                    $special['allergies'],
                    $special['medications'],
                    $special['othermedical'],
                    $special['specialaccommodations'],
                    $special['specialaccommodation_notes'],
                );
            }
            
            \DF\Export::csv($export_data);
            exit;
        }
        else
        {
            $this->view->type = $type;
            $this->view->types = $this->config->fishcamp->registration_types->toArray();

            $this->view->pager = new \DF\Paginator($special_users, $this->_getParam('page', 1));
        }
    }

    public function specialnotesAction()
    {
        if ($this->_hasParam('notes'))
        {
            $notes = (array)$this->_getParam('notes', array());

            foreach($notes as $user_id => $note)
            {
                $user = \Entity\User::find($user_id);
                $user->specialaccommodation_notes = $note;
                $this->em->persist($user);
            }

            $this->em->flush();
        }

        $this->alert('<b>Notes Saved: '.date('n/d/Y g:ia').'</b>', 'green');
        $this->redirectFromHere(array('action' => 'special'));
        return;
    }

    /* Counselor Demographic Comparison Report */
    public function demographicsAction()
    {
        $view = $this->_getParam('view', 'counselors');
        $this->view->display = $view;

        if ($view == "counselors")
            $types = array(FC_TYPE_COUNSELOR, FC_TYPE_COUNSELOR2);
        else if ($view == "freshmen")
            $types = array(FC_TYPE_FRESHMAN);

        $counselors_raw = $this->em->createQuery('SELECT u FROM Entity\User u WHERE u.fc_app_type IN (:types) AND u.fc_app_completed != 0 AND u.fc_received_papers = 1 AND u.fc_received_payment = 1 ORDER BY u.uin ASC')
            ->setParameter('types', $types)
            ->getArrayResult();

        $reg_elements = $this->current_module_config->forms->profile->form->groups->personal->elements->toArray();
        $race_options = $reg_elements['race'][1]['multiOptions'];
        $college_options = $reg_elements['college'][1]['multiOptions'];

        $categories = array(
            'Gender' => array('Male', 'Female'),
            'Ethnicity' => array(),
            'College' => array(),
            'Total' => array('Total'),
        );

        foreach($race_options as $race_id => $race_name)
            $categories['Ethnicity'][] = $race_name;

        foreach($college_options as $college_id => $college_name)
            $categories['College'][] = $college_name;

        $stats = array();
        foreach($categories as $cat_name => $cat_area)
        {
            foreach($cat_area as $cat_type)
            {
                $stats[$cat_name][$cat_type] = array(
                    'assigned'      => 0,
                    'assigned_pct'  => 0,
                    'unassigned'    => 0,
                    'unassigned_pct' => 0,
                    'total'         => 0,
                    'total_pct'     => 0,
                );
            }
        }

        foreach($counselors_raw as $counselor)
        {
            $type = ($counselor['fc_app_type'] == FC_TYPE_COUNSELOR2) ? 'assigned' : 'unassigned';

            $gender = ($counselor['gender'] == "F") ? 'Female' : 'Male';
            $stats['Gender'][$gender][$type]++;
            $stats['Gender'][$gender]['total']++;

            $race = ($counselor['race']) ? $race_options[$counselor['race']] : $race_options['NA'];
            $stats['Ethnicity'][$race][$type]++;
            $stats['Ethnicity'][$race]['total']++;

            $college = ($counselor['college']) ? $college_options[$counselor['college']] : $college_options['NA'];
            $stats['College'][$college][$type]++;
            $stats['College'][$college]['total']++;

            $stats['Total']['Total'][$type]++;
            $stats['Total']['Total']['total']++;
        }

        $num_unassigned = $stats['Total']['Total']['unassigned'];
        $num_assigned = $stats['Total']['Total']['assigned'];
        $num_total = $stats['Total']['Total']['total'];

        foreach($stats as &$stat_group)
        {
            foreach($stat_group as &$stat_item)
            {
                if ($num_assigned > 0)
                    $stat_item['assigned_pct'] = round(($stat_item['assigned'] / $num_assigned) * 100, 2);
                if ($num_unassigned)
                    $stat_item['unassigned_pct'] = round(($stat_item['unassigned'] / $num_unassigned) * 100, 2);
                if ($num_total)
                    $stat_item['total_pct'] = round(($stat_item['total'] / $num_total) * 100, 2);
            }
        }

        $this->view->stats = $stats;
    }
    
    /* Shirt size report. */
    public function shirtsizesAction()
    {
        $this->doNotRender();

        $type = $this->_getParam('type', FC_TYPE_FRESHMAN);
        
        $all_campers = $this->em->createQuery('SELECT u FROM \Entity\User u WHERE u.fc_app_type = '.$type.' AND u.fc_app_completed != 0 ORDER BY u.fc_assigned_session ASC, u.fc_assigned_camp ASC')->getArrayResult();
        
        $shirt_size_report = array();
        foreach($all_campers as $camper)
        {
            $session = $camper['fc_assigned_session'];
            $camp = $camper['fc_assigned_camp'];
            $shirt_size = $camper['shirt_size'];
            
            if ($session && $camp && $camp != "WL" && $session != "None")
            {
                $shirt_size_report[$session]['All'][$shirt_size]++;
                $shirt_size_report[$session][$camp][$shirt_size]++;
            }
            else
            {
                $shirt_size_report['None']['All'][$shirt_size]++;
            }
        }
        
        $reg_form = $this->current_module_config->forms->profile->form->toArray();
        $shirt_size_opts = $reg_form['groups']['personal']['elements']['shirt_size'][1]['multiOptions'];
        
        $header_row = array('Session', 'Camp');
        
        foreach($shirt_size_opts as $shirt_size_abbr => $shirt_size_name)
        {
            $header_row[] = $shirt_size_name;
        }
        
        $export_data = array($header_row);
        
        foreach($shirt_size_report as $session => $camps)
        {
            foreach($camps as $camp => $sizes)
            {
                $export_row = array();
                $export_row[] = $session;
                $export_row[] = $camp;
                
                foreach($shirt_size_opts as $shirt_size_abbr => $shirt_size_name)
                {
                    $export_row[] = (int)$sizes[$shirt_size_abbr];
                }
                
                $export_data[] = $export_row;
            }
        }
        
        \DF\Export::csv($export_data);
        return;
    }
    
    /* Alternate payment report.
    public function altpaymentAction()
    {
        $requests = array();
        
        // Process requests submitted in newer system.
        $alt_payment_requests = $this->em->createQuery('SELECT a, u FROM \Entity\AltPaymentRequest a LEFT JOIN a.user u')
            ->getArrayResult();
        
        foreach($alt_payment_requests as $request)
        {
            $requests[] = array(
                'user'      => $request['user'],
                'timestamp' => $request['created_at']->getTimestamp(),
                'alt_option' => $request['request_data']['altpayment_option'],
                'alt_circumstances' => $request['request_data']['circumstances'],
                'alt_amount' => $request['request_data']['amount'],
            );
        }
        
        // Process requests submitted in older system.
        $legacy_requests = $this->em->createQuery('SELECT u FROM \Entity\User u WHERE u.fc_app_type = :fc_app_type AND u.fc_app_completed != 0 AND u.fc_altpayment_option IS NOT NULL')
            ->setParameter('fc_app_type', FC_TYPE_FRESHMAN)
            ->getArrayResult();
        
        foreach($legacy_requests as $request)
        {
            $requests[] = array(
                'user'      => $request,
                'timestamp' => $request['fc_app_completed'],
                'alt_option' => $request['fc_altpayment_option'],
                'alt_circumstances' => $request['fc_altpayment_circumstances'],
                'alt_amount' => $request['fc_altpayment_amount'],
            );
        }
        
        uasort($requests, array($this, '_sortByTimestamp'));
        
        switch($this->_getParam('format', 'csv'))
        {
            case "csv":
            default:
                $this->doNotRender();
                
                $export_data = array(array(
                    'First Name',
                    'Last Name',
                    'UIN',
                    'E-mail Address',
                    'Phone',
                    'Date Submitted',
                    'Alt Payment Option',
                    'Alt Payment Circumstances',
                    'Alt Payment Amount',
                ));
                
                foreach($requests as $request)
                {
                    $export_data[] = array(
                        $request['user']['firstname'],
                        $request['user']['lastname'],
                        $request['user']['uin'],
                        $request['user']['email'],
                        $request['user']['phone'],
                        date('m/d/Y g:ia', $request['timestamp']),
                        $request['alt_option'],
                        $request['alt_circumstances'],
                        $request['alt_amount'],
                    );
                }
                
                \DF\Export::csv($export_data);
                exit;
            break;
        }
    }
    */
    
    protected function _sortByTimestamp($a_row, $b_row)
    {
        $a = $a_row['timestamp'];
        $b = $b_row['timestamp'];
        
        if ($a == $b)
            return 0;
        
        return ($a < $b) ? -1 : 1;
    }
    
    /* Print Master Lists */
    public function masterlistAction()
    {
        $type = $this->_getParam('type', FC_TYPE_FRESHMAN);
        
        // Redirect to the search feature, which can handle this request.
        $criteria = array(
            'fc_app_type'       => array($type),
            'fc_received_payment' => 'yes',
            'fc_received_papers' => 'yes',
            'master_report'     => 1,
        );
        
        $this->redirectToRoute(array(
            'module'        => 'registration',
            'controller'    => 'search',
            'action'        => 'index',
            'criteria'      => base64_encode(\Zend_Json::encode($criteria)),
            'format'        => 'csv',
        ));
        return;
    }
    
    /* Print fish packets. */
    public function fishpacketsAction()
    {
        $this->doNotRender();
        
        $rows_per_page = 10;
        $x_offset = 20;
        $y_offset = 740;
        
        $pdf_fonts = \DF\Export::getPdfFonts();
        
        // Get all campers.
        if ($this->_hasParam('test'))
        {
            $base_camper = User::getRepository()->findOneBy(array('uin' => '218001759'))->toArray();
            $all_campers = array_fill(0, $rows_per_page, $base_camper);
        }
        else if ($this->_hasParam('users'))
        {
            $users_raw = trim($this->_getParam('users'));
            $users = explode(',', $users_raw);
            
            $all_campers = $this->em->createQuery('SELECT u FROM \Entity\User u WHERE u.id IN (:user_ids) ORDER BY u.fc_assigned_session ASC, u.fc_assigned_camp ASC, u.lastname ASC, u.firstname ASC')
                ->setParameter('user_ids', $users)
                ->getArrayResult();
        }
        else
        {
            $fc = \Zend_Registry::get('fc');
            $all_campers = $fc->getEligibleCampers('u.fc_assigned_session ASC, u.fc_assigned_camp ASC, u.lastname ASC, u.firstname ASC');
            
            if ($this->_hasParam('session'))
            {
                $session = strtoupper($this->_getParam('session', 'A'));
                
                $new_camper_list = array();
                foreach($all_campers as $camper_key => $camper)
                {
                    if (strcmp(strtoupper($camper['fc_assigned_session']), $session) == 0)
                        $new_camper_list[] = $camper;
                }
                $all_campers = $new_camper_list;
            }
        }
        
        $full_pdf = \Zend_Pdf::load(DF_INCLUDE_STATIC.'/AveryBase.pdf');
        $pdf_page_raw = $full_pdf->pages[0];
        
        $page = new \Zend_Pdf_Page($pdf_page_raw);
        $i = 0;
        foreach($all_campers as $camper)
        {
            if ($i == $rows_per_page)
            {
                $full_pdf->pages[] = $page;
                $page = new \Zend_Pdf_Page($pdf_page_raw);
                $i = 0;
            }
        
            $base_x = $x_offset + (300 * ($i % 2));
            $base_y = $y_offset - (144 * floor($i / 2));
            
            $encoded_uin = 'FC'.str_pad($camper['id'], 5, '0', STR_PAD_LEFT);
            
            $page->setFont($pdf_fonts['barcode'], 58);
            $page->drawText('*'.$encoded_uin.'*', $base_x+37, $base_y-70);
                
            $page->setFont($pdf_fonts['trebuchet_bold'], 25);
            
            $camper_name = substr(htmlspecialchars_decode($camper['firstname'].' '.$camper['lastname']), 0, 20);
            $page->drawText($camper_name, $base_x, $base_y - 18);
            
            $page->setFont($pdf_fonts['trebuchet'], 10);
            $page->drawText('Session/Camp:', $base_x, $base_y - 85);
            $page->drawText('Cabin:', $base_x+140, $base_y - 85);
            $page->drawText('Bus:', $base_x+240, $base_y - 85);
            
            $page->setFont($pdf_fonts['trebuchet_bold'], 12);
            $page->drawText($camper['fc_assigned_session'].' '.$camper['fc_assigned_camp'], $base_x, $base_y - 100);
            
            $page->setFont($pdf_fonts['trebuchet_bold'], 12);
            $page->drawText($camper['fc_assigned_cabin'], $base_x+140, $base_y-100);
            $page->drawText($camper['fc_assigned_bus'], $base_x+240, $base_y-100);
            
            $page->setFont($pdf_fonts['trebuchet'], 8);
            $page->drawText('Fish Camp '.date('Y'), $base_x, $base_y - 120);
            
            $page->drawText($encoded_uin, $base_x+238, $base_y-120);
            
            $i++;
        }
        
        $full_pdf->pages[] = $page;
        unset($full_pdf->pages[0]);
        
        // Printing the results to the screen.
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: no-cache, must-revalidate");
        header("Content-Type: application/pdf");
        header("Content-Disposition: inline; filename=FishPackets.pdf");
        
        echo $full_pdf->render();
        return;
    }
    
    /* Print medical releases. */
    public function releasesAction()
    {
        ini_set('memory_limit', -1);
        
        $this->doNotRender();
        
        $full_pdf = \Zend_Pdf::load(DF_INCLUDE_STATIC.'/documents/MedicalReleaseTemplate.pdf');
        $pdf_page_raw = $full_pdf->pages[0];
        
        $pdf_page = new \Zend_Pdf_Page($pdf_page_raw);
        
        $courier = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_COURIER);
        $courier_bold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_COURIER_BOLD);
        
        $type = $this->_getParam('type', FC_TYPE_FRESHMAN);
        
        $fc = \Zend_Registry::get('fc');
        
        if ($type == FC_TYPE_FRESHMAN)
        {
            $campers = $fc->getEligibleCampers('u.lastname ASC');
        }
        else
        {
            $campers = $this->em->createQuery('SELECT u FROM \Entity\User u WHERE u.fc_app_type = :app_type ORDER BY u.lastname ASC')
                ->setParameter('app_type', $type)
                ->getArrayResult();
        }

        $base_x = 5;
        $base_y = 30;
        
        foreach($campers as $camper)
        {
            if (empty($camper['fc_initials']))
                continue;
            
            $formatted_uin = substr($camper['uin'], 0, 3).'-'.substr($camper['uin'], 3, 2).'-'.substr($camper['uin'], -4);
            
            $pdf_page = new \Zend_Pdf_Page($pdf_page_raw);
            $pdf_page->setFont($courier, 9);
            
            $pdf_page->drawText($camper['fc_initials'], 190+$base_x, 651+$base_y);
            $pdf_page->drawText(date('m/d/Y g:ia', $camper['fc_initial_timestamp']), 423+$base_x, 651+$base_y);
            
            $pdf_page->drawText($camper['firstname'].' '.$camper['lastname'], 106+$base_x, 628+$base_y);
            $pdf_page->drawText($formatted_uin, 385+$base_x, 628+$base_y);
            
            $pdf_page->drawText(date('m/d/Y', $camper['birthdate']), 120+$base_x, 605+$base_y);
            
            $pdf_page->drawText($camper['addr1'].' '.$camper['addrcity'].', '.$camper['addrstate'].' '.$camper['addrzip'], 120+$base_x, 582+$base_y);
            $pdf_page->drawText($camper['phone'], 455+$base_x, 582+$base_y);
            
            $pdf_page->drawText($camper['parentname'].' (Parent)', 30+$base_x, 470+$base_y);
            
            $pdf_page->drawText($camper['parentname'], 185+$base_x, 440+$base_y);
            $pdf_page->drawText($camper['parentphone'], 391+$base_x, 440+$base_y);
            
            $pdf_page->drawText($camper['insname'], 145+$base_x, 428+$base_y);
            $pdf_page->drawText($camper['insgrouppolicy'], 391+$base_x, 428+$base_y);
            
            // $pdf_page->drawText($camper['inspolicyholder'], 128+$base_x, 475+$base_y);
            
            $pdf_page->drawText($camper['specialaccommodations'], 125+$base_x, 388+$base_y);
            $pdf_page->drawText($camper['allergies'], 250+$base_x, 376+$base_y);
            $pdf_page->drawText($camper['medications'], 300+$base_x, 365+$base_y);
            
            $pdf_page->drawText($camper['firstname'].' '.$camper['lastname'], 185+$base_x, 290+$base_y);
            
            $pdf_page->drawText($camper['fc_initials'], 70+$base_x, 227+$base_y);
            $pdf_page->drawText(date('m/d/Y g:ia', $camper['fc_initial_timestamp']), 450+$base_x, 227+$base_y);
            
            $full_pdf->pages[] = $pdf_page;
        }
        
        unset($full_pdf->pages[0]);
        
        // Printing the results to the screen.
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", FALSE);
        
        header("Content-Disposition: attachment; filename=OnlineMedicalReleases.pdf");
        header("Content-type:application/pdf");
        
        echo $full_pdf->render();
        return;
    }
    
    /* Bus information */
    public function businfoAction()
    {
        $fc = \Zend_Registry::get('fc');
        $fc_info = $fc->getSettings();
        
        $sessions = $fc_info['sessions'];
        
        $bus_data = array();
        foreach($sessions as $session_name => $session_info)
        {
            $session_bus_data = $fc->getBusData($session_name);
            $bus_data[$session_name] = $session_bus_data;
        }
        
        $this->view->sessions = $sessions;
        $this->view->bus_info = $bus_data;
    }
    
    /* Session statistics */
    public function statsAction()
    {
        $fc = \Zend_Registry::get('fc');
        $fc_settings = $fc->getSettings();
        
        $all_campers = $fc->getEligibleCampers();
        asort($all_campers);
        
        // Assemble arrays used for camp assignment.
        $sessions = array();
        
        foreach ($fc_settings['sessions'] as $session_name => $session_info)
        {
            $sessions[$session_name] = array(
                'current'           => array(
                    'M'         => 0,
                    'F'         => 0,
                    'all'       => 0,
                ),
                'total'             => array(
                    'M'         => 0,
                    'F'         => 0,
                    'all'       => 0,
                ),
                'camps'             => array(),
                'buses'             => array(),
            );
        }
        
        // Compose cabin information.
        $raw_cabin_data = $this->em->createQuery('SELECT cabin, camp.name FROM \Entity\Cabin cabin JOIN cabin.camp camp ORDER BY camp.name ASC, cabin.name ASC')
            ->getArrayResult();
        
        foreach($raw_cabin_data as $camp_info)
        {
            $cabin_info = $camp_info[0];
            
            foreach($fc_settings['sessions'] as $session_name => $session_info)
            {
                $cabin_gender = ($cabin_info['cabin_gender_'.$session_name] == 'M') ? 'M' : 'F';
                $cabin_camp = $camp_info['name'];
                
                $sessions[$session_name]['total'][$cabin_gender] += $cabin_info['population'];
                $sessions[$session_name]['total']['all'] += $cabin_info['population'];
                
                $sessions[$session_name]['camps'][$cabin_camp]['total'][$cabin_gender] += $cabin_info['population'];
                $sessions[$session_name]['camps'][$cabin_camp]['total']['all'] += $cabin_info['population'];
                
                $sessions[$session_name]['camps'][$cabin_camp]['cabins'][$cabin_info['name']] = array(
                    'gender'            => $cabin_gender,
                    'current'           => 0,
                    'total'             => $cabin_info['population'],
                );
            }
        }
        
        // Loop through campers and increment the respective array items.
        foreach($all_campers as $camper)
        {
            $gender = ($camper['gender'] == 'M') ? 'M' : 'F';
            $session = $camper['fc_assigned_session'];
            
            if (isset($sessions[$session]))
            {
                $camp = $camper['fc_assigned_camp'];
                $cabin = $camper['fc_assigned_cabin'];
                $bus = $camper['fc_assigned_bus'];
                
                $sessions[$session]['current'][$gender]++;
                $sessions[$session]['current']['all']++;
                
                if ($camp != "WL")
                {
                    $sessions[$session]['camps'][$camp]['current'][$gender]++;
                    $sessions[$session]['camps'][$camp]['current']['all']++;
                    
                    if ($cabin != "WL")
                    {
                        $sessions[$session]['camps'][$camp]['cabins'][$cabin]['current']++;
                    }
                }
                
                if ($bus != "WL")
                {
                    $sessions[$session]['buses'][$bus]['current']++;
                    $sessions[$session]['buses'][$bus]['total'] = $fc_settings['freshmen_per_bus'];
                }
            }
        }
        
        // Perform final sorting operations.
        $overassignments = array();
        foreach($sessions as $session_name => &$session)
        {
            uksort($session['buses'], array($this, "_sortBuses"));
            
            $session['percent']['M'] = $this->_getPercent($session['current']['M'], $session['total']['M']);
            $session['percent']['F'] = $this->_getPercent($session['current']['F'], $session['total']['F']);
            $session['percent']['all'] = $this->_getPercent($session['current']['all'], $session['total']['all']);
            
            foreach($session['camps'] as $camp_name => &$camp)
            {
                $camp['color'] = $fc->getCampColor($camp_name);
                
                $camp['percent']['M'] = $this->_getPercent($camp['current']['M'], $camp['total']['M']);
                $camp['percent']['F'] = $this->_getPercent($camp['current']['F'], $camp['total']['F']);
                $camp['percent']['all'] = $this->_getPercent($camp['current']['all'], $camp['total']['all']);
                
                foreach($camp['cabins'] as $cabin_name => &$cabin)
                {
                    $cabin['percent'] = $this->_getPercent($cabin['current'], $cabin['total']);
                    
                    if ($cabin['current'] > $cabin['total'])
                    {
                        $overassignments[] = 'Session '.$session_name.' '.$camp_name.': '.$cabin_name.' ('.$cabin['current'].'/'.$cabin['total'].')';
                    }
                }
            }
            foreach($session['buses'] as &$bus)
            {
                $bus['percent'] = $this->_getPercent($bus['current'], $bus['total']);   
            }
        }
        
        $this->view->overassignments = $overassignments;
        $this->view->sessions = $sessions;
    }
    
    public function _getPercent($a, $b)
    {
        $percent = ($b != 0) ? round(($a / $b) * 100) : 0;
        return ($percent <= 100) ? $percent : 100;
    }
    
    public function _sortBuses($a, $b)
    {
        $a = intval(substr($a, 1));
        $b = intval(substr($b, 1));
        
        if((int)$a == (int)$b)return 0;
        if((int)$a  > (int)$b)return 1;
        if((int)$a  < (int)$b)return -1;
    }
}