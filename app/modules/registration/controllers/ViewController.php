<?php
use \Entity\User;
use \Entity\Ledger;
use \Entity\LedgerLog;
use \Entity\LedgerItemType;

class Registration_ViewController extends \DF\Controller\Action
{
	public function permissions()
    {
		return $this->acl->isAllowed('manage registration');
    }

    protected $_user;
	public function preDispatch()
	{
        parent::preDispatch();

		$user_id = (int)$this->_getParam('id');
		$user = User::find($user_id);
		
		if (!($user instanceof User))
			throw new \DF\Exception\DisplayOnly('User not found!');
		
		$this->_user = $user;
		$this->view->user = $user;
	}
	
    /**
     * Main display.
     */
    public function indexAction()
    {
		$form = new \FCO\Form\Registration(NULL, $this->_user, TRUE);
		$this->view->form = $form;
	}
	
	public function editAction()
	{
		$form = new \FCO\Form\Registration(NULL, $this->_user, TRUE);
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$form->save();
			
			$this->alert('<b>Registration details updated.</b>', 'green');
			$this->redirectFromHere(array('action' => 'index'));
            return;
		}
		
		$this->view->form = $form;
	}

    public function settypeAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->registration_type);
        $form->setDefaults($this->_user->toArray(TRUE, TRUE));

        if ($_POST && $form->isValid($_POST))
        {
            $data = $form->getValues();
            $this->_user->fromArray($data);
            $this->_user->save();

            $this->alert('<b>User registration type updated.</b>', 'green');
            $this->redirectFromHere(array('action' => 'index'));
            return;
        }

        $this->view->headTitle('Set Registration Type');
        $this->renderForm($form);
    }
	
	/**
	 * Ledger functions.
	 */
	public function ledgerAction()
	{
		\DF\Acl::checkPermission('view accounting');
        
        $qb = $this->em->createQueryBuilder()
            ->select('l, ml, lit, ll, lt')
            ->from('\Entity\Ledger', 'l')
            ->leftJoin('l.matching_ledger', 'ml')
            ->leftJoin('l.touchnet', 'lt')
            ->leftJoin('l.type', 'lit')
            ->leftJoin('l.logs', 'll')
            ->where('l.user_id = :user_id')
            ->setParameter('user_id', $this->_user->id);
        
        $ledger = $qb->getQuery()->getArrayResult();
        
		$running_total = 0;
		if (count($ledger) > 0)
		{
			foreach($ledger as $item)
			{
				$running_total += $item['amount'];
			}
		}
		
		$this->view->running_total = $running_total;
		$this->view->ledger = $ledger;
        
        $this->view->ledger_item_types = LedgerItemType::fetchSelect();
	}

    public function togglecancelledAction()
    {
        $is_cancelled = $this->_user->fc_is_cancelled;
        $this->_user->fc_is_cancelled = !$is_cancelled;
        $this->_user->save();

        $this->alert('The user\'s cancelled status has been toggled.', 'green');
        $this->redirectFromHere(array('action' => 'ledger', 'ledger_id' => NULL));
        return;
    }
    
    public function addledgeritemAction()
    {
        $this->acl->checkPermission('manage accounting');

        $this->redirectFromHere(array('action' => 'editledgeritem', 'type' => $item_type));
        return;
    }
    
    public function refundAction()
    {
        $this->acl->checkPermission('manage accounting');
        
        $item = LedgerItemType::getRepository()->findOneby(array('name' => 'Refund'));
        $item_type = $item->id;
        
        $matching_id = (isset($_REQUEST['matching_id'])) ? $_REQUEST['matching_id'] : NULL;
        
        $this->redirectFromHere(array('action' => 'editledgeritem', 'type' => $item_type, 'matching_id' => $matching_id));
        return;
    }
    
    public function transferAction()
    {
        \DF\Acl::checkPermission('manage accounting');
        
        $item_id = (int)$this->_getParam('ledger_id');
        $item = Ledger::find($item_id);
        
        if (!($item instanceof Ledger))
            throw new \DF\Exception\DisplayOnly('Ledger item not found!');
            
        // Find new user to transfer to.
        $current_user = \DF\Auth::getLoggedInUser();
        $existing_user = $item->user;
        $new_user = User::getOrCreate($this->_getParam('uin'));
        
        // Determine which ledger items require updating.
        $items_to_update = array($item);
        
        if ($item->matching_ledger)
            $items_to_update[] = $item->matching_ledger;
        
        foreach($items_to_update as $item_to_update)
        {
            // Switch ledger item to new user.
            $item_to_update->user = $new_user;
            $this->em->persist($item_to_update);
            
            // Switch TouchNet transaction to new user.
            if ($item_to_update->touchnet)
            {
                $touchnet = $item_to_update->touchnet;
                $touchnet->user = $new_user;
                $this->em->persist($touchnet);
            }
            
            $change_string = '<b>'.$current_user['firstname'].' '.$current_user['lastname'].'</b> transferred this ledger item from <i>'.$existing_user['firstname'].' '.$existing_user['lastname'].' ('.$existing_user['uin'].')</i> to <i>'.$new_user['firstname'].' '.$new_user['lastname'].' ('.$new_user['uin'].')</i>. The following reason was specified: '.$this->_getParam('reason');
            
            $log_entry = new LedgerLog();
            $log_entry->ledger = $item_to_update;
            $log_entry->message = $change_string;
            $this->em->persist($log_entry);
        }
        
        $this->em->flush();
        
        \FCO\Registration::updateUserPaidStatus($existing_user);
        \FCO\Registration::updateUserPaidStatus($new_user);
        
        $this->alert('The ledger item selected has been transferred to this user\'s account.');
        $this->redirectFromHere(array('action' => 'ledger', 'ledger_id' => NULL, 'id' => $new_user->id));
        return;
    }
	
	public function editledgeritemAction()
	{
		\DF\Acl::checkPermission('manage accounting');
		
        if ($this->_hasParam('item_id'))
            $record = Ledger::find($this->_getParam('item_id'));
        
        $form_config = $this->current_module_config->forms->ledgeritem->form->toArray();

        /*
        $amount_options = array();
        foreach($ledger_type->amounts as $amount)
        {
            $amount_options[$amount['amount']] = $amount['name'].': '.\DF\Utilities::money_format($amount['amount']);
        }
        $form_config['elements']['amount'][1]['multiOptions'] = $amount_options;
        
        // Special handling for refunds.
        $refund_type = LedgerItemType::getRepository()->findOneby(array('name' => 'Refund'));
        
        if ($ledger_type->id == $refund_type->id)
        {
            unset($form_config['elements']['ipayments_reference_number']);
            unset($form_config['elements']['check_number']);
            unset($form_config['elements']['check_date']);
            
            $form_config['elements']['post_date'][1]['label'] = 'Refund Date';
        }
        */
        
        // Handle form population.
        $form = new \DF\Form($form_config);
        
        if ($record instanceof Ledger)
            $form->setDefaults($record->toArray());
		
        // Handle form submission.
		if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			$current_user = $this->auth->getLoggedInUser();
			
			if (!($record instanceof Ledger))
			{
                // Create new ledger item.
				$record = new Ledger();
				$record->user = $this->_user;
				$record->submitter = $current_user->firstname.' '.$current_user->lastname;
			}
            else
            {
                // Log changes.
                $current_record = $record->toArray();
                $changes = array();
                
                foreach($data as $data_key => $new_val)
                {
                    if (strcmp($current_record[$data_key], $new_val) !== 0)
                        $changes[] = '<i>'.$data_key.'</i> to "'.$new_val.'"';
                }
                
                if ($changes)
                {
                    $change_string = '<b>'.$current_user['firstname'].' '.$current_user['lastname'].'</b> changed '.implode(', ', $changes).'.';
                    
                    $log_entry = new LedgerLog();
                    $log_entry->ledger = $record;
                    $log_entry->message = $change_string;
                    $log_entry->save();
                }
            }
            
            if ($this->_hasParam('matching_item_id'))
            {
                $matching_ledger_item = Ledger::find($this->_getParam('matching_item_id'));
                $matching_ledger_item->matching_ledger = $record;
                $this->em->persist($matching_ledger_item);
                
                $record->matching_ledger = $matching_ledger_item;
            }
			
			$record->fromArray($data);
            $this->em->persist($record);
            $this->em->flush();
			
			$this->alert('Ledger item posted.');
			$this->redirectFromHere(array('action' => 'ledger', 'item_id' => NULL));
        }
		
		$this->view->form = $form;
	}
	
	public function deleteledgeritemAction()
	{
		\DF\Acl::checkPermission('manage accounting');
		
		$item = Ledger::find($this->_getParam('item_id'));
		if ($item)
        {
            if ($item->logs->count() > 0)
            {
                foreach($item->logs as $log)
                {
                    $log->delete();
                }
            }
			$item->delete();
        }
			
		$this->alert('Record deleted.');
        $this->redirectFromHere(array('action' => 'ledger', 'item_id' => NULL));
	}
}