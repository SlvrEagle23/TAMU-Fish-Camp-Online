<?php
/**
 * Appeals Core Controller
 */

use \Entity\Camp;
use \Entity\Session;
use \Entity\User;
use \Entity\Counselect;
use \Entity\Settings;

class Counselect_IndexController extends \DF\Controller\Action
{
    protected $_is_open;
    protected $_is_results_view;

    protected $_camp;
    protected $_session;

    public function permissions()
    {
        return $this->acl->isAllowed(array('is chair', 'manage counselect'));
    }

    public function preDispatch()
    {
        $camp_name = NULL;
        $session_name = NULL;

        $this->_is_open = Settings::getSetting('counselect_unlocked', 1);
        $this->_is_results_view = Settings::getSetting('counselect_results_view', 1);

        if ($this->acl->isAllowed('manage counselect'))
        {
            $camp_name = $this->_getParam('camp', '');
            $session_name = $this->_getParam('session', '');
        }
        else
        {
            if (!$this->_is_open && !$this->_is_results_view)
                throw new \DF\Exception\DisplayOnly('Pre-Counselect has closed and is no longer accepting submissions. You will receive your list of assigned counselors from the Director Staff.');
        }

        if (!$camp_name || !$session_name)
        {
            $user = $this->auth->getLoggedInUser();
            $camp_name = $user->fc_assigned_camp;
            $session_name = $user->fc_assigned_session;
        }

        if (!$camp_name || !$session_name)
            throw new \DF\Exception\DisplayOnly('You are not currently assigned to a session or camp. You must be assigned to a session and camp before you can submit preferences for that camp. Contact a director for assistance.');

        $this->_camp = Camp::getRepository()->findOneByName($camp_name);
        $this->_session = Session::getRepository()->findOneByName($session_name);

        parent::preDispatch();
    }

    public function indexAction()
    {
    	$preferences = $this->em->createQuery('SELECT cs, u FROM Entity\Counselect cs JOIN cs.user u WHERE cs.camp = :camp AND cs.session = :session AND u.fc_app_type != :no_reg ORDER BY cs.category ASC, cs.preference ASC')
            ->setParameter('no_reg', FC_TYPE_NONE)
    		->setParameter('camp', $this->_camp)
    		->setParameter('session', $this->_session)
    		->getArrayResult();
    	
    	$entries_by_category = array();
        foreach($preferences as $preference)
        {
            $entries_by_category[$preference['category']][] = $preference;
        }

        $this->view->camp = $this->_camp;
        $this->view->session = $this->_session;

        $this->view->is_open = $this->_is_open;
        $this->view->is_results_view = $this->_is_results_view;

        $this->view->entries_by_category = $entries_by_category;
        $this->view->categories = Counselect::getCategories();
    }

    public function enterAction()
    {
        if (!$this->_is_open && !$this->acl->isAllowed('manage counselect'))
            throw new \DF\Exception\DisplayOnly('Pre-Counselect is currently locked. You cannot submit new counselor preferences.');

        $form = new \DF\Form($this->current_module_config->forms->preferences->form);

        $cat_id = $this->_getParam('category');
        $categories = Counselect::getCategories();
        $cat_name = $categories[$cat_id];

        if ($_POST && $form->isValid($_POST))
        {
            $data = $form->getValues();
            $uins_raw = $data['uins'];

            $current = $this->em->createQuery('SELECT cs FROM Entity\Counselect cs WHERE cs.camp = :camp AND cs.session = :session AND cs.category = :category')
                ->setParameter('camp', $this->_camp)
                ->setParameter('session', $this->_session)
                ->setParameter('category', $cat_id)
                ->getResult();
            
            foreach($current as $row)
            {
                $this->em->remove($row);
            }

            $pref = 1;
            $uins = explode("\n", $uins_raw);

            foreach((array)$uins as $uin)
            {
                $uin = (int)substr(trim($uin), 0, 9);
                if ($uin != 0)
                {
                    $user = User::getOrCreate($uin, FALSE);
                    $user->fc_app_type = FC_TYPE_COUNSELOR;
                    $this->em->persist($user);

                    $record = new Counselect;
                    $record->user = $user;
                    $record->camp = $this->_camp;
                    $record->session = $this->_session;
                    $record->category = $cat_id;
                    $record->preference = $pref;
                    $record->status = Counselect::STATUS_PENDING;

                    $this->em->persist($record);

                    $pref++;
                }
            }

            $this->em->flush();

            $this->alert('<b>Preferences entered for '.$cat_name.'.</b>', 'green');
            $this->redirectFromHere(array('action' => 'index', 'category' => NULL));
            return;
        }

        $this->renderForm($form, 'edit', 'Preferences: '.$cat_name);
    }
}