<?php
/**
 * Appeals Core Controller
 */

use \Entity\Camp;
use \Entity\Session;
use \Entity\User;
use \Entity\CounselorAppReview;
use \Entity\Settings;

class Counselect_ReviewController extends \DF\Controller\Action
{
    protected $_is_open;
    protected $_camp;
    protected $_session;

    public function permissions()
    {
        return $this->acl->isAllowed(array('is chair', 'manage counselect'));
    }

    public function preDispatch()
    {
        $camp_name = NULL;
        $session_name = NULL;

        $this->_is_open = Settings::getSetting('counselor_selection_unlocked', 1);

        if ($this->acl->isAllowed('manage counselect'))
        {
            $camp_name = $this->_getParam('camp', '');
            $session_name = $this->_getParam('session', '');
        }
        else
        {
            if (!$this->_is_open && !$this->_is_results_view)
                throw new \DF\Exception\DisplayOnly('Counselor Selection has closed and is no longer accepting submissions. You will receive your list of assigned counselors from the Director Staff.');
        }

        if (!$camp_name || !$session_name)
        {
            $user = $this->auth->getLoggedInUser();
            $camp_name = $user->fc_assigned_camp;
            $session_name = $user->fc_assigned_session;
        }

        if (!$camp_name || !$session_name)
            throw new \DF\Exception\DisplayOnly('You are not currently assigned to a session or camp. You must be assigned to a session and camp before you can submit preferences for that camp. Contact a director for assistance.');

        $this->_camp = Camp::getRepository()->findOneByName($camp_name);
        $this->_session = Session::getRepository()->findOneByName($session_name);

        parent::preDispatch();
    }

    public function indexAction()
    {
        $this->view->camp = $this->_camp;
        $this->view->session = $this->_session;
        $this->view->is_open = $this->_is_open;

        // Pull camp/session-specific tracking information.
        $tracking_raw = $this->em->createQuery('SELECT car FROM Entity\CounselorAppReview car WHERE car.camp = :camp AND car.session = :session')
    		->setParameter('camp', $this->_camp)
    		->setParameter('session', $this->_session)
    		->getArrayResult();

        $tracking = array();
        foreach($tracking_raw as $row)
            $tracking[$row['user_id']] = $row;

        $this->view->tracking = $tracking;

        // Pull main list of counselors.
        if ($this->_hasParam('q'))
        {
            $query = $this->_getParam('q');
            $this->view->searchterms = $q;

            if ($this->_camp->name == "Crew")
            {
                $q = $this->em->createQuery('SELECT u FROM Entity\User u WHERE (CONCAT(u.firstname, CONCAT(\' \', u.lastname)) LIKE :q_like OR u.email LIKE :q_like OR u.uin = :q_exact) AND (u.fc_app_type = :counselor) ORDER BY u.lastname ASC')
                    ->setParameter('q_like', '%'.$query.'%')
                    ->setParameter('q_exact', $query)
                    ->setParameter('counselor', FC_TYPE_COUNSELOR);
            }
            else
            {
                $q = $this->em->createQuery('SELECT u FROM Entity\User u WHERE (CONCAT(u.firstname, CONCAT(\' \', u.lastname)) LIKE :q_like OR u.email LIKE :q_like OR u.uin = :q_exact) AND (u.fc_app_type = :counselor AND u.fc_session_preference LIKE :session) ORDER BY u.lastname ASC')
                    ->setParameter('q_like', '%'.$query.'%')
                    ->setParameter('q_exact', $query)
                    ->setParameter('counselor', FC_TYPE_COUNSELOR)
                    ->setParameter('session', '%'.$this->_session->name.'%');
            }
        }
        else
        {
            if ($this->_camp->name == "Crew")
            {
                $q = $this->em->createQuery('SELECT u FROM Entity\User u WHERE (u.fc_app_type = :counselor) ORDER BY u.lastname ASC')
                    ->setParameter('counselor', FC_TYPE_COUNSELOR);
            }
            else
            {
                $q = $this->em->createQuery('SELECT u FROM Entity\User u WHERE (u.fc_app_type = :counselor AND u.fc_session_preference LIKE :session) ORDER BY u.lastname ASC')
                    ->setParameter('counselor', FC_TYPE_COUNSELOR)
                    ->setParameter('session', '%'.$this->_session->name.'%');
            }
        }

        $pager = new \DF\Paginator\Doctrine($q, $this->_getParam('page', 1));
        $this->view->pager = $pager;
    }

    public function viewAction()
    {
        $id = (int)$this->_getParam('id');
        $user = User::find($id);

        if (!($user instanceof User))
            throw new \DF\Exception\DisplayOnly('User not found!');

        $form = new \FCO\Form\Registration(NULL, $user, FALSE, TRUE);
        $this->view->form = $form;
        $this->view->record = $user;
    }

    public function togglereadAction()
    {
        $id = (int)$this->_getParam('id');
        $user = User::find($id);

        if (!($user instanceof User))
            throw new \DF\Exception\DisplayOnly('User not found!');

        $record = CounselorAppReview::getRepository()->findOneBy(array('session_id' => $this->_session->id, 'camp_id' => $this->_camp->id, 'user_id' => $user->id));

        if ($record instanceof CounselorAppReview)
        {
            $record->is_read = !$record->is_read;
            $record->save();
        }
        else
        {
            $record = new CounselorAppReview;
            $record->user = $user;
            $record->camp = $this->_camp;
            $record->session = $this->_session;
            $record->is_read = true;
            $record->save();
        }

        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
    }
}