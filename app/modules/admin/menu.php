<?php
return array(
    'default' => array(
        'admin' => array(
            'label' => 'Admin',
            'module' => 'admin',
            'show_only_with_subpages' => TRUE,
			
            'order' => -40,
			
            'pages' => array(
				'settings'	=> array(
					'label'	=> 'Site Settings',
					'module' => 'admin',
					'controller' => 'settings',
					'action' => 'index',
					'permission' => 'administer all',
				),
				
                'users' => array(
                    'label' => 'Users',
                    'module' => 'admin',
                    'controller' => 'users',
					'action' => 'index',
					'permission' => 'manage users',
				),
				'permissions' => array(
					'label' => 'Permissions',
					'module' => 'admin',
					'controller' => 'permissions',
					'permission' => 'manage permissions',
					'pages' => array(),
				), 
				
				'ledgertypes' => array(
					'label' => 'Ledger Types',
					'module' => 'admin',
					'controller' => 'ledgertypes',
					'permission' => 'manage accounting',
					'pages' => array(),
				),

				'sessions' => array(
					'label' => 'Sessions',
					'module' => 'admin',
					'controller' => 'sessions',
					'permission' => 'manage registration lookups',
					'pages' => array(
						'sessions_edit' => array(
							'module' => 'admin',
							'controller' => 'sessions',
							'action' => 'edit',
						),
					),
				),
				'camps' => array(
					'label' => 'Camps',
					'module' => 'admin',
					'controller' => 'camps',
					'permission' => 'manage registration lookups',
					'pages' => array(
						'camps_edit' => array(
							'module' => 'admin',
							'controller' => 'camps',
							'action' => 'edit',
						),
					),
				),
				'cabins' => array(
					'label' => 'Cabins',
					'module' => 'admin',
					'controller' => 'cabins',
					'permission' => 'manage registration lookups',
					'pages' => array(
						'cabins_edit' => array(
							'module' => 'admin',
							'controller' => 'cabins',
							'action' => 'edit',
						),
					),
				),

            ),
        ),
    ),
);