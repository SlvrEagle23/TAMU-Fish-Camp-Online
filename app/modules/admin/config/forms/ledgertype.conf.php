<?php
/**
 * Ledger Item Type Form
 */
 
$config = Zend_Registry::get('config');
$item_categories = $config->fishcamp->ledger_categories->toArray();
 
return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
			
			'name'		=> array('text', array(
				'label' => 'Item Name',
				'required' => TRUE,
				'class'	=> 'full-width',
			)),
			
			'category'	=> array('select', array(
				'label'	=> 'Item Category',
				'multiOptions' => array_combine($item_categories, $item_categories),
			)),
            
            'budget'	=> array('text', array(
				'label'	=> 'Total Budget',
				'filters' => array('Float'),
				'description' => 'Do not include dollar signs. To remove money from a user, enter a negative amount.',
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);