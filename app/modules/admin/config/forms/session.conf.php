<?php
/**
 * Session Form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
					
			'name'		=> array('text', array(
				'label' => 'Session Letter',
				'required' => true,
	        )),
			
			'startdate' => array('unixdate', array(
				'label' => 'Start Date',
				'required' => true,
			)),
			
			'enddate' 	=> array('unixdate', array(
				'label' => 'End Date',
				'required' => true,
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);