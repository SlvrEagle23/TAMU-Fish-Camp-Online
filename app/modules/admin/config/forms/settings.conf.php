<?php
/**
 * Settings form.
 */

$config = Zend_Registry::get('config');

$registration_types = $config->fishcamp->registration_types->toArray();
$override_options = array('' => 'No Override') + $config->fishcamp->status_codes->toArray();

$form_config = array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
        
        'groups'        => array(
		
			'fishcamp_system'	=> array(
				'legend' => 'Fish Camp System Settings',
				'elements' => array(
				
					'camp_year' => array('text', array(
						'label' => 'Camp Year',
						'filters' => array('Float'),
					)),	

					'upay_service_charge' => array('text', array(
						'label' => 'TouchNet Service Charge',
						'filters' => array('Float'),
					)),
					'payment_enabled' => array('radio', array(
                    	'label' => 'Payment Enabled',
                    	'multiOptions' => array(0 => 'No', 1 => 'Yes'),
	                )),

					'counselect_unlocked' => array('radio', array(
                    	'label' => 'Counselect Preference Entry Unlocked',
                    	'multiOptions' => array(0 => 'No', 1 => 'Yes'),
	                )),
	                'counselect_results_view' => array('radio', array(
                    	'label' => 'Counselect Results-Only View Unlocked',
                    	'multiOptions' => array(0 => 'No', 1 => 'Yes'),
	                )),
	                'counselor_selection_unlocked' => array('radio', array(
                    	'label' => 'Counselor Phase I Selection Unlocked',
                    	'multiOptions' => array(0 => 'No', 1 => 'Yes'),
	                )),

	                'auto_assignment' => array('radio', array(
						'label' => 'Run Hourly Auto-Assignment for Sessions, Camps and Cabins',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					)),
					'auto_assignment_buses' => array('radio', array(
						'label' => 'Run Hourly Auto-Assignment for Buses',
						'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					)),

	            ),
	        ),

	        'agreements' => array(
	        	'legend' => 'Agreements and Waivers',
	        	'elements' => array(

					'policy_agreement'		=> array('textarea', array(
						'label'	=> 'Policy Text',
						'class'	=> 'full-width full-height',
					)),
					'behavior_agreement'	=> array('textarea', array(
						'label' => 'Behavior Agreement Text',
						'class' => 'full-width full-height',
					)),
					'liability_waiver'		=> array('textarea', array(
						'label' => 'Liability Waiver Text',
						'class'	=> 'full-width full-height',
					)),
					'photo_release' => array('textarea', array(
						'label' => 'Photo Release Text',
						'class' => 'full-width full-height',
					)),

				),
			),

			'fishcamp_registration' => array(
				'legend' => 'Registration Deadlines',
				'elements' => array(
					
					'medical_release_deadline' => array('unixdate', array(
						'label' => 'Medical Release Deadline',
					)),
					
                    'session_change_deadline' => array('unixdate', array(
                        'label' => 'Freshman Session Change Deadline',
                    )),
					
					'session_notification_date' => array('unixdate', array(
						'label' => 'Freshman Session Notification Date',
					)),

					'payment_due_date' => array('unixdate', array(
						'label' => 'Freshman Payment Final Due Date',
					)),
                    
                    'refund_close_date' => array('unixdate', array(
                        'label' => 'Refund/Cancellation Request Form Close Date',
                    )),

                    'altpayment_close_date' => array('unixdate', array(
                        'label' => 'Alternate Payment (Scholarship/Installment) Form Close Date',
                    )),
				
				),
			),
		),
	),
);

$registration_elements = array();
foreach($registration_types as $id => $name)
{
	$group = array(
		'legend' => $name,
		'elements' => array(

			'registration_'.$id.'_open' => array('unixdate', array(
				'label' => 'Open Date',
			)),
				
			'registration_'.$id.'_close' => array('unixdate', array(
				'label' => 'Close Date',
			)),

			'registration_'.$id.'_override' => array('select', array(
				'label' => 'Override Status',
				'multiOptions' => $override_options,
			)),

			'registration_'.$id.'_due' => array('text', array(
				'label' => 'Amount Due (Payment in Full)',
				'filters' => array('Float'),
				'description' => 'Leave blank for registration types where no payment is required.',
			)),

			'registration_'.$id.'_installment' => array('text', array(
				'label' => 'Amount Due (Installment Plan)',
				'filters' => array('Float'),
				'description' => 'Leave blank for registration types where no payment is required.',
			)),

		),
	);
	$form_config['form']['groups']['elements_'.$id] = $group;
}

$form_config['form']['groups']['submit'] = array(
    'legend' => '',
    'elements' => array(
        'submit'		=> array('submit', array(
            'type'	=> 'submit',
            'label'	=> 'Save Changes',
            'helper' => 'formButton',
            'class' => 'ui-button',
        )),
    ),
);

return $form_config;