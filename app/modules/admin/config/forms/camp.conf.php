<?php
return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
					
			'name' => array('text', array(
				'label' => 'Camp Name',
				'class' => 'full-width',
				'required' => true,
	        )),

	        'color' => array('text', array(
	        	'label' => 'Hex Color in Displays',
	        )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);