<?php
use \Entity\LedgerItemType;
use \Entity\LedgerItemTypeAmount;

class Admin_LedgertypesController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('manage accounting');
    }
    
    public function indexAction()
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('lit, lita')
            ->from('\Entity\LedgerItemType','lit')
            ->leftJoin('lit.amounts','lita');
            
        $ledger_types_raw = $qb->getQuery()->getArrayResult();
		
		$ledger_types = array();
		
		$ledger_categories = $this->config->fishcamp->ledger_categories->toArray();
		foreach($ledger_categories as $category_name)
		{
			$ledger_types[$category_name] = array();
		}
		
		foreach($ledger_types_raw as $ledger_type)
		{
			$category = $ledger_type['category'];
			$ledger_types[$category][] = $ledger_type;
		}
		
		$this->view->ledger_types = $ledger_types;
    }
    
    public function amountsAction()
    {
        $item_id = (int)$this->_getParam('id');
        $lit = LedgerItemType::find($item_id);
        
        $this->view->lit = $lit;
    }
    public function addamountAction()
    {
        $item_id = (int)$this->_getParam('id');
        $lit = LedgerItemType::find($item_id);
        
        if (isset($_REQUEST['amount']))
        {
            $lit_amount = new LedgerItemTypeAmount();
            $lit_amount->ledger_item_type = $lit;
            $lit_amount->name = $_REQUEST['name'];
            $lit_amount->amount = floatval($_REQUEST['amount']);
            $lit_amount->save();
        }
        
        $this->redirectFromHere(array('action' => 'amounts'));
        return;
    }
    public function deleteamountAction()
    {
        $item_id = (int)$this->_getParam('id');
        $amount_id = (int)$this->_getParam('amount_id');
        
        $lit_amount = LedgerItemTypeAmount::find($amount_id);
        $lit_amount->delete();
        
        $this->redirectFromHere(array('action' => 'amounts'));
        return;
    }
	
	public function editAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->ledgertype->form);
		
		if ($this->_hasParam('id'))
		{
			$record = LedgerItemType::find($this->_getParam('id'));
			$form->setDefaults($record->toArray());
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof LedgerItemType))
				$record = new LedgerItemType();
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Record updated.');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
        }

        $this->view->form = $form;
	}
	
	public function deleteAction()
	{
        $this->validateToken($this->_getParam('csrf'));
		
        $action = LedgerItemType::find($this->_getParam('id'));
		if ($action)
			$action->delete();
			
		$this->alert('Record deleted.');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}