<?php
use \Entity\Settings;

class Admin_SettingsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('administer all');
    }
    
	public function indexAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->settings->form);
        
        $existing_settings = Settings::fetchAll();
        $form->setDefaults($existing_settings);
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
            $data = $form->getValues();
			
            foreach($data as $key => $value)
            {
                Settings::setSetting($key, $value);
            }

            Settings::fetchCache(TRUE);
            
			$this->alert('Settings updated!');
			$this->redirectHere();
		}
		
		$this->view->form = $form;
	}
}