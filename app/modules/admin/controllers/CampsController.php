<?php
use \Entity\Camp;

class Admin_CampsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::isAllowed('manage registration lookups');
    }
    
    public function indexAction()
    {
		$this->view->camps = Camp::fetchArray('name');
    }
	
	public function editAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->camp->form);
		
		if ($this->_hasParam('id'))
		{
			$id = (int)$this->_getParam('id');
			$record = Camp::find($id);

			$form->setDefaults($record->toArray());
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof Camp))
				$record = new Camp;
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Record updated.', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Camp');
        $this->renderForm($form);
	}
	
	public function deleteAction()
	{
		$record = Camp::find($this->_getParam('id'));
		if ($record instanceof Camp)
			$record->delete();
			
		$this->alert('Record deleted.', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}