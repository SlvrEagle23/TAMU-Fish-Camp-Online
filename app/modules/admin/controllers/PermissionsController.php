<?php
use \Entity\Action;
use \Entity\Role;

class Admin_PermissionsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('administer all');
    }
    
    public function indexAction()
    {
        $this->view->actions = Action::fetchArray('name');
        $this->view->roles = Role::fetchArray('name');
    }
	
	public function editactionAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->action->form);
		
		if ($this->_hasParam('id'))
		{
            $record = Action::find($this->_getParam('id'));
			$form->setDefaults($record->toArray());
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof Action))
				$record = new Action();
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('<b>Action changes saved.</b>', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Action');
        $this->renderForm($form);
	}
	
	public function deleteactionAction()
	{
        $this->validateToken($this->_getParam('csrf'));
        
        $action = Action::find($this->_getParam('id'));
		if ($action)
			$action->delete();
			
		$this->alert('Action deleted!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}

    public function editroleAction()
    {
		$form_config = $this->current_module_config->forms->role->form->toArray();
        $form = new \DF\Form($form_config);
		
		if ($this->_hasParam('id'))
		{
            $record = Role::find($this->_getParam('id'));
			$form_defaults = $record->toArray();
            $form_defaults['actions'] = array();
			
			foreach($form_config['elements']['actions'][1]['multiOptions'] as $action_id => $action_name)
			{
				if ($this->acl->roleAllowed($record->id, $action_name, TRUE))
					$form_defaults['actions'][] = $action_id;
			}
			
			$form->setDefaults($form_defaults);
		}

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
			
			if (!($record instanceof Role))
				$record = new Role();
                
            $actions = $data['actions'];
            unset($data['actions']);

			$record->fromArray($data);
            $record->actions->clear();
			
			// Add new actions.
			foreach($actions as $action_id)
			{
                $action = Action::find($action_id);
                $record->actions->add($action);
            }
            
            $record->save();

            $this->alert('<b>Role changes saved.</b>', 'green');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
			return;
        }

        $this->view->headTitle('Add/Edit Role');
        $this->renderForm($form);
    }

    public function deleteroleAction()
    {
        $this->validateToken($this->_getParam('csrf'));
		
        $role = Role::find($this->_getParam('id'));
		
		if ($record)
            $record->delete();
		
		$this->alert("Role deleted!");
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
    }
}