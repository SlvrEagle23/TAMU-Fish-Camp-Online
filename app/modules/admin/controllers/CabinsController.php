<?php
use \Entity\Cabin;
use \Entity\Session;

class Admin_CabinsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::isAllowed('manage registration lookups');
    }
    
    public function indexAction()
    {
    	$this->view->cabins = $this->em->createQuery('SELECT cb, ca FROM Entity\Cabin cb INNER JOIN cb.camp ca ORDER BY ca.name ASC, cb.name ASC')->getArrayResult();
    	$this->view->sessions = Session::fetchSelect();
    }
	
	public function editAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->cabin->form);
		
		if ($this->_hasParam('id'))
		{
			$id = (int)$this->_getParam('id');
			$record = Cabin::find($id);

			$form->setDefaults($record->toArray(TRUE, TRUE));
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof Cabin))
				$record = new Cabin;
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Record updated.', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Cabin');
        $this->renderForm($form);
	}
	
	public function deleteAction()
	{
		$record = Cabin::find($this->_getParam('id'));
		if ($record instanceof Cabin)
			$record->delete();
			
		$this->alert('Record deleted.', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}