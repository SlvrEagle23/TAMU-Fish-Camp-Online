<?php
/**
 * Add/Edit Event Form
 */

return array(
	'form' => array(
		'method'		=> 'post',
        
		'elements'		=> array(
			
            'type_id' => array('select', array(
                'label' => 'Event Type',
                'required' => true,
                'multiOptions' => \Entity\EventType::fetchSelect(),
            )),
            
            'subtype' => array('text', array(
				'label' => 'Event Sub-Type (Optional)',
				'class'	=> 'full-width',
	        )),
            
            'location' => array('text', array(
                'label' => 'Event Location',
                'class' => 'full-width',
            )),
            
            'description' => array('textarea', array(
                'label' => 'Event Description',
                'class' => 'full-width half-height',
            )),
            
            'starttime' => array('unixdatetime', array(
                'label' => 'Start Date/Time',
                'required' => true,
            )),
            
            'endtime' => array('unixdatetime', array(
                'label' => 'End Date/Time',
                'required' => true,
            )),
            
            'canattend' => array('radio', array(
                'label' => 'Open for Attendance',
                'multiOptions' => array(0 => 'No', 1 => 'Yes'),
            )),
            
            'capacity' => array('text', array(
                'label' => 'Total Capacity',
                'description' => 'Enter 0 for infinite capacity.',
                'required' => true,
                'filters' => array('Digits'),
            )),
			
			'submit' => array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);