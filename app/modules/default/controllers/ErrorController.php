<?php
class ErrorController extends \DF\Controller\Action
{
    public function errorAction()
    {
        // Grab the error object from the request
        $errors = $this->_getParam('error_handler');
        
        // 404 error -- controller or action not found
        if (in_array($errors->type, array(\Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER, \Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION)))
        {
			$this->_helper->viewRenderer('error/pagenotfound', null, true);
			
			$this->getResponse()->setHttpResponseCode(404);
			$this->view->message = 'Page not found';
		}
		else if ($errors->exception instanceof \DF\Exception\DisplayOnly)
		{
			$this->_helper->viewRenderer('error/displayonly', NULL, TRUE);
		}
		else if ($errors->exception instanceof \DF\Exception\NotLoggedIn)
		{
			$this->_helper->viewRenderer('error/notloggedin', NULL, TRUE);
			$this->view->message = 'Login Required to Access This Page';
		}
		else if ($errors->exception instanceof \DF\Exception\PermissionDenied)
		{
			$this->_helper->viewRenderer('error/accessdenied', NULL, TRUE);
			$this->view->message = 'Access Denied';
		}
		else
		{
			// Application Error
			$this->getResponse()->setHttpResponseCode(500);
			$this->view->message = 'Application error';
			
			// Show user-friendly error message and report back to central API.
			if (DF_APPLICATION_ENV == "production")
			{
				$this->_helper->viewRenderer('error/error', null, true);
				
				// Log error in central database.
				if ($errors->exception)
				{
					$user = $this->auth->getLoggedInUser();
					$user_info = ($user instanceof \Entity\User) ? $user->firstname.' '.$user->lastname.' ('.$user->email.')' : NULL;

					$api = new \DF\Service\DoitApi;
					$api->logException($errors->exception, $user_info);
				}
			}
			else
			{
				$this->_helper->viewRenderer('error/dev', null, true);
			}
		}
		
        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;
    }
}