<?php
use \DF\Auth;

use \Entity\User;

class AccountController extends \DF\Controller\Action
{
	public function indexAction()
	{
		$this->redirectHome();
		return;
	}
	
    public function loginAction()
    {
		if (!isset($_REQUEST['ticket']))
		{
			$this->storeReferrer('login');
		}
		
		try
		{
			$this->auth->authenticate();
		}
		catch(Exception $e)
		{
			$this->alert($e->getMessage());
			$this->redirectToRoute(array('module' => 'default'));
		}
		
		$this->flash('<b>Logged in!</b><br>Be sure to log out after your session is completed.', \DF\Flash::SUCCESS);
		
		$this->redirectToStoredReferrer('login');
		$this->doNotRender();
		return;
    }

    public function logoutAction()
    {
		if ($this->auth->isLoggedIn())
		{
			$this->auth->logout();
			session_unset();
		}

        $this->redirectToRoute(array('module' => 'default'));
    }

    public function profileAction()
    {
    	$this->acl->checkPermission('is logged in');
    	$this->alert('<b>Note: Editing this page does not affect your registration status.</b><br>If you have not already registered for Fish Camp, click the "Register Now" link at the top of the page to register. This page will allow you to update your basic contact information at any time after your registration has been submitted.', 'blue');

    	$user = $this->auth->getLoggedInUser();

    	$form = new \FCO\Form\Registration;
    	$form->removeSubForm('registration');

    	if ($_POST && $form->isValid($_POST))
    	{
    		$data = $form->getValues();
    		$user->fromArray($data);
    		$user->save();

    		$this->alert('<b>Profile updated!</b>', 'green');
    		$this->redirectHome();
    		return;
    	}

    	$this->view->headTitle('Edit Profile');
    	$this->renderForm($form);
    }

    public function masqueradeAction()
    {
        $this->acl->checkPermission('administer all');

        $uin = (int)$this->_getParam('uin');
        $user = User::getOrCreate($uin);

        $this->auth->masqueradeAsUser($user);

        $this->redirectHome();
        return;
    }

    public function endmasqueradeAction()
    {
        if ($this->auth->isMasqueraded())
            $this->auth->endMasquerade();

        $this->redirectHome();
        return;
    }
}