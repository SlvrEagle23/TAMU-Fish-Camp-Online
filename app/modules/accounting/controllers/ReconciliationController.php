<?php
use \Entity\LedgerItemType;
use \Entity\LedgerItemTypeAmount;
use \Entity\Touchnet;

class Accounting_ReconciliationController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::isAllowed('view accounting');
    }
	
    /**
     * Main display.
     */
	public function indexAction()
	{
		if (!$this->_hasParam('start_date'))
		{
			$this->render('select');
            return;
		}
		
		$start_date_raw = $this->_getParam('start_date');
		$start_date = ($start_date_raw) ? \DF\Form\Element\UnixDate::processArray($start_date_raw) : mktime(0, 0, 0, 1, 1);
		
		$end_date_raw = $this->_getParam('end_date');
		$end_date = ($end_date_raw) ? \DF\Form\Element\UnixDate::processArray($end_date_raw) : time();
        $end_date = strtotime(date('m/d/Y', $end_date).' 23:59:59');
		
		$this->view->start_date = $start_date;
		$this->view->end_date = $end_date;
		
        $payment_methods = $this->config->fishcamp->payment_methods->toArray();
		$this->view->payment_methods = $payment_methods;
		
		$total_zeroes = array();
		foreach($payment_methods as $method_name => $method_title)
		{
			$total_zeroes[$method_name] = 0;
		}
		$total_zeroes['total'] = 0;
		
		// Get ledger types.
		$ledger_types = LedgerItemType::fetchAll();
        
		$ledger_totals = array();
        
		foreach($ledger_types as $type)
		{
            if ($type['budget'])
            {
                $current_usage = 0;
                
                $qb = $this->em->createQueryBuilder()
                    ->select('l.amount')
                    ->from('\Entity\Ledger', 'l')
                    ->where('l.ledger_item_type_id = :id')
                    ->andWhere('l.matching_ledger_id IS NULL')
                    ->setParameter('id', $type['id']);
                
                $transactions = $qb->getQuery()->getArrayResult();
                
                foreach((array)$transactions as $transaction)
                {
                    $current_usage += $transaction['amount'];
                }
            }
            
			$ledger_totals[$type['id']] = array(
				'name'		=> $type['name'],
                'budget'    => $type['budget'],
                'remaining' => $type['budget'] - $current_usage,
				'category'	=> $type['category'],
				'totals'	=> $total_zeroes,
			);
		}
		
		// Get all items for all transactions.
        $q = $this->em->createQuery('SELECT l, lit FROM \Entity\Ledger l LEFT JOIN l.type lit WHERE (l.post_date >= :start AND l.post_date <= :end) ORDER BY l.created_at ASC')->setParameters(array('start' => $start_date, 'end' => $end_date));
        
        $ledger_items = $q->getArrayResult();
		
		foreach($ledger_items as $item)
		{
			$item_type_id = $item['ledger_item_type_id'];
			$payment_method = $item['payment_method'];
			$amount = $item['amount'];
			
			$ledger_totals[$item_type_id]['totals'][$payment_method] += $amount;
			$ledger_totals[$item_type_id]['totals']['total'] += $amount;
		}
		
		$report = array();
		$ledger_categories = $this->config->fishcamp->ledger_categories->toArray();
		
		foreach($ledger_categories as $category_name)
		{
			$report[$category_name] = array();
		}
		
		foreach($ledger_totals as $ledger_item_id => $ledger_total_row)
		{
			$category = $ledger_total_row['category'];
			$report[$category][$ledger_item_id] = $ledger_total_row;
		}
		
		$this->view->report = $report;
	}
    
    // Auto-reconciliation script.
    public function autoAction()
    {
        \DF\Acl::checkPermission('manage accounting');
        
        if (!isset($_FILES['touchnet']['name']))
        {
            $this->render('auto_upload');
            return;
        }
        else
        {
            set_time_limit(6000);
            
            $db_info = $this->current_module_config->application->resources->doctrine->conn->toArray();
            $db_info['username'] = $db_info['user'];
            unset($db_info['driver'], $db_info['user']);
            
            $db_info['dbname'] = 'fishcamp_temp';
            $old_db = \Zend_Db::factory('Pdo_Mysql', $db_info);
            
            // Upload, process, and delete file.
            $touchnet_temp_path = 'touchnet_temp.csv';
            $touchnet_short_path = \DF\File::moveUploadedFile($_FILES['touchnet'], $touchnet_temp_path);
            $csv_data = \DF\File::getCleanCsv($touchnet_short_path);
            \DF\File::deleteFile($touchnet_short_path);
            
            $transaction_ids = array();
            
            $results = array();
            
            // Loop through transactions in file.
            foreach($csv_data as $trans)
            {
                $local_data = array(
                    'time_created'      => strtotime($trans['paymentdate']),
                    'time_updated'      => strtotime($trans['paymentdate']),
                    'payment_order_id'  => $trans['systemtrackingid'],
                    'transaction_id'    => (string)$trans['externaltransid'],
                    'payment_card_type' => $trans['paymentmethod'],
                    'payment_name'      => $trans['name'],
                    'payment_internal_trans_id' => $trans['tpgreferencenumber'],
                    'payment_amount'    => $trans['paymentamount'],
                    'payment_status'    => 'success',
                );
                
                $transactions[$local_data['transaction_id']] = $local_data;
                $transaction_ids[] = $local_data['transaction_id'];
            }
            
            $results['num_transactions'] = count($transaction_ids);
            
            $dbtq = $this->em->createQuery('SELECT t, l FROM Entity\Touchnet t JOIN t.ledgers l WHERE t.transaction_id IN (:ids) GROUP BY t.id');
            $dbtq->setParameter('ids', $transaction_ids);
            $db_transactions = $dbtq->getArrayResult();
            
            foreach($db_transactions as $db_trans)
            {
                unset($transactions[$db_trans['transaction_id']]);
                
                if (count($db_trans['ledgers']) == 0)
                {
                    $results['missing_ledgers']++;
                }
                else
                {
                    $results['correct']++;
                }
            }
            
            \DF\Utilities::print_r($results);
            exit;
            
            $this->view->results = $results;
            
            $this->render('auto_results');
            return;
        }
    }
}