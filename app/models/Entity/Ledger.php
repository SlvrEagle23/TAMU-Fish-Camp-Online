<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="ledger")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Ledger extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->logs = new ArrayCollection;
        $this->is_posted_to_famis = 0;
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
    /** @PreUpdate */
    public function _updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /**
     * @PostPersist
     * @PostUpdate
     */
    public function _updateUserTotal()
    {
		\FCO\Registration::updateUserPaidStatus($this->user);
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /** @Column(name="ledger_item_type_id", type="integer") */
    protected $ledger_item_type_id;
    
    /**
     * @ManyToOne(targetEntity="LedgerItemType")
     * @JoinColumn(name="ledger_item_type_id", referencedColumnName="id")
     */
    protected $type;

    /** @Column(name="amount", type="decimal", scale=2, nullable=true) */
    protected $amount;

    /** @Column(name="touchnet_id", type="integer", nullable=true) */
    protected $touchnet_id;
    
    /**
     * @ManyToOne(targetEntity="Touchnet", inversedBy="ledgers")
     * @JoinColumn(name="touchnet_id", referencedColumnName="id")
     */
    protected $touchnet;
    
    /** @Column(name="matching_ledger_id", type="integer", nullable=true) */
    protected $matching_ledger_id;
    
    /**
     * @OneToOne(targetEntity="Ledger")
     * @JoinColumn(name="matching_ledger_id", referencedColumnName="id")
     */
    protected $matching_ledger;

    /** @Column(name="submitter", type="string", length=255, nullable=true) */
    protected $submitter;

    /** @Column(name="payment_method", type="string", length=255, nullable=true) */
    protected $payment_method;

    /** @Column(name="funding_source", type="string", length=255, nullable=true) */
    protected $funding_source;
    
    /** @Column(name="ipayments_reference_number", type="string", length=255, nullable=true) */
    protected $ipayments_reference_number;

    /** @Column(name="check_number", type="string", length=255, nullable=true) */
    protected $check_number;

    /** @Column(name="check_date", type="integer", nullable=true) */
    protected $check_date;

    /** @Column(name="post_date", type="integer", nullable=true) */
    protected $post_date;

    /** @Column(name="is_posted_to_famis", type="integer", length=1) */
    protected $is_posted_to_famis;

    /** @Column(name="notes", type="string", nullable=true) */
    protected $notes;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
    
    /**
     * @OneToMany(targetEntity="Entity\LedgerLog", mappedBy="ledger")
     */
    protected $logs;
}