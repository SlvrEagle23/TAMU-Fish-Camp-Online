<?php

namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="user", indexes={
 *   @index(name="account_idx", columns={"uin","username"}),
 *   @index(name="search_idx", columns={"firstname","lastname","email"}),
 *   @index(name="sort_idx", columns={"fc_app_type","fc_app_completed","fc_received_papers","fc_received_payment"})
 * })
 * @Entity
 */
class User extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->roles = new ArrayCollection;
        $this->touchnet = new ArrayCollection;
        $this->appeals = new ArrayCollection;
        $this->alt_payment_requests = new ArrayCollection;
        $this->refund_requests = new ArrayCollection;

        $this->fc_is_cancelled = 0;
        $this->fc_app_completed = 0;
        $this->fc_sessions_selected = 0;
        $this->fc_app_last_modified = 0;
        $this->fc_received_papers = 0;
        $this->fc_received_payment = 0;
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="uin", type="string", length=20) */
    protected $uin;

    /** @Column(name="username", type="string", length=255) */
    protected $username;

    /** @Column(name="password", type="text", nullable=true) */
    protected $password;

    /** @Column(name="auth_key", type="string", length=64, nullable=true) */
    protected $auth_key;

    /** @Column(name="firstname", type="string", length=255, nullable=true) */
    protected $firstname;

    /** @Column(name="lastname", type="string", length=255, nullable=true) */
    protected $lastname;

    public function getName()
    {
        return $this->firstname.' '.$this->lastname;
    }

    /** @Column(name="title", type="string", length=255, nullable=true) */
    protected $title;

    /** @Column(name="email", type="string", length=255, nullable=true) */
    protected $email;

    /** @Column(name="email2", type="string", length=255, nullable=true) */
    protected $email2;

    /** @Column(name="phone", type="string", length=20, nullable=true) */
    protected $phone;

    /** @Column(name="addr1", type="string", length=255, nullable=true) */
    protected $addr1;

    /** @Column(name="addrcity", type="string", length=255, nullable=true) */
    protected $addrcity;

    /** @Column(name="addrstate", type="string", length=10, nullable=true) */
    protected $addrstate;

    /** @Column(name="addrzip", type="string", length=10, nullable=true) */
    protected $addrzip;

    /** @Column(name="gender", type="string", length=2, nullable=true) */
    protected $gender;

    /** @Column(name="race", type="string", length=2, nullable=true) */
    protected $race;

    /** @Column(name="college", type="string", length=2, nullable=true) */
    protected $college;

    /** @Column(name="parentname", type="string", length=255, nullable=true) */
    protected $parentname;

    /** @Column(name="parentphone", type="string", length=255, nullable=true) */
    protected $parentphone;

    /** @Column(name="parentemail", type="string", length=255, nullable=true) */
    protected $parentemail;

    /** @Column(name="parentaddr", type="text", nullable=true) */
    protected $parentaddr;

    /** @Column(name="alt1name", type="string", length=255, nullable=true) */
    protected $alt1name;

    /** @Column(name="alt1relationship", type="string", length=255, nullable=true) */
    protected $alt1relationship;

    /** @Column(name="alt1phone", type="string", length=255, nullable=true) */
    protected $alt1phone;

    /** @Column(name="alt1email", type="string", length=255, nullable=true) */
    protected $alt1email;

    /** @Column(name="alt1addr", type="text", nullable=true) */
    protected $alt1addr;

    /** @Column(name="alt2name", type="string", length=255, nullable=true) */
    protected $alt2name;

    /** @Column(name="alt2relationship", type="string", length=255, nullable=true) */
    protected $alt2relationship;

    /** @Column(name="alt2phone", type="string", length=255, nullable=true) */
    protected $alt2phone;

    /** @Column(name="alt2email", type="string", length=255, nullable=true) */
    protected $alt2email;

    /** @Column(name="alt2addr", type="text", nullable=true) */
    protected $alt2addr;

    /** @Column(name="insname", type="string", length=255, nullable=true) */
    protected $insname;

    /** @Column(name="insgrouppolicy", type="string", length=255, nullable=true) */
    protected $insgrouppolicy;

    /** @Column(name="inspolicyholder", type="string", length=255, nullable=true) */
    protected $inspolicyholder;

    /** @Column(name="inspolicyssn", type="string", length=255, nullable=true) */
    protected $inspolicyssn;

    /** @Column(name="allergies", type="text", nullable=true) */
    protected $allergies;

    /** @Column(name="medications", type="text", nullable=true) */
    protected $medications;

    /** @Column(name="othermedical", type="text", nullable=true) */
    protected $othermedical;

    /** @Column(name="specialaccommodations", type="text", nullable=true) */
    protected $specialaccommodations;

    /** @Column(name="specialaccommodation_notes", type="text", nullable=true) */
    protected $specialaccommodation_notes;

    /** @Column(name="birthdate", type="integer", nullable=true) */
    protected $birthdate;

    /** @Column(name="tetanus", type="integer", nullable=true) */
    protected $tetanus;

    /** @Column(name="shirt_size", type="string", length=3, nullable=true)*/
    protected $shirt_size;

    /** @Column(name="fc_app_type", type="smallint", nullable=true) */
    protected $fc_app_type;

    public function setFcAppType($type)
    {
        // Trigger a reset of the user's status if changing registration type.
        if ($type != $this->fc_app_type)
        {
            $this->fc_is_cancelled = 0;
            $this->fc_app_completed = 0;
            $this->fc_sessions_selected = 0;
            $this->fc_app_last_modified = 0;
            $this->fc_received_papers = 0;
            $this->fc_received_payment = 0;
        }

        $this->fc_app_type = $type;
    }

    /** @Column(name="fc_is_cancelled", type="boolean", nullable=true) */
    protected $fc_is_cancelled;

    /** @Column(name="fc_app_last_modified", type="integer", nullable=true) */
    protected $fc_app_last_modified;

    /** @Column(name="fc_app_completed", type="integer", nullable=true) */
    protected $fc_app_completed;

    /** @Column(name="fc_sessions_selected", type="integer", nullable=true) */
    protected $fc_sessions_selected;

    /** @Column(name="fc_org_affiliation", type="text", nullable=true) */
    protected $fc_org_affiliation;

    /** @Column(name="fc_is_mailin", type="boolean", nullable=true) */
    protected $fc_is_mailin;

    /** @Column(name="fc_session_preference", type="string", length=15, nullable=true) */
    protected $fc_session_preference;

    /** @Column(name="fc_initials", type="string", length=5, nullable=true) */
    protected $fc_initials;

    /** @Column(name="fc_initial_timestamp", type="integer", nullable=true) */
    protected $fc_initial_timestamp;

    /** @Column(name="fc_assignment_override", type="integer", length=1, nullable=true) */
    protected $fc_assignment_override;

    /** @Column(name="fc_assigned_session", type="string", length=20, nullable=true) */
    protected $fc_assigned_session;

    /** @Column(name="fc_assigned_camp", type="string", length=20, nullable=true) */
    protected $fc_assigned_camp;

    /** @Column(name="fc_assigned_cabin", type="string", length=20, nullable=true) */
    protected $fc_assigned_cabin;

    /** @Column(name="fc_assigned_bus", type="string", length=20, nullable=true) */
    protected $fc_assigned_bus;

    /** @Column(name="fc_received_papers", type="integer", length=1, nullable=true) */
    protected $fc_received_papers;

    /** @Column(name="fc_received_payment", type="integer", length=1, nullable=true) */
    protected $fc_received_payment;

    /** @Column(name="fc_payment_type", type="integer", length=1, nullable=true) */
    protected $fc_payment_type;

    /** @Column(name="fc_payment_amount", type="float", length=10, nullable=true) */
    protected $fc_payment_amount;

    /** @Column(name="fc_refund_amount", type="float", nullable=true) */
    protected $fc_refund_amount;

    /** @Column(name="fc_scholarship_amount", type="float", nullable=true) */
    protected $fc_scholarship_amount;

    /** @Column(name="fc_altpayment_option", type="string", length=50, nullable=true) */
    protected $fc_altpayment_option;

    /** @Column(name="fc_altpayment_circumstances", type="text", nullable=true) */
    protected $fc_altpayment_circumstances;

    /** @Column(name="fc_notification_sent", type="integer", nullable=true) */
    protected $fc_notification_sent;

    /** @Column(name="fc_altpayment_amount", type="text", nullable=true) */
    protected $fc_altpayment_amount;

    /** @Column(name="fc_audit_log", type="json", nullable=true) */
    protected $fc_audit_log;

    /** @Column(name="fc_counselor_application", type="json", nullable=true) */
    protected $fc_counselor_application;

    /** @Column(name="admin_deposit_slip_number", type="string", length=255, nullable=true) */
    protected $admin_deposit_slip_number;

    /** @Column(name="admin_ipayment_number", type="string", length=255, nullable=true) */
    protected $admin_ipayment_number;

    /** @Column(name="admin_famis_posted_date", type="string", length=255, nullable=true) */
    protected $admin_famis_posted_date;

    /** @Column(name="admin_famis_posted_timestamp", type="integer", nullable=true) */
    protected $admin_famis_posted_timestamp;

    /** @Column(name="admin_ipayment_posted_date", type="string", length=255, nullable=true) */
    protected $admin_ipayment_posted_date;

    /** @Column(name="admin_accounts", type="text", nullable=true) */
    protected $admin_accounts;

    /** @Column(name="admin_invoice_number", type="string", length=255, nullable=true) */
    protected $admin_invoice_number;

    /** @Column(name="admin_is_posted_to_famis", type="integer", length=1, nullable=true) */
    protected $admin_is_posted_to_famis;

    /** @Column(name="admin_receipt_number", type="string", length=100, nullable=true) */
    protected $admin_receipt_number;

    /** @Column(name="admin_accounting_notes", type="text", nullable=true) */
    protected $admin_accounting_notes;
    
    /**
     * @ManyToMany(targetEntity="Role", inversedBy="users")
     * @JoinTable(name="user_has_role",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $roles;

    /** @OneToMany(targetEntity="Touchnet", mappedBy="user") */
    protected $touchnet;

    /** @OneToMany(targetEntity="AppealForm", mappedBy="user") */
    protected $appeals;

    /** @OneToMany(targetEntity="AltPaymentRequest", mappedBy="user") */
    protected $alt_payment_requests;

    /** @OneToMany(targetEntity="RefundRequest", mappedBy="user") */
    protected $refund_requests;

    /**
     * Single Record Functions
     */
    
    public function assignFromDirectory()
    {
		$directory = \DF\Service\TamuRest::getByUin($this->uin);
		
		if ($directory)
		{
			// Special replacement rules for NetID.
			if ($this->username == $this->uin && isset($directory['tamuEduPersonNetID'][0]))
				$this->username = $directory['tamuEduPersonNetID'][0];
			
			$replace_values = array(
				'firstname'		=> $directory['givenName'][0],
				'lastname'		=> $directory['sn'][0],
				'email'			=> $directory['mail'][0],
				'phone'			=> preg_replace('/[^\d]/', '', $directory['telephoneNumber'][0]),
			);
			
			foreach($replace_values as $replace_local => $replace_value)
			{
				$replace_current = $this->{$replace_local};
				if (empty($replace_current) && !empty($replace_value))
					$this->{$replace_local} = $replace_value;
			}
		}
        
        return $this;
    }
    
    /**
     * Static Functions
     */
    
    // Get or create a user by UIN.
    public static function getOrCreate($uin, $save_mode = TRUE)
    {
        $uin = trim($uin);
        if (strlen($uin) != 9)
        {
            throw new \DF\Exception\DisplayOnly('Your UIN ('.$uin.') appears to be a guest UIN and not an official Texas A&M University identification number. All accounts in this system must use NetIDs associated with official UINs. For more information about guest UINs, contact HelpDeskCentral at 979-845-8300.');
        }

        $record = self::getRepository()->findOneByUin($uin);
        
        if (!($record instanceof self))
        {
            $record = new self();
			$record->username = $uin;
            $record->uin = $uin;
            $record->password = ' ';
			$record->assignFromDirectory();

            if ($save_mode)
                $record->save();
        }
        
        return $record;
    }

    public static function getUsersWithAction($action_name)
    {
        $em = \Zend_Registry::get('em');
        
        return $em->createQuery('SELECT u FROM '.__CLASS__.' u LEFT JOIN u.roles r LEFT JOIN r.actions a WHERE a.name = :action_name ORDER BY u.lastname ASC')
            ->setParameter('action_name', $action_name)
            ->getArrayResult();
    }

    public static function lookUp($user_text)
    {
        $user_text = trim($user_text);
        
        if (strpos($user_text, ';') !== FALSE)
        {
            $api = new \DF\Service\DoitApi();
            $user_text = $api->getUinFromCard($user_text);
        }
        
        if (strlen($user_text) == 9 && is_numeric($user_text))
        {
            return self::getOrCreate($user_text);
        }
        
        try
        {
            $em = \Zend_Registry::get('em');
            $user_row = $em->createQuery('SELECT u FROM '.__CLASS__.' u WHERE u.id != 0 AND (u.username LIKE :q OR u.id = :q)')
                ->setParameter('q', $user_text)
                ->getSingleResult();
            
            return $user_row;
        }
        catch(\Exception $e)
        {
            return NULL;
        }
    }
}