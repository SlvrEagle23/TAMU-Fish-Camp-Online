<?php
namespace Entity;

/**
 * @Table(name="access_codes")
 * @Entity
 * @HasLifecycleCallbacks
 */
class AccessCode extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->created_at = time();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /** @Column(name="type", type="integer") */
    protected $type;

    public function getTypeName()
    {
        $config = \Zend_Registry::get('config');
        $types = $config->fishcamp->registration_types->toArray();

        return $types[$this->type];
    }

    public function setType($type)
    {
        $this->type = $type;

        $hash_base = 'fc_registration_override_'.mt_rand().'_'.$type;
        $this->code = substr(strtoupper(sha1($hash_base)), 0, 5).$type;
    }
    
    /** @Column(name="code", type="string", length=50) */
    protected $code;

    public function getCodeUrl()
    {
        return \DF\Url::route(array(
            'module'        => 'registration', 
            'controller'    => 'index', 
            'action'        => 'index', 
            'type'          => $this->type, 
            'type_access'   => $this->code,
        ));
    }

    /** @Column(name="active_start", type="integer") */
    protected $active_start;

    /** @Column(name="active_end", type="integer") */    
    protected $active_end;

    /** @Column(name="created_at", type="integer") */
    protected $created_at;

    /**
     * Static Functions
     */

    public static function check($type, $code = '')
    {
        $em = self::getEntityManager();

        // Clear old codes
        $delete_expired = $em->createQuery('DELETE FROM '.__CLASS__.' ac WHERE ac.active_end <= :time')
            ->setParameter('time', time())
            ->execute();

        // Check for a code that matches the parameters.
        try
        {
            $record = $em->createQuery('SELECT ac FROM '.__CLASS__.' ac WHERE ac.type = :type AND ac.code = :code AND ac.active_start <= :time AND ac.active_end >= :time')
                ->setParameter('type', $type)
                ->setParameter('code', $code)
                ->setParameter('time', time())
                ->getSingleResult();

            if ($record instanceof self)
                return true;
        }
        catch(\Exception $e)
        {
            return FALSE;
        }
    }
}