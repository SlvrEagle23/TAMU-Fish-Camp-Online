<?php
namespace Entity;

/**
 * @Table(name="settings")
 * @Entity
 */
class Settings extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="setting_key", type="string", length=64)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $setting_key;

    /** @Column(name="setting_value", type="json", nullable=true) */
    protected $setting_value;
    
    /**
     * Static Functions
     */
    
    public static function setSetting($key, $value)
    {
        $record = self::getRepository()->findOneBy(array('setting_key' => $key));
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->setSettingKey($key);
        }
        
        $record->setSettingValue($value);
        $record->save();
    }
    
    public static function getSetting($key, $default_value = NULL)
    {
        $settings = self::fetchCache();

        if (isset($settings[$key]))
            return $settings[$key];
        else
            return $default_value;
    }
    
    public static function fetchAll()
    {
        $all_records_raw = self::getRepository()->findAll();
        $all_records = array();
        foreach($all_records_raw as $record)
        {
            $all_records[$record['setting_key']] = $record['setting_value'];
        }
        return $all_records;
    }

    public static function fetchCache($reload = FALSE)
    {
        $settings = \DF\Cache::get('settings');

        if (!$settings || $reload)
        {
            $settings = self::fetchAll();
            \DF\Cache::set($settings, 'settings');
        }

        return $settings;
    }
}