<?php

namespace Entity;

/**
 * @Table(name="counselect")
 * @Entity
 */
class Counselect extends \DF\Doctrine\Entity
{
    const TOTAL_PER_CAMP = 12;

    const EXPERIENCED_MALE = 'expm';
    const EXPERIENCED_FEMALE = 'expf';
    const INEXPERIENCED_MALE = 'inexpm';
    const INEXPERIENCED_FEMALE = 'inexpf';

    const STATUS_PENDING = 0;
    const STATUS_UNASSIGNED = 5;
    const STATUS_ASSIGNED = 10;

    public function __construct()
    {
        $this->is_assigned = FALSE;
        $this->is_locked = FALSE;
        $this->status = self::STATUS_PENDING;
    }

    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="camp_id", type="integer") */
    protected $camp_id;

    /** @Column(name="session_id", type="integer") */
    protected $session_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="category", type="string", length=50, nullable=true) */
    protected $category;

    /** @Column(name="preference", type="integer", nullable=true) */
    protected $preference;

    /** @Column(name="status", type="smallint", nullable=true) */
    protected $status;

    /**
     * @ManyToOne(targetEntity="Camp")
     * @JoinColumn(name="camp_id", referencedColumnName="id")
     */
    protected $camp;

    /**
     * @ManyToOne(targetEntity="Session")
     * @JoinColumn(name="session_id", referencedColumnName="id")
     */
    protected $session;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Static Functions
     */
    
    public static function getCategories()
    {
        return array(
            self::EXPERIENCED_MALE      => 'Older Male',
            self::EXPERIENCED_FEMALE    => 'Older Female',
            self::INEXPERIENCED_MALE    => 'Freshman Male',
            self::INEXPERIENCED_FEMALE  => 'Freshman Female',
        );
    }

    public static function getCounselorsPerCategory()
    {
        $num_categories = count(self::getCategories());
        return round(self::TOTAL_PER_CAMP / $num_categories);
    }
}