<?php

namespace Entity;

/**
 * Entity\Camp
 *
 * @Table(name="camp")
 * @Entity
 */
class Camp extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="color", type="string", length=7, nullable=true) */
    protected $color;

    public function setColor($color)
    {
        $this->color = '#'.substr(str_replace('#', '', $color), 0, 6);
    }

    /**
     * Static Functions
     */

    public static function fetchSelect($add_blank = FALSE)
    {
        $select = array();
        if ($add_blank)
            $select[''] = 'Select...';

        $camps = self::fetchArray('name');
        foreach($camps as $camp)
            $select[$camp['name']] = $camp['name'];

        return $select;
    }
}