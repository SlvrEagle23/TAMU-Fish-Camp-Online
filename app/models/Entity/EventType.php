<?php
namespace Entity;

/**
 * EventType
 *
 * @Table(name="event_types")
 * @Entity
 */
class EventType extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="id", type="string", length=50)
     * @Id
     */
    protected $id;
    
    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;
    
    /** @Column(name="is_hidden", type="integer", length=1, nullable=true) */
    protected $is_hidden;
    
    /** @Column(name="training_credit", type="string", length=255, nullable=true) */
    protected $training_credit;
    
    /** @OneToMany(targetEntity="Entity\Event", mappedBy="type") */
    protected $events;

    /** @Column(name="confirmation_message", type="text", nullable=true) */
    protected $confirmation_message;
    
    /**
     * Static Functions
     */
    
    public static function fetchSelect()
    {
        $em = \Zend_Registry::get('em');
        $all_events = $em->createQuery('SELECT et FROM '.__CLASS__.' et ORDER BY et.name ASC')
            ->getArrayResult();
        
        $event_select = array();
        if ($all_events)
        {
            foreach($all_events as $event)
            {
                $event_select[$event['id']] = $event['name'];
            }
        }
        return $event_select;
    }
}