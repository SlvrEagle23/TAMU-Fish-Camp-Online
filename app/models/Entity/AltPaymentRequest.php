<?php

namespace Entity;

/**
 * @Table(name="alt_payment_request")
 * @Entity
 * @HasLifecycleCallbacks
 */
class AltPaymentRequest extends \DF\Doctrine\Entity
{
    const STATUS_PENDING_DIRECTOR = 1;
    const STATUS_PENDING_ADVISOR = 5;
    const STATUS_PENDING_ACCOUNTING = 10;
    const STATUS_ARCHIVED = 0;

    public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
        $this->status = self::STATUS_PENDING_DIRECTOR;
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /** @PostPersist */
    public function generateAccessCode()
    {
        $hash_start = sha1($this->id.'_FishCampAltPaymentRequest');
        $hash_start = strtoupper($hash_start);
        $hash_start = substr($hash_start, 0, 5);
        
        $this->access_code = 'FC-'.$hash_start.'-'.str_pad($this->id, 6, '0', STR_PAD_LEFT);
        $this->save();
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="access_code", type="string", length=30, nullable=true) */
    protected $access_code;

    /** @Column(name="status", type="smallint", nullable=true) */
    protected $status;

    public function getStatusName()
    {
        $names = self::getStatusNames();
        return $names[$this->status];
    }

    public function setStatus($new_status)
    {
        $old_status = $this->status;
        $this->status = $new_status;

        if ($old_status != $new_status)
            $this->notify($new_status);
    }

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="request_type", type="string", length=25, nullable=true) */
    protected $request_type;

    public function getRequestType()
    {
        if ($this->request_type)
            return $this->request_type;
        else
            return $this->request_data['altpayment_option'];
    }

    /** @Column(name="request_data", type="json", nullable=true) */
    protected $request_data;

    /** @Column(name="approval_type", type="smallint", nullable=true) */
    protected $approval_type;

    public function getApprovalTypeName()
    {
        $settings = self::loadSettings();

        if ($this->approval_type === NULL)
            return 'N/A';
        else
            return $settings['approval_types'][$this->approval_type];
    }

    /** @Column(name="scholarship", type="string", length=25, nullable=true) */
    protected $scholarship;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
    
    /**
     * @ManyToOne(targetEntity="Entity\User", inversedBy="alt_payment_requests")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    public function notify($phase = NULL)
    {
        $phase = ($phase === NULL) ? $this->status : $phase;
        $settings = self::loadSettings();

        if (isset($settings['notify'][$phase]))
        {
            \DF\Messenger::send(array(
                'to'        => $settings['notify'][$phase],
                'subject'   => 'Alternate Payment Request Updated',
                'module'    => 'forms',
                'template'  => 'altpayment/review',
                'vars'      => array(
                    'access_code' => $this->access_code,
                    'user' => $this->user,
                    'form' => $this->request_data,
                ),
            ));
        }
    }

    public function notifySubmitter()
    {
        \DF\Messenger::send(array(
            'to'        => $this->user->email,
            'subject'   => 'Your Fish Camp Alternate Payment Request',
            'module'    => 'forms',
            'template'  => 'altpayment/result',
            'vars'      => array(
                'access_code' => $this->access_code,
                'type' => $this->request_type,
                'approval' => $this->approval_type,
            ),
        ));
    }

    /**
     * Static Functions
     */

    public static function getStatusNames()
    {
        return array(
            self::STATUS_PENDING_DIRECTOR => 'Pending FC Directors',
            self::STATUS_PENDING_ADVISOR => 'Pending FC Advisor',
            self::STATUS_PENDING_ACCOUNTING => 'Pending Accounting',
            self::STATUS_ARCHIVED => 'Archived',
        );
    }

    public static function loadSettings()
    {
        static $settings;

        if ($settings === NULL)
        {
            $config = \Zend_Registry::get('module_config');
            $settings = $config['forms']->forms->altpayment->toArray();
        }

        return $settings;
    }
}