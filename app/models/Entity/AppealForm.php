<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_appeal")
 * @Entity
 * @HasLifecycleCallbacks
 */
class AppealForm extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->phase = 0;
		$this->created_at = new \DateTime('NOW');
        $this->updated_at = new \DateTime('NOW');
    }
    
    /** @PostPersist */
    public function inserted()
    {
        $id = $this->id;
        $password = str_pad($id, 5, '0', STR_PAD_LEFT).'-'.substr(strtoupper(sha1('StuActTravelForm'.$id)), 0, 5);
        
        $this->password = $password;
        $this->save();
    }
    
    /**
     * @Column(name="id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /** @Column(name="password", type="string", length=20, nullable=true) */
    protected $password;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;
    
    /** @Column(name="request_data", type="json", nullable=true) */
    protected $request_data;

    /** @Column(name="admin_data", type="json", nullable=true) */
    protected $admin_data;

    /** @Column(name="phase", type="integer", length=1, nullable=true) */
    protected $phase;
    
    /** @Column(name="review_final", type="integer", length=1, nullable=true) */
    protected $review_final;

    /** @Column(name="created_at", type="datetime", nullable=true) */
    protected $created_at;

    /** @Column(name="updated_at", type="datetime", nullable=true) */
    protected $updated_at;

    /**
     * @ManyToOne(targetEntity="Entity\User", inversedBy="appeals")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Per-Record Functions
     */

    public function getPhaseText()
    {
        $settings = self::getSettings();
        return $settings['phases'][$this->phase];
    }
    
    public function setPhase($new_phase)
    {
        $current_phase = $this->phase;

        if ($current_phase != $new_phase)
        {
            $this->phase = $new_phase;
            $this->updated_at = new \DateTime('NOW');
            $this->save();

            $this->notify($new_phase);
        }
    }

    public function notify($phase = NULL)
    {
        $settings = self::getSettings();
        $em = \Zend_Registry::get('em');

        define('DF_FORCE_EMAIL', TRUE);

        $phase = (!is_null($phase)) ? $phase : $this->phase;
        
        switch($phase)
        {
            case APPEAL_PHASE_INFO:
                $email_to = \Entity\User::getUsersWithAction('appeal form administrator');
                $email_subject = 'Appeal Requires Additional Information';
                $email_template = 'information';
            break;

            // Send notification to committee for review.
            case APPEAL_PHASE_COMMITTEE:
                $email_to = \Entity\User::getUsersWithAction('appeal form reviewer');
                $email_subject = 'Appeal Requires Review';
                $email_template = 'committee';
            break;
            
            // Send notification to manager for final decision.
            case APPEAL_PHASE_DECISION:
                $email_to = \Entity\User::getUsersWithAction('appeal form manager');
                $email_subject = 'Appeal Ready for Decision';
                $email_template = 'manager';
            break;
            
            // Send notification to final reviewer for approval.
            case APPEAL_PHASE_FINAL:
                $email_to = \Entity\User::getUsersWithAction('appeal form final');
                $email_subject = 'Appeal Needs Final Approval';
                $email_template = 'final';
            break;
                
            // Send notification to original submitter of final result.
            case APPEAL_PHASE_COMPLETE:
                $email_to = array($this->user);
                $email_subject = 'Appeal Review Complete';
                $email_template = 'result';
            break;
        }
        
        if ($email_to)
        {
            $to = array();
            foreach($email_to as $user)
                $to[] = $user['email'];
        }

        if (!$to)
            $to = $settings['default_email'];

        if ($email_template)
        {
            $email_settings = array(
                'to'        => $to,
                'subject'   => $email_subject,
                'module'    => 'forms',
                'template'  => 'appeals/'.$email_template,
                'vars'      => array(
                    'record' => $this,
                ),
            );

            \DF\Messenger::send($email_settings);
            return $email_settings;
        }
    }

    /**
     * Static Functions
     */
    
    public static function getSettings()
    {
        static $settings;

        if ($settings === NULL)
        {
            $config = \Zend_Registry::get('module_config');
            $settings = $config['forms']->appeals->toArray();
        }

        return $settings;
    }
}