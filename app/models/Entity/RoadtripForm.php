<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity\RoadtripForm
 *
 * @Table(name="roadtrip_form")
 * @Entity
 * @HasLifecycleCallbacks
 */
class RoadtripForm extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->messages = new ArrayCollection;
        
        // Immediately save to trigger the post-persist ID generation.
        $this->save();
    }
    
    /** @PrePersist */
    public function _setDefaults()
    {
        $this->timestamp = time();
        $this->form_is_submitted = 0;
    }
    
    /** @PostPersist */
    public function _generateIdHash()
    {
        $hash_start = sha1($this->id.'_FishCampRoadtripForm');
		$hash_start = strtoupper($hash_start);
		$hash_start = substr($hash_start, 0, 5);
        
        $this->app_id_hash = 'FC-'.$hash_start.'-'.str_pad($this->id, 6, '0', STR_PAD_LEFT);
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /** @Column(name="app_id_hash", type="string", length=50, nullable=true) */
    protected $app_id_hash;
    
    /** @Column(name="timestamp", type="integer", nullable=true) */
    protected $timestamp;

    /** @Column(name="form_data", type="json", nullable=true) */
    protected $form_data;

    public function setFormData($data)
    {
        $this->form_data = $data;
        $this->index_startdate = (int)$data['departure_date'];
        $this->index_enddate = (int)$data['return_date'];
    }

    /** @Column(name="index_startdate", type="integer", nullable=true) */
    protected $index_startdate;

    /** @Column(name="index_enddate", type="integer", nullable=true) */
    protected $index_enddate;
    
    /** @Column(name="form_advisor_notes", type="text", nullable=true) */
    protected $form_advisor_notes;
    
    /** @Column(name="form_director_notes", type="text", nullable=true) */
    protected $form_director_notes;
    
    /** @Column(name="form_is_submitted", type="integer", length=1) */
    protected $form_is_submitted;
    
    /**
     * @OneToMany(targetEntity="Entity\RoadtripFormMessage", mappedBy="form", cascade={"remove"})
     * @OrderBy({"message_timestamp" = "ASC"})
     */
    protected $messages;
}