<?php

namespace Entity;

/**
 * @Table(name="touchnet")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Touchnet extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->time_created = $this->time_updated = time();
        $this->ledgers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /** @PreUpdate */
    public function _updateTimestamp()
    {
        $this->time_updated = time();
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="transaction_id", type="string", length=100, nullable=true) */
    protected $transaction_id;
    
    /** @Column(name="local_record_id", type="integer", nullable=true) */
    protected $local_record_id;
    
    /** @Column(name="time_created", type="integer") */
    protected $time_created;

    /** @Column(name="time_updated", type="integer") */
    protected $time_updated;

    /** @Column(name="payment_status", type="string", length=200, nullable=true) */
    protected $payment_status;

    /** @Column(name="payment_amount", type="decimal", scale=2, nullable=true) */
    protected $payment_amount;

    /** @Column(name="payment_card_type", type="string", length=200, nullable=true) */
    protected $payment_card_type;

    /** @Column(name="payment_name", type="string", length=255, nullable=true) */
    protected $payment_name;

    /** @Column(name="payment_order_id", type="integer", nullable=true) */
    protected $payment_order_id;

    /** @Column(name="payment_internal_trans_id", type="string", length=50, nullable=true) */
    protected $payment_internal_trans_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="touchnet")
     * @JoinColumn(name="local_record_id", referencedColumnName="id")
     */
    protected $user;
    
    /** @OneToMany(targetEntity="Ledger", mappedBy="touchnet") */
    protected $ledgers;
}