<?php

namespace Entity;

/**
 * Entity\LedgerLog
 *
 * @Table(name="ledger_log")
 * @Entity
 * @HasLifecycleCallbacks
 */
class LedgerLog extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="ledger_item_id", type="integer") */
    protected $ledger_id;
    
    /**
     * @ManyToOne(targetEntity="Entity\Ledger")
     * @JoinColumn(name="ledger_item_id", referencedColumnName="id")
     */
    protected $ledger;

    /** @Column(name="message", type="text") */
    protected $message;

    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;

}