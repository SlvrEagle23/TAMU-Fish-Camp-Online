<?php

namespace Entity;

/**
 * Entity\RefundRequest
 *
 * @Table(name="refund_request")
 * @Entity
 * @HasLifecycleCallbacks
 */
class RefundRequest extends \DF\Doctrine\Entity
{
    const STATUS_PENDING_DIRECTOR = 1;
    const STATUS_PENDING_ADVISOR = 5;
    const STATUS_PENDING_ACCOUNTING = 10;
    const STATUS_ARCHIVED = 0;

    public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
        $this->status = self::STATUS_PENDING_DIRECTOR;
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /** @PostPersist */
	public function generateAccessCode()
	{
		$hash_start = sha1($this->id.'_FishCampRefundRequest');
		$hash_start = strtoupper($hash_start);
		$hash_start = substr($hash_start, 0, 5);
		
		$this->access_code = 'FC-'.$hash_start.'-'.str_pad($this->id, 6, '0', STR_PAD_LEFT);
        $this->save();
	}
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="access_code", type="string", length=30, nullable=true) */
    protected $access_code;

    /** @Column(name="status", type="smallint", nullable=true) */
    protected $status;

    public function getStatusName()
    {
        $names = self::getStatusNames();
        return $names[$this->status];
    }

    public function setStatus($new_status)
    {
        $old_status = $this->status;
        $this->status = $new_status;

        if ($old_status != $new_status)
            $this->notify($phase);
    }

    /** @Column(name="decision", type="string", length=25, nullable=true) */
    protected $decision;

    /** @Column(name="decision_amount", type="string", length=15, nullable=true) */
    protected $decision_amount;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="request_data", type="json", nullable=true) */
    protected $request_data;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
    
    /**
     * @ManyToOne(targetEntity="Entity\User", inversedBy="refund_requests")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    public function notify($phase = NULL)
    {
        $phase = ($phase === NULL) ? $this->status : $phase;
        $settings = self::loadSettings();

        if (isset($settings['notify'][$phase]))
        {
            \DF\Messenger::send(array(
                'to'        => $settings['notify'][$phase],
                'subject'   => 'Refund Request Status Updated',
                'module'    => 'forms',
                'template'  => 'refunds/review',
                'vars'      => array(
                    'access_code' => $this->access_code,
                    'user' => $this->user,
                    'form' => $this->request_data,
                ),
            ));
        }
    }

    public function notifySubmitter()
    {
        \DF\Messenger::send(array(
            'to'        => $this->user->email,
            'subject'   => 'Your Fish Camp Refund Request',
            'module'    => 'forms',
            'template'  => 'refunds/result',
            'vars'      => array(
                'access_code' => $this->access_code,
                'decision' => $this->decision,
                'decision_amount' => $this->decision_amount,
            ),
        ));
    }

    /**
     * Static Functions
     */

    public static function getStatusNames()
    {
        return array(
            self::STATUS_PENDING_DIRECTOR => 'Pending FC Directors',
            self::STATUS_PENDING_ADVISOR => 'Pending FC Advisor',
            self::STATUS_PENDING_ACCOUNTING => 'Pending Accounting',
            self::STATUS_ARCHIVED => 'Archived',
        );
    }

    public static function loadSettings()
    {
        static $settings;

        if ($settings === NULL)
        {
            $config = \Zend_Registry::get('module_config');
            $settings = $config['forms']->forms->refund->toArray();
        }

        return $settings;
    }

    public static function isOpen()
    {
        $close_date = Settings::getSetting('refund_close_date', time()+86400);
        $close_date = strtotime('Today 17:00:00', $close_date);
        return ($close_date > time());
    }
}