<?php

namespace Entity;

/**
 * Entity\RoadtripFormMessage
 *
 * @Table(name="roadtrip_form_message")
 * @Entity
 * @HasLifecycleCallbacks
 */
class RoadtripFormMessage extends \DF\Doctrine\Entity
{
    const MESSAGE_FROM_ADMIN = 0;
    const MESSAGE_FROM_SUBMITTER = 1;
    
    public function __construct()
    {
        $this->message_timestamp = time();
        $this->message_is_read = 0;
    }
    
    /**
     * @PostPersist
     */
    public function sendMessages()
    {
        switch($this->message_type)
        {
            case self::MESSAGE_FROM_ADMIN:
                $email_to = array($this->form->form_data['email'], $this->form->form_data['partner_email']);
                $email_template = 'roadtrip/messagesubmitter';
            break;
            
            case self::MESSAGE_FROM_SUBMITTER:
                $config = \Zend_Registry::get('config');
                $roadtrip_config = $config->roadtrip->toArray();
                
                $email_to = $roadtrip_config['notify_message'];
                $email_template = 'roadtrip/messageadmin';
            break;
        }
        
        \DF\Messenger::send(array(
            'to'        => $email_to,
            'subject'   => 'Message for Road Trip Form',
            'template'  => $email_template,
            'module'    => 'forms',
            'vars'      => array(
                'message'   => $this,
                'form'      => $this->form,
            ),
        ));
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /** @Column(name="form_id", type="integer") */
    protected $form_id;
    
    /** @Column(name="message_type", type="integer", length=1) */
    protected $message_type;
    
    /** @Column(name="message_body", type="text", nullable=true) */
    protected $message_body;
    
    /** @Column(name="message_timestamp", type="integer") */
    protected $message_timestamp;
    
    /** @Column(name="message_is_read", type="integer", length=1) */
    protected $message_is_read;

    /**
     * @ManyToOne(targetEntity="Entity\RoadtripForm")
     * @JoinColumn(name="form_id", referencedColumnName="id")
     */
    protected $form;
}