<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="session_change_form")
 * @Entity
 * @HasLifecycleCallbacks
 */
class SessionChangeForm extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
        $this->is_archived = false;
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;
    
    /** @Column(name="timestamp", type="integer", nullable=true) */
    protected $timestamp;

    /** @Column(name="old_sessions", type="string", length=20, nullable=true) */
    protected $old_sessions;

    /** @Column(name="new_sessions", type="string", length=20, nullable=true) */
    protected $new_sessions;

    /** @Column(name="reason", type="text", nullable=true) */
    protected $reason;

    /** @Column(name="is_archived", type="boolean") */
    protected $is_archived;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
}