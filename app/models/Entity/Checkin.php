<?php

namespace Entity;

/**
 * Entity\Checkin
 *
 * @Table(name="checkin")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Checkin extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->created_at = new \DateTime("now");
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /** @Column(name="user_id", type="integer") */
    protected $user_id;
    
    /** @Column(name="origin", type="string", length=50) */
    protected $origin;

    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
}