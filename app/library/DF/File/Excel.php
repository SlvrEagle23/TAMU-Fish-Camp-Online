<?php
namespace DF\File;

class Excel extends \DF\File
{
	public static function fetch($file_name, $sheet_num = 0)
	{
		$master_excel = new \PHPExcel;
		
		// Load spreadsheet and convert to CSV-style array.
		$file_path = self::getFilePath($file_name);
		
		$excel = \PHPExcel_IOFactory::load($file_path);
		$sheet = $excel->getSheet($sheet_num);
		$xls_data = $sheet->toArray();
		
		return $xls_data;
	}
	
	public static function clean($file_name)
	{
		$xls_data = self::fetch($file_name);
		$clean_data = array();
		
		if ($xls_data)
		{
			$headers = array();
			$row_num = 0;
            $col_num = 0;
            
            $header_row = array_shift($xls_data);
            foreach($header_row as $xls_col)
            {
                $field_name = strtolower(preg_replace("/[^a-zA-Z0-9_]/", "", $xls_col));
                if (!empty($field_name))
                    $headers[$col_num] = $field_name;
                $col_num++;
            }
            
			foreach($xls_data as $xls_row)
			{
				$col_num = 0;
				$clean_row = array();
				foreach($xls_row as $xls_col)
				{
					$col_name = (isset($headers[$col_num])) ? $headers[$col_num] : $col_num;
					$clean_row[$col_name] = $xls_col;
					$col_num++;
				}
                
                $clean_data[] = $clean_row;
				$row_num++;
			}
		}
		
		return $clean_data;
	}
}