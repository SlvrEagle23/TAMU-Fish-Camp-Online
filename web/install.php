<?php
// Check PHP version.
if (version_compare(PHP_VERSION, '5.3.0', '<'))
	die('ERROR: PHP 5.3.0 or above is required to run this application.');

// Include core functionality.
require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

// Initialize maintenance mode.
$layout = \DF\Application\Maintenance::getLayout();
$view = $layout->getView();

$this_page = \DF\Url::baseUrl().'/'.basename(__FILE__);

// Initalize Doctrine schema tools.
$em = \Zend_Registry::get('em');
$metadatas = $em->getMetadataFactory()->getAllMetadata();
$schema_tool = new \Doctrine\ORM\Tools\SchemaTool($em);

// Special actions.
switch(strtolower($_REQUEST['action']))
{
	case 'schema':
		// Trigger schema update.
		$schema_tool->updateSchema($metadatas);
		
		header("Location: ".$this_page);
		exit;
	break;

	case 'access':
		// Verify lack of existing roles and actions.
		$current_roles = \Entity\Role::fetchArray();
		$current_actions = \Entity\Action::fetchArray();

		if (count($current_roles) == 0 && count($current_actions) == 0)
		{
			$action = new \Entity\Action;
			$action->name = 'administer all';
			$em->persist($action);

			$role = new \Entity\Role;
			$role->name = 'Administrator';
			$role->actions->add($action);
			$em->persist($role);

			try
			{
				$user = $em->createQuery('SELECT u FROM \Entity\User u ORDER BY u.id ASC')
					->setMaxResults(1)
					->getSingleResult();
				
				$user->roles->add($role);
				$em->persist($user);
			}
			catch(\Exception $e) {}

			$em->flush();
		}

		header("Location: ".$this_page);
		exit;
	break;
	
	case 'info':
		phpinfo();
		exit;
	break;
}

/**
 * Run through checklist of items.
 */

$checklist = array(
	'server_type' => array(
		'title' 		=> 'Server Details',
		'description'	=> 'Information about the server type powering the application.',
		'status'		=> 'info',
		'messages'		=> array(
			'Server Software: '.$_SERVER['SERVER_SOFTWARE'],
			'PHP Version: '.phpversion(),
		),
	),
	'php_settings' => array(
		'title' 		=> 'PHP Settings',
		'description' 	=> 'Verify the status of certain PHP settings to ensure compatibility.',
		'status' 		=> 'success',
		'messages' 		=> array(),
	),
	'db' => array(
		'title' 		=> 'Database Connection',
		'description' 	=> 'Verify that the application can connect to the database and execute queries.',
		'status'		=> 'success',
		'messages'		=> array(),
		
	),
	'db_schema' => array(
		'title'			=> 'Database Schema',
		'description'	=> 'Verify that the database contains an up-to-date version of the database.',
		'status'		=> 'success',
		'messages'		=> array(),
	),
	'clean_urls' => array(
		'title'			=> 'Clean URLs',
		'description'	=> 'Verify that the pages of the site can be accessed from "Friendly" URLs (i.e. /account/login)',
		'status'		=> 'success',
		'messages'		=> array(),
	),
	'access' => array(
		'title'			=> 'Access Control',
		'description'	=> 'Verify that an administrator account can access the site.',
		'status'		=> 'success',
		'messages'		=> array(),
	),
);

// Check PHP settings.
$checklist['php_settings']['messages'][] = 'The <i>short_open_tag</i> setting must be set to <i>On</i>.';
$checklist['php_settings']['messages'][] = 'The <i>magic_quotes_gpc</i> setting must be set to <i>Off</i>.';
$checklist['php_settings']['messages'][] = 'The <i>magic_quotes_runtime</i> setting must be set to <i>Off</i>.';
$checklist['php_settings']['messages'][] = 'The <i>register_globals</i> setting must be set to <i>Off</i>.';

if (ini_get('short_open_tag') != 1)
	$checklist['php_settings']['status'] = 'error';
if (ini_get('magic_quotes_gpc') == 1)
	$checklist['php_settings']['status'] = 'error';
if (ini_get('magic_quotes_runtime') == 1)
	$checklist['php_settings']['status'] = 'error';
if (ini_get('register_globals') == 1)
	$checklist['php_settings']['status'] = 'error';

// DB already connected before this point in the bootstrap.
$checklist['db']['status'] = 'success';
$checklist['db']['messages'][] = 'The application was able to successfully connect to the database.';

// Check for DB updates.
$sqls = $schema_tool->getUpdateSchemaSql($metadatas);

if (count($sqls) == 0)
{
	$checklist['db_schema']['status'] = 'success';
	$checklist['db_schema']['messages'][] = 'The database schema is up to date.';
}
else
{
	$checklist['db_schema']['status'] = 'error';
	$checklist['db_schema']['messages'][] = 'The database schema is out of date. To force an update of the database schema, click the button below.<div class="alert-actions"><a class="btn" href="'.$this_page.'?action=schema">Update Schema</a></div>';
}

// Check for Clean URLs.
try
{
	$homepage = \DF\Url::baseUrl().'/index/index/test/true';
	$client = new Zend_Http_Client($homepage, array(
		'maxredirects' => 0,
		'timeout'      => 30
	));
	
	$response = $client->request('GET');
	
	if ($response->isSuccessful())
	{
		$checklist['clean_urls']['status'] = 'success';
		$checklist['clean_urls']['messages'][] = 'Clean URLs are correctly enabled on your server.';
	}
	else
	{
		$checklist['clean_urls']['status'] = 'error';
		$checklist['clean_urls']['messages'][] = 'Clean URLs are not correctly enabled on your server. Check to ensure the <i>.htaccess</i> (for Apache) or <i>web.config</i> (for IIS) file is configured properly.';
	}
}
catch(\Exception $e)
{
	$checklist['clean_urls']['status'] = 'error';
	$checklist['clean_urls']['messages'][] = 'An error occurred while trying to check for Clean URLs.';
}

// Check for access control.
try
{
	$current_roles = \Entity\Role::fetchArray();
	$current_actions = \Entity\Action::fetchArray();
	$current_users = \Entity\User::fetchArray();

	if (count($current_roles) != 0 || count($current_actions) != 0)
	{
		$checklist['access']['status'] = 'success';
		$checklist['access']['messages'][] = 'Access has been successfully set up.';
	}
	else
	{
		if (count($current_users) == 0)
		{
			$checklist['access']['status'] = 'error';
			$checklist['access']['messages'][] = 'You have not logged in to the application yet. Once you have logged in, your account can be granted access. Click the button below to visit the application homepage:<div class="alert-actions"><a class="btn" href="'.\DF\Url::baseUrl().'">Visit Homepage</a></div>';
		}
		else
		{
			$checklist['access']['status'] = 'error';
			$checklist['access']['messages'][] = 'Access control has not been set up for this application. Click the button below to set it up:<div class="alert-actions"><a class="btn" href="'.$this_page.'?action=access">Set Up Access Control</a></div>';
		}
	}
}
catch(\Exception $e)
{
	$checklist['access']['status']= 'error';
	$checklist['access']['messages'][] = 'You cannot set up access until the database is set up correctly.';
}

/**
 * Render main body.
 */
$view->placeholder('content')->captureStart();

echo '<h2>Installer Tool</h2>
	<p>Welcome to the DF application installer tool. This tool will ensure that your application meets the basic requirements for operation. If any items below are red, you must correct them before installing.</p>
	<p><b>Once you have completed the installation process, you should delete this file for security purposes.</b></p>
	<hr>';

foreach($checklist as $item)
{
	echo '<div class="alert-message block-message '.$item['status'].'">
			<h3>'.$item['title'].'</h3>
			<p>'.$item['description'].'</p>
			<ul style="margin-top: 10px; margin-bottom: 5px"><li>'.implode('</li><li>', $item['messages']).'</li></ul>
		</div>';
}

$view->placeholder('content')->captureEnd();
$content = (string)$view->placeholder('content');

\DF\Application\Maintenance::display($content);
?>