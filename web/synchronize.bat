@ECHO OFF

SET phpFolder="D:\PHP\5.3.9-nts-vc9"
SET PHPRC=%phpFolder%
SET PATH=%phpFolder%;%PATH

echo Started: >> synchronize_log.txt
date /t >> synchronize_log.txt
time /t >> synchronize_log.txt

SET phpBinary="%phpFolder%\php.exe"
"%phpBinary%" -d html_errors=off -qC ".\synchronize.php" %*

echo Finished: >> synchronize_log.txt
date /t >> synchronize_log.txt
time /t >> synchronize_log.txt
echo. >> synchronize_log.txt
exit 0